<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>workflowaction</shortname>
	<info>A tag library for Content Centre workflow action methods.</info>
	<tag>
		<name>scatter</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Scatter</tagclass>
		<info>
			Retrieves all fields of a loaded workflow action object and "scatters" their values into individual variables sharing a common prefix.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gather</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Gather</tagclass>
		<info>
			Retrieves name/value pairs from the environment, and sets them into a loaded workflow action object. The name/value pairs are grouped together using a prefix and a prefix separator.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Getid</tagclass>
		<info>
			Gets the id of the workflow action.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Getname</tagclass>
		<info>
			Gets the name of the workflow action.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Setname</tagclass>
		<info>
			Sets the name of the workflow action. 
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getelementname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Getelementname</tagclass>
		<info>
			Gets the element name of the workflow action.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setelementname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Setelementname</tagclass>
		<info>
			Sets the element name of the workflow action.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>


	<tag>
		<name>getdescription</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Getdescription</tagclass>
		<info>
			Gets the description of the workflow action.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setdescription</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Setdescription</tagclass>
		<info>
			Sets the description of the workflow action.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getarguments</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Getarguments</tagclass>
		<info>
			Gets the arguments of the workflow action.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setarguments</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowaction.Setarguments</tagclass>
		<info>
			Sets the arguments of the workflow action.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

</taglib>
