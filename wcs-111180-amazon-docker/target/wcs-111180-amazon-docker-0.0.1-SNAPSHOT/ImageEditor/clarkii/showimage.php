<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <title></title>
        <meta name="google" value="notranslate">         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Include CSS to eliminate any default margins/padding and set the height of the html element and
		     the body element to 100%, because Firefox, or any Gecko based browser, interprets percentage as
			 the percentage of the height of its parent container, which has to be set explicitly.  Fix for
			 Firefox 3.6 focus border issues.  Initially, don't display flashContent div so it won't show
			 if JavaScript disabled.
		-->
        <style type="text/css" media="screen"> 
			html, body	{ height:100%; }
            body { margin:0; padding:0; overflow:auto; text-align:center; background-color: #ffffff; }
			object:focus { outline:none; }
            #flashContent { display:none; }
        </style>

        <script type="text/javascript" src="swfobject.js"></script>

        <script type="text/javascript">
            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
            var swfVersionStr = "10.0.0";
            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
            var xiSwfUrlStr = "playerProductInstall.swf";

            // Store image file name
            var savedImage ="";

            var flashvars = {};
            var params = {};
            var attributes = {};

            // !!!  Change this according to script language !!!
            var fileExtension = ".php"; //".aspx" , ".php"

            //Here you can change default Clarkii data path
            var filePath = "";

            // Change these variables according to your needs
            var snapshotScript = "snapshot" + fileExtension;
            var showresultScript = "showresult" + fileExtension;
            var customizerScript = "customizer" + fileExtension;
			var uploadProjectScript = "upload_project" + fileExtension;
            var helpScript = "showhelp.html";
            var uploadsDir = "uploads/";
            var snapshotsDir = "snapshots/";

            //Flash variables
            // !!! Remove this in product version !!!
            flashvars.AdminID = "1";

            //Preloader flashvars.
            flashvars.PreloaderMainApplicationUrl = "clarkii4.swf";
            flashvars.PreloaderBackgroundColor = "FFFFFF";
            flashvars.PreloaderProgressBarColor = "0089DA";

            // Uncomment one of two
            //Start Clarkii with clarkii.png image from Clarkii's uploads directory.
            //flashvars.StartupProject = uploadsDir + "clarkii.png";

            //Start Clarkii with startup.xml project from Clarkii's projects directory.
            flashvars.FilePath = filePath;
            flashvars.StartupProject = "startup";
            flashvars.CustomizerScript = customizerScript;
			flashvars.UploadProjectScript = uploadProjectScript;
            flashvars.ClarkiiID = "Clarkii_1";
            flashvars.UserData = "Some user defined data string";

            //Text layer defaults
            flashvars.DefaultText="Put your text here";
            flashvars.DefaultTextFontFamily="_Arial";
            flashvars.DefaultTextSize="48";
            flashvars.DefaultTextColor="000000";
            flashvars.DefaultTextFontSizes="8,10,12,14,16,24,36,48,72";

            //Text Along Path layer defaults
            flashvars.DefaultTAPText="Put your text here";
            flashvars.DefaultTAPTextFontFamily="_Tahoma";
            flashvars.DefaultTAPTextSize="24";
            flashvars.DefaultTAPTextColor="0000FF";
            flashvars.DefaultTAPTextFontSizes="8,10,12,14,16,24,36,48,72";

            //Parameters
            params.quality = "high";
            params.bgcolor = "#ffffff";
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";

            //Attributes
            attributes.id = "clarkii4";
            attributes.name = "clarkii4";
            attributes.align = "middle";


            swfobject.embedSWF(
                "starter.swf", "flashContent",
                "100%", "100%",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);
			<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
			swfobject.createCSS("#flashContent", "display:none;text-align:left;");



            // Clarkii functions

            function onClarkiiStart(args){
               // Put your code here
               //var ClarkiiID = args[0];
               //var UserData = args[1];
               //alert(ClarkiiID + ", " + UserData);
               //clarkii().setUserData("Changed user data");
               //alert(clarkii().getUserData())

            }

            //Do not delete this function - it removes Clarkii's Splash screen
            function onClarkiiShow(){
               clarkii().removeSplashScreen();
            }

            function onApplicationLoad(args){
               // Put your code here
               //var ClarkiiID = args[0];
               //var UserData = args[1];
               //alert(ClarkiiID + ", " + UserData);
               //clarkii().setUserData("Changed user data");
               //alert(clarkii().getUserData())

            }

            function showClarkiiHelp(){
               //location.href = helpScript;
               window.open(helpScript);
            }

            function saveClarkiiSnapshot(args){
                try{
                    var saveLocally = args[0]
                    var fileName = args[1];
                    var jpgQuality = args[2];
                    //var ClarkiiID = args[3];
                    //var UserData = args[4];
                    //alert(ClarkiiID + ", " + UserData);
                } catch(e){
                    alert(e);
                }

                if (fileName == ""){
                    fileName = flashvars.StartupProject;
                }

                var extension = fileName.split(".").pop();
                fileName = fileName.replace(uploadsDir, "");

                // Uncoment/Comment this for random/not random file names
				var rand = 1000000 + Math.floor((9999999, 1000000 + 1) *  Math.random());
                fileName = rand + "." + extension;

                // Store file name
                savedImage = fileName;

                fileName = snapshotsDir + fileName;

                //clarkii().getAsBase64(extension, jpgQuality);
                clarkii().saveSnapshot(saveLocally, extension, jpgQuality, snapshotScript + "?fileName=" + fileName);

            }

            function onClarkiiSnapshot(args){
                // Put your code here
                //alert(args[0]);
                //var ClarkiiID = args[1];
                //var UserData = args[2];
                //alert(ClarkiiID + ", " + UserData);

                window.open(showresultScript + "?image=" + filePath + snapshotsDir + savedImage);
                //location.href = "showresultScript + "?image=" + filePath + snapshotsDir + savedImage;
            }

            function onClarkiiImageOpen(args){
                // Put your code here
                //var fileName = args[0];
                //var ClarkiiID = args[1];
                //var UserData = args[2];
                //alert(fileName + ", " + ClarkiiID + ", " + UserData);
            }

            function onClarkiiImageComplete(args){
                // Put your code here

                var ClarkiiID = args[0];
                var fileName = args[1];
                var UserData = args[2];
                var layerId = args[3];
                var imageX = args[4];
                var imageY = args[5];
                var imageWidth = args[6];
                var imageHeight = args[7];
                var imageRotation = args[8];


                //clarkii().moveImage(layerId, 100, 50, 45);
                //clarkii().scaleImage(layerId, 0.5, 0.5);
                //clarkii().cropImage(layerId, 0, 0, 100, 80);

                /*
                alert("ClarkiiID: " + ClarkiiID + "\n" +
                    "fileName: " + fileName + "\n" +
                    "UserData: " + UserData + "\n" +
                    "layerId: " + layerId + "\n" +
                    "x: " +  imageX + ", y: " + imageY + "\n" +
                    "image size: " + imageWidth + "x" + imageHeight + "\n" +
                    "imageRotation: " + imageRotation);
                */

            }

            function onClarkiiTemplateSave(args){
                // Put your code here
                //var fileName = args[0];
                //var ClarkiiID = args[1];
                //var UserData = args[2];
                //alert(fileName + ", " + ClarkiiID + ", " + UserData);
            }

            function setClarkiiProperty(){
                 clarkii().setProperty( document.getElementById("compId").value,
                                        document.getElementById("property").value,
                                        document.getElementById("value").value )
            }

            // Additional functions

            function save(){

                savedImage = document.getElementById("filename").value;

                clarkii().saveSnapshot(document.getElementById("savelocally").checked,
                                       document.getElementById("imagetype").value,
                                       document.getElementById("jpgquality").value,
                                       document.getElementById("scriptfile").value + "?fileName=" + snapshotsDir + savedImage);

            }

            function onSaveLocallyClick(){
                if (document.getElementById("savelocally").checked) {
                    document.getElementById("filename").disabled="disabled";
                    document.getElementById("scriptfile").disabled="disabled";
                } else {
                    document.getElementById("filename").disabled="";
                    document.getElementById("scriptfile").disabled="";
                }
            }

            function onImageTypeChange(){

                if (document.getElementById("imagetype").value == "jpg"){
                    document.getElementById("jpgquality").disabled="";
                } else {
                   document.getElementById("jpgquality").disabled="disabled";
                }
            }

            function onStartupFileChange(){

                flashvars.StartupProject = document.getElementById("startupfile").value;

                restartClarkii();

            }

            function restartClarkii(){

                swfobject.removeSWF("clarkii4");

                var newNode = document.createElement("div");
                newNode.setAttribute("id", "flashContent");
                document.body.appendChild(newNode);

                swfobject.embedSWF(
                    "clarkii4.swf", "flashContent",
                    "100%", "100%",
                    swfVersionStr, xiSwfUrlStr,
                    flashvars, params, attributes);
            }

            function getScriptExtension() {
                var ext = window.location.pathname.split('?')[0];
				ext = ext.split('.').pop();
				if (ext.toLowerCase() == "html") {
					ext = "php"; // Default is php scripting language
				}
                return "." + ext;
            }

	        function clarkii(){

				return swfobject.getObjectById("clarkii4");

			}


        </script>
    </head>
    <body>

        <!-- Test Clarkii styling via JavaScript-->
        <!--
        <div align="left">

        id:
        <input id="compId"  type="text" value="" />

        property:
        <input id="property" type="text" value="" />

        value:
        <input id="value" type="text" value="" />

        <input type="button" value="Set property" onclick="setClarkiiProperty()">
        </div>
        <hr/>
        -->

        <!-- Test Clarkii saving via JavaScript-->
        <!--
        <div align="left">
        Startup file:
        <select id="startupfile">
        <option value="uploads/clarkii.png">Image with relative path: uploads/clarkii.png</option>
        <option value="startup" selected>Startup project: startup</option>
        <option value="http://wp.clarkii.com/qm.png">Image from the same domain: http://wp.clarkii.com/qm.png</option>
        <option value="http://wp.clarkii.com/clarkii-proxy/?url=http://www.liveaquaria.com/images/categories/product/p-26101-clarkii.jpg">Image from internet via proxy: http://wp.clarkii.com/clarkii-proxy/?url=http://www.liveaquaria.com/images/categories/product/p-26101-clarkii.jpg </option>
        </select>
        <input type="button" value="Restart Clarkii" onclick="onStartupFileChange()">
        <br />

        Save locally:
        <input id="savelocally" type="checkbox" value="" onclick="onSaveLocallyClick()"></input>

        Image type:
        <select id="imagetype" onchange="onImageTypeChange()">
        <option value="jpg" selected>jpg</option>
        <option value="gif">gif</option>
        <option value="png">png</option>
        </select>

        Jpg quality:
        <select id="jpgquality">
        <option value="80" selected>80</option>
        <option value="40">40</option>
        <option value="60">60</option>
        </select>

        Script file:
        <input id="scriptfile"  type="text" value="snapshot.php" />

        File name:
        <input id="filename" type="text" value="snapshot.jpg" />

        <input type="button" value="Save" onclick="save()">
        </div>
        <hr/>
        -->


        <!-- SWFObject's dynamic embed method replaces this alternative HTML content with Flash content when enough
			 JavaScript and Flash plug-in support is available. The div is initially hidden so that it doesn't show
			 when JavaScript is disabled.
		-->

        <div id="flashContent"> flashContent
        	<p>
	        	To view this page ensure that Adobe Flash Player version
				10.0.0 or greater is installed.
			</p>
			<script type="text/javascript">
				var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://");
				document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='"
								+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" );
			</script>
        </div>

       	<noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%" id="clarkii4">
                <param name="movie" value="clarkii4.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="always" />
                <param name="allowFullScreen" value="true" />
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="clarkii4.swf" width="100%" height="100%">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="allowScriptAccess" value="always" />
                    <param name="allowFullScreen" value="true" />
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                	<p> 
                		Either scripts and active content are not permitted to run or Adobe Flash Player version
                		10.0.0 or greater is not installed.
                	</p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
	    </noscript>		
   </body>
</html>
