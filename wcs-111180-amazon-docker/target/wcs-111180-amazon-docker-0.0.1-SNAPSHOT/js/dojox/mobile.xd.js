/*
	Copyright (c) 2004-2011, The Dojo Foundation All Rights Reserved.
	Available via Academic Free License >= 2.1 OR the modified BSD license.
	see: http://dojotoolkit.org/license for details
*/


dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","dojox.mobile"],["require","dojox.mobile._base"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["dojox.mobile"]){_4._hasResource["dojox.mobile"]=true;_4.provide("dojox.mobile");_4.require("dojox.mobile._base");_4.experimental("dojox.mobile");}}};});