dojo._xdResourceLoaded(function(dojo, dijit, dojox){
return {depends: [["provide", "dojo.nls.fw_wem_login_ja-jp"],
["provide", "dijit.form.nls.validate"],
["provide", "dijit.form.nls.validate.ja_jp"]],
defineResource: function(dojo, dijit, dojox){dojo.provide("dojo.nls.fw_wem_login_ja-jp");dojo.provide("dijit.form.nls.validate");dijit.form.nls.validate._built=true;dojo.provide("dijit.form.nls.validate.ja_jp");dijit.form.nls.validate.ja_jp={"rangeMessage":"この値は範囲外です。","invalidMessage":"入力した値は無効です。","missingMessage":"この値は必須です。"};

}};});