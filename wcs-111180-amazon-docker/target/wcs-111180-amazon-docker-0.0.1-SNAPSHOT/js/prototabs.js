/*  Prototabs
 *  (c) 2007 James Starmer
 *
 *  Prototabs is freely distributable under the terms of an MIT-style license.
 *  For details, see the web site: http://www.jamesstarmer.com/prototabs
 *
/*--------------------------------------------------------------------------*/
var ProtoTabs = Class.create();
ProtoTabs.prototype = {
	
	initialize: function(element, options) {
		this.options = Object.extend({
			defaultPanel: '',
			ajaxUrls:{},nonajaxUrls:{},
			ajaxLoadingText:''	
		}, options || {});
		
		this.currentTab = '';
		this.element = $(element);
		this.listElements = $A(this.element.getElementsByTagName('LI'));

		//loop over each list element
		for(i = 0; i < this.listElements.length; i++) {	
			
			//get the tabs
			tabLI = this.listElements[i];
			var itemLinks = tabLI.getElementsByTagName('A'),
			tabLIitemId = itemLinks[0].href.split("#")[1];
			tabLI.linkedPanel = $(tabLIitemId);
			tabLI.linkedPanel.style.clear = "both";		//firefox hack
			if(this.options.ajaxUrls[tabLIitemId]){
			    url = this.options.ajaxUrls[tabLIitemId];
				new Ajax.PeriodicalUpdater(tabLIitemId,url,{
					method: 'get',
					frequency: 5,
					evalScripts:true,
					onSuccess:function(transport){
					updateTab(transport);
				}
				});
			} else if (this.options.nonajaxUrls[tabLIitemId]){
				url = this.options.nonajaxUrls[tabLIitemId];
				new Ajax.Updater(tabLIitemId,url,{
					method: 'get',
					onSuccess:function(transport){
					updateTab(transport);
				}
				});
			}

			//check for the intially active tab
			if((this.options.defaultPanel != '') && (this.options.defaultPanel == tabLIitemId)){
				this.openPanel(tabLI);
			}else{
				$($(tabLI).linkedPanel).hide();
			}
			// watch for clicked
			$(itemLinks[0]).observe('click', function(event){
					var target = event.target || event.srcElement;
					var closeImg = document.getElementById("_X");
					if (closeImg == target) {
						// Click occured inside the close box, do nothing.
					return false;
					} else {
						element = Event.findElement(event, 'LI');
						this.openPanel(element);					
						Event.stop(event); // like return false;
					}
			}.bind(this));
		}
	},
	
	openPanel: function(tab){
	    tab = $(tab); // ie hack
		if(this.currentTab != ''){
			this.currentTab.linkedPanel.hide();
			this.currentTab.removeClassName('selected');
		}
		
		//set the currently open panel to the new panel
		this.currentTab = tab;
		tab.linkedPanel.show();
		tab.addClassName('selected');
		if($('_searchTable') && $('onDemandTabs')){
			showOrHideSearchBx(tab);
		}
		var url = this.options.ajaxUrls[tab.itemId];
		
		// if there is an ajax url defined update the panel with ajax
		if(url != undefined){
			tab.linkedPanel.update(this.options.ajaxLoadingText);
			    new Ajax.Request(url,{
			    method: 'get',
			    onComplete: function(transport){
				tab.linkedPanel.update(transport.responseText);
				updateTab(transport);
				}
			});
		}
		
	}
};