/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.wem.framework.WorkSpace"],["require","fw.wem.framework.ApplicationManager"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.wem.framework.WorkSpace"]){_4._hasResource["fw.wem.framework.WorkSpace"]=true;_4.provide("fw.wem.framework.WorkSpace");_4.require("fw.wem.framework.ApplicationManager");_4.declare("fw.wem.framework.WorkSpace",null,{constructor:function(_7){},render:function(){this.domNode=document.body;var pm=new fw.wem.framework.ApplicationManager(this.domNode);pm.render();this.hideLoadingIndicator();},hideLoadingIndicator:function(){_4.destroy("wemloading");}});}}};});