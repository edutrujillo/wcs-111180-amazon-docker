/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.wem.framework.IframeRenderer"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.wem.framework.IframeRenderer"]){_4._hasResource["fw.wem.framework.IframeRenderer"]=true;_4.provide("fw.wem.framework.IframeRenderer");_4.declare("fw.wem.framework.IframeRenderer",null,{constructor:function(_7){this.view=_7;},render:function(){var _8=this.view.parentnode,_9=this.view.sourceurl,_a=this.view.height?this.view.height:"100%",_b=this.view.width?this.view.width:"100%",_c=document.createElement("iframe");_c.width=_b;_c.height=_a;_c.frameBorder=0;if(_4.isIE){_c.attachEvent("onload",this._onloadHandler);}else{_c.onload=this._onloadHandler;}_4.publish(fw.wem._APP_RENDERING_EVENT_);_c.src=_9;_4.byId(_8).appendChild(_c);},_onloadHandler:function(){_4.publish(fw.wem._APP_LOADED_EVENT_);}});}}};});