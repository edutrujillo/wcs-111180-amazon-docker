/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.wem.framework.ScriptRenderer"]){dojo._hasResource["fw.wem.framework.ScriptRenderer"]=true;dojo.provide("fw.wem.framework.ScriptRenderer");dojo.declare("fw.wem.framework.ScriptRenderer",null,{constructor:function(_1){this.view=_1;},render:function(){this.viewUrl=this.view.sourceurl;dojo.publish(fw.wem._APP_RENDERING_EVENT_);if(this.viewUrl){var _2=document.createElement("script");_2.type="text/javascript";_2.src=this.viewUrl;dojo.byId(this.view.parentnode).appendChild(_2);}else{eval(this.view.javascriptcontent);}dojo.publish(fw.wem._APP_LOADED_EVENT_);}});}