/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.wem.framework.WorkSpace"]){dojo._hasResource["fw.wem.framework.WorkSpace"]=true;dojo.provide("fw.wem.framework.WorkSpace");dojo.require("fw.wem.framework.ApplicationManager");dojo.declare("fw.wem.framework.WorkSpace",null,{constructor:function(_1){},render:function(){this.domNode=document.body;var pm=new fw.wem.framework.ApplicationManager(this.domNode);pm.render();this.hideLoadingIndicator();},hideLoadingIndicator:function(){dojo.destroy("wemloading");}});}