/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.wem.framework.IframeRenderer"]){dojo._hasResource["fw.wem.framework.IframeRenderer"]=true;dojo.provide("fw.wem.framework.IframeRenderer");dojo.declare("fw.wem.framework.IframeRenderer",null,{constructor:function(_1){this.view=_1;},render:function(){var _2=this.view.parentnode,_3=this.view.sourceurl,_4=this.view.height?this.view.height:"100%",_5=this.view.width?this.view.width:"100%",_6=document.createElement("iframe");_6.width=_5;_6.height=_4;_6.frameBorder=0;if(dojo.isIE){_6.attachEvent("onload",this._onloadHandler);}else{_6.onload=this._onloadHandler;}dojo.publish(fw.wem._APP_RENDERING_EVENT_);_6.src=_3;dojo.byId(_2).appendChild(_6);},_onloadHandler:function(){dojo.publish(fw.wem._APP_LOADED_EVENT_);}});}