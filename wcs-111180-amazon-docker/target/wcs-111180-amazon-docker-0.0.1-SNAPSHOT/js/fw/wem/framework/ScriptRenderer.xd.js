/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.wem.framework.ScriptRenderer"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.wem.framework.ScriptRenderer"]){_4._hasResource["fw.wem.framework.ScriptRenderer"]=true;_4.provide("fw.wem.framework.ScriptRenderer");_4.declare("fw.wem.framework.ScriptRenderer",null,{constructor:function(_7){this.view=_7;},render:function(){this.viewUrl=this.view.sourceurl;_4.publish(fw.wem._APP_RENDERING_EVENT_);if(this.viewUrl){var _8=document.createElement("script");_8.type="text/javascript";_8.src=this.viewUrl;_4.byId(this.view.parentnode).appendChild(_8);}else{eval(this.view.javascriptcontent);}_4.publish(fw.wem._APP_LOADED_EVENT_);}});}}};});