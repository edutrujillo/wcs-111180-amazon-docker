/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.wem.framework.IncludeRenderer"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.wem.framework.IncludeRenderer"]){_4._hasResource["fw.wem.framework.IncludeRenderer"]=true;_4.provide("fw.wem.framework.IncludeRenderer");_4.declare("fw.wem.framework.IncludeRenderer",null,{_proxyUrl:"ProxyServlet?url=",constructor:function(_7){this.view=_7;},render:function(){this.viewUrl=this.view.sourceurl;if(this.viewUrl){if(this.viewUrl.substr(0,1)!="/"&&this.viewUrl.substr(0,7)!="http://"){this.viewUrl=_4.config.fw_csPath+this.viewUrl;}else{this.viewUrl=(_4.config.fw_csPath?_4.config.fw_csPath:"")+this._proxyUrl+this.viewUrl;}_4.publish(fw.wem._APP_RENDERING_EVENT_);_4.xhrGet({url:this.viewUrl,load:_4.hitch(this,"includeHTML"),error:function(e){_4.publish(fw.wem._APP_LOADED_EVENT_);}});}else{_4.publish(fw.wem._APP_RENDERING_EVENT_);_4.byId(this.view.parentnode).innerHTML=this.view.includecontent;_4.publish(fw.wem._APP_LOADED_EVENT_);}},includeHTML:function(_8){_4.byId(this.view.parentnode).innerHTML=_8;_4.publish(fw.wem._APP_LOADED_EVENT_);}});}}};});