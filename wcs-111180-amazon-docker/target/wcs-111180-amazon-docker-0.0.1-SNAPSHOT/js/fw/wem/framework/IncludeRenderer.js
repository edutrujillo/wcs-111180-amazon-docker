/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.wem.framework.IncludeRenderer"]){dojo._hasResource["fw.wem.framework.IncludeRenderer"]=true;dojo.provide("fw.wem.framework.IncludeRenderer");dojo.declare("fw.wem.framework.IncludeRenderer",null,{_proxyUrl:"ProxyServlet?url=",constructor:function(_1){this.view=_1;},render:function(){this.viewUrl=this.view.sourceurl;if(this.viewUrl){if(this.viewUrl.substr(0,1)!="/"&&this.viewUrl.substr(0,7)!="http://"){this.viewUrl=dojo.config.fw_csPath+this.viewUrl;}else{this.viewUrl=(dojo.config.fw_csPath?dojo.config.fw_csPath:"")+this._proxyUrl+this.viewUrl;}dojo.publish(fw.wem._APP_RENDERING_EVENT_);dojo.xhrGet({url:this.viewUrl,load:dojo.hitch(this,"includeHTML"),error:function(e){dojo.publish(fw.wem._APP_LOADED_EVENT_);}});}else{dojo.publish(fw.wem._APP_RENDERING_EVENT_);dojo.byId(this.view.parentnode).innerHTML=this.view.includecontent;dojo.publish(fw.wem._APP_LOADED_EVENT_);}},includeHTML:function(_2){dojo.byId(this.view.parentnode).innerHTML=_2;dojo.publish(fw.wem._APP_LOADED_EVENT_);}});}