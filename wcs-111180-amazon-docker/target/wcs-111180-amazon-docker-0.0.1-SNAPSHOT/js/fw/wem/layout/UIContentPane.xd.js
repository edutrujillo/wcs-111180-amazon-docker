/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.wem.layout.UIContentPane"],["require","dojox.layout.ContentPane"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.wem.layout.UIContentPane"]){_4._hasResource["fw.wem.layout.UIContentPane"]=true;_4.provide("fw.wem.layout.UIContentPane");_4.require("dojox.layout.ContentPane");_4.declare("fw.wem.layout.UIContentPane",_6.layout.ContentPane,{_layoutChildren:function(){var gc=this.getChildren;this.getChildren=function(){return [];};this.inherited(arguments);this.getChildren=gc;},_isQuerying:false,_load:function(){this._isQuerying=true;var _7=_4.create("div",{innerHTML:this.onDownloadStart()},this.domNode,"first");_4.style(_7,{backgroundColor:"#fff",padding:"10px",position:"absolute",zIndex:"100",left:((this.domNode.offsetWidth-_7.offsetWidth)/2)+"px",top:"150px"});this.inherited(arguments);},_setContent:function(_8,_9){if(!this._isQuerying){this.inherited(arguments);}else{this._isQuerying=false;}}});}}};});