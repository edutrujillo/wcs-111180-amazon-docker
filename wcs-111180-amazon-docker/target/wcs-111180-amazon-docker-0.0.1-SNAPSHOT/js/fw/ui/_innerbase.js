/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui._innerbase"]){dojo._hasResource["fw.ui._innerbase"]=true;dojo.provide("fw.ui._innerbase");dojo.require("fw.xhr");dojo.require("fw.ui.ObjectFactory");dojo.require("fw.ui.slot.SingleSlotSource");dojo.require("dojo.parser");dojo.require("fw.ui.dnd.IFrameLogic");dojo.require("fw.ui.slot.MultiSlotContainer");dojo.require("fw.ui.dijit.insite.InlineEditBox");dojo.require("fw.ui.dijit.insite.CKEdit");dojo.require("dojo.io.script");dojo.require("dijit.form.NumberTextBox");dojo.require("dijit.form.Textarea");dojo.require("dijit.form.CurrencyTextBox");dojo.require("fw.ui.dijit.DateTextBox");dojo.require("dijit.form.ValidationTextBox");dojo.require("fw.ui.dijit.TimestampPicker");dojo.require("fw.ui.dijit.insite.IntegerTextBox");dojo.require("fw.ui.dijit.insite.UploadEditor");dojo.require("fw.ui.dijit.insite.MultivaluedContainer");dojo.require("fw.ui.slot.SingleDropZone");dojo.require("dojo.DeferredList");dojo.require("fw.ui.InnerConfig");dojo.require("fw.ui.insite.Extensions");dojo.require("fw.ui.dijit.SWFUpload");}