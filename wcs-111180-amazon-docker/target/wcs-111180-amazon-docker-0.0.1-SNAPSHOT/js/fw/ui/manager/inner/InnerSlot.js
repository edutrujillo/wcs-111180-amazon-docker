/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.manager.inner.InnerSlot"]){dojo._hasResource["fw.ui.manager.inner.InnerSlot"]=true;dojo.provide("fw.ui.manager.inner.InnerSlot");dojo.require("fw.ui.manager.inner.Slot");dojo.declare("fw.ui.manager.inner.InnerSlot",fw.ui.manager.inner.Slot,{set:function(_1){this.inherited(arguments);var _2;if(_1){if(_1.id&&_1.type){_2=this.parentAsset;if(_2){_2.set(this.parentField,{id:_1.id,type:_1.type},this.getIndex());_2.markEdited(this.parentField);}}if(_1.tname){this.setStatus("ED");}}},isContentEditable:function(){var _3=this.getParentAsset();if(_3){if(!_3.isEditable()){this.reason=fw.util.getString("UI/UC1/JS/NoAssetPermissions");return false;}return true;}return true;},clear:function(){var _4=this.parentAsset;if(_4){delete this.assetType;delete this.assetId;delete this.assetName;_4.set(this.parentField,"",this.getIndex());_4.markEdited(this.parentField);}}});}