/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.manager.inner.MultivaluedSlot"]){dojo._hasResource["fw.ui.manager.inner.MultivaluedSlot"]=true;dojo.provide("fw.ui.manager.inner.MultivaluedSlot");dojo.require("fw.ui.manager.inner.Slot");dojo.declare("fw.ui.manager.inner.MultivaluedSlot",fw.ui.manager.inner.Slot,{set:function(_1){},isEditable:function(){return false;},isContentEditable:function(){var _2=this.getParentAsset();if(_2){if(!_2.isEditable()){this.reason="You do not have the required asset permissions";return false;}return true;}return true;},clear:function(){},getButtonList:function(){if(this.getParentAsset()){return ["edit","reorderSlot"];}else{return ["edit"];}}});}