/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.manager.inner.AssetDataManager"]){dojo._hasResource["fw.ui.manager.inner.AssetDataManager"]=true;dojo.provide("fw.ui.manager.inner.AssetDataManager");dojo.declare("fw.ui.manager.inner.AssetDataManager",null,{assetRegistry:null,constructor:function(_1){this.assetRegistry={};},init:function(){var _2=this.getAll();dojo.forEach(_2,function(_3){_3.init();});},register:function(_4){var _5,id;if(_4){id=_4.id+":"+_4.type;_5=this.assetRegistry[id];if(!_5){_5=new fw.ui.manager.inner.Asset(_4);this.assetRegistry[id]=_5;}}return _5;},get:function(_6){var c,_7,_8=null,_9;if(_6){c=_6.type;_7=_6.id;_9=_7+":"+c;_8=this.assetRegistry[_9];}return _8;},getAll:function(){var _a=[],_b=this.assetRegistry,_c="";for(_c in _b){if(_b.hasOwnProperty(_c)){_a.push(_b[_c]);}}return _a;},getEditedAssets:function(){var _d=[],_e=this.assetRegistry,_f,key;for(key in _e){if(_e.hasOwnProperty(key)){_f=_e[key];if(_f.status==="ED"){_d.push(_e[key]);}}}return _d;},toJson:function(_10){var _11="[",len=0,i=0;if(_10){len=_10.length;for(i=0;i<len;i++){_11+=_10[i].toJson();if(i<len-1){_11+=",";}}}_11+="]";return _11;},getAllIds:function(){var _12=[],_13=this.assetRegistry,key="",_14;for(key in _13){if(_13.hasOwnProperty(key)){_14=_13[key];_12.push({id:_14.id,type:_14.type});}}return _12;}});}