/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.manager.inner.SingleValuedSlot"]){dojo._hasResource["fw.ui.manager.inner.SingleValuedSlot"]=true;dojo.provide("fw.ui.manager.inner.SingleValuedSlot");dojo.require("fw.ui.manager.inner.Slot");dojo.declare("fw.ui.manager.inner.SingleValuedSlot",fw.ui.manager.inner.Slot,{set:function(_1){this.inherited(arguments);if(_1){if(_1.id&&_1.type){this.parentAsset.set(this.parentField,{id:_1.id,type:_1.type},this.getIndex());this.parentAsset.markEdited(this.parentField);}if(_1.tname){this.setStatus("ED");}}},isContentEditable:function(){var _2=this.getParentAsset();if(_2){if(!_2.isEditable()){this.reason=fw.util.getString("UI/UC1/JS/NoAssetPermissions");return false;}return true;}return true;},clear:function(){var _3=this.parentAsset;if(_3){delete this.assetType;delete this.assetId;delete this.assetName;_3.set(this.parentField,"",this.getIndex());_3.markEdited(this.parentField);}}});}