/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.document.DocumentId"]){dojo._hasResource["fw.ui.document.DocumentId"]=true;dojo.provide("fw.ui.document.DocumentId");dojo.declare("fw.ui.document.DocumentId",null,{constructor:function(_1,id,_2,_3,_4){this.getId=function(){return id;};this.getType=function(){return _1;};this.getAssetType=function(){return _2;};this.isFlex=function(){return _3;};this.isComplex=function(){return _4;};},equals:function(_5){if(_5 instanceof fw.ui.document.DocumentId){var _6=this.getId()==_5.getId()&&this.getType()==_5.getType();var _7=(typeof (this.getAssetType())!=="undefined"&&typeof (_5.getAssetType())!=="undefined");var _8=(typeof (this.isFlex())!=="undefined"&&typeof (_5.isFlex())!=="undefined");var _9=(typeof (this.isComplex())!=="undefined"&&typeof (_5.isComplex())!=="undefined");if(_7===true){_6=_6&&this.getAssetType()==_5.getAssetType();}if(_8===true){_6=_6&&this.isFlex()==_5.isFlex();}if(_9===true){_6=_6&&this.isComplex()==_5.isComplex();}return _6;}}});}