/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.model.Asset"]){dojo._hasResource["fw.ui.model.Asset"]=true;dojo.provide("fw.ui.model.Asset");dojo.declare("fw.ui.model.Asset",null,{id:0,type:"",status:"",lockStatus:null,_isNewAsset:false,ccsourceid:0,preselected_dimensionid:0,preselected_dimensiontype:"Dimension",constructor:function(_1){dojo.mixin(this,_1);},set:function(_2,_3){if(!_2){return;}var _4=this[_2];this[_2]=_3;this._onPropertyChange(_2,_3,_4);},get:function(_5){if(!_5){return;}return this[_5];},_onPropertyChange:function(_6,_7,_8){this.onUpdate(_6,_7,_8);},onUpdate:function(_9,_a,_b){},isNew:function(){return this._isNewAsset;},setNew:function(_c){this._isNewAsset=_c;}});}