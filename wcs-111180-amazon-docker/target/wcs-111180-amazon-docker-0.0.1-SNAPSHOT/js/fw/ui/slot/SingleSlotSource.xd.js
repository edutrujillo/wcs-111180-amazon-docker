/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.slot.SingleSlotSource"],["require","fw.ui.slot.SingleDropZone"],["require","fw.ui.slot.ActivatorMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.slot.SingleSlotSource"]){_4._hasResource["fw.ui.slot.SingleSlotSource"]=true;_4.provide("fw.ui.slot.SingleSlotSource");_4.require("fw.ui.slot.SingleDropZone");_4.require("fw.ui.slot.ActivatorMixin");_4.declare("fw.ui.slot.SingleSlotSource",[fw.ui.slot.SingleDropZone,fw.ui.slot.ActivatorMixin],{buttons:[],overlayTitle:"",constructor:function(_7){this._addActivator();_4.connect(this,"onDndStart",this,function(){if(this._tooltip){this._tooltip.close();}});this.skipForm=true;},markupFactory:function(_8,_9){_8._skipStartup=true;return new fw.ui.slot.SingleSlotSource(_9,_8);},sync:function(){this.inherited(arguments);this._addActivator();},getOverlayParentNode:function(){var n=_4.query(".dojoDndItem",this.getNode())[0];if(!n){n=this._emptyNode;}return n;}});}}};});