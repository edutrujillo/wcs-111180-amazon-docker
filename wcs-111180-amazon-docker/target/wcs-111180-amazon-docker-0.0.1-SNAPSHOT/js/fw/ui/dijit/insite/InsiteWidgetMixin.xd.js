/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.insite.InsiteWidgetMixin"],["require","fw.ui.ObjectFactory"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.insite.InsiteWidgetMixin"]){_4._hasResource["fw.ui.dijit.insite.InsiteWidgetMixin"]=true;_4.provide("fw.ui.dijit.insite.InsiteWidgetMixin");_4.require("fw.ui.ObjectFactory");_4.declare("fw.ui.dijit.insite.InsiteWidgetMixin",null,{dirty:false,init:function(_7){var _8=this.widgetArgs,_9=this.get("value");this._asset=_7.asset;this.assetManager=fw.ui.ObjectFactory.createManagerObject("asset");if(typeof _9!=="undefined"){this._asset.set(_8.fieldName,_9,_8.index);}this.set("disabled",true);},canEdit:function(){return _4.when(this.assetManager.canEdit(this._asset.getId()),function(_a){return {isEditable:_a.permitted};});},setDirty:function(_b){this.dirty=_b;},isDirty:function(){return this.dirty;}});}}};});