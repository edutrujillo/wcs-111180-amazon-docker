/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.insite.InsiteWidgetMixin"]){dojo._hasResource["fw.ui.dijit.insite.InsiteWidgetMixin"]=true;dojo.provide("fw.ui.dijit.insite.InsiteWidgetMixin");dojo.require("fw.ui.ObjectFactory");dojo.declare("fw.ui.dijit.insite.InsiteWidgetMixin",null,{dirty:false,init:function(_1){var _2=this.widgetArgs,_3=this.get("value");this._asset=_1.asset;this.assetManager=fw.ui.ObjectFactory.createManagerObject("asset");if(typeof _3!=="undefined"){this._asset.set(_2.fieldName,_3,_2.index);}this.set("disabled",true);},canEdit:function(){return dojo.when(this.assetManager.canEdit(this._asset.getId()),function(_4){return {isEditable:_4.permitted};});},setDirty:function(_5){this.dirty=_5;},isDirty:function(){return this.dirty;}});}