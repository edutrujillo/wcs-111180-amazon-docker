/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.insite.AssetEditor"],["require","dijit._Widget"],["require","fw.ui.dijit.TooltipBar"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.insite.AssetEditor"]){_4._hasResource["fw.ui.dijit.insite.AssetEditor"]=true;_4.provide("fw.ui.dijit.insite.AssetEditor");_4.require("dijit._Widget");_4.require("fw.ui.dijit.TooltipBar");_4.declare("fw.ui.dijit.insite.AssetEditor",_5._Widget,{widgetArgs:null,editorParams:null,value:null,postMixInProperties:function(){this.inherited(arguments);var _7=this.widgetArgs;_7.dialogArgs={attrType:"textField",assetId:_7.assetId,fieldName:_7.fieldName,editor:this.editor,editorParams:this.editorParams,title:_7.fieldName,dialogType:"MultiValuedAssetDialog",isBinary:false,onSave:function(_8){}};},startup:function(){this.inherited(arguments);var _9=new fw.ui.dijit.TooltipBar({connectId:[this.id],buttonList:["reorder"],args:this.widgetArgs.dialogArgs});this.tooltip=_9;},isValid:function(){return true;}});}}};});