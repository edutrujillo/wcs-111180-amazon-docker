/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.insite.InsiteConfig"]){dojo._hasResource["fw.ui.dijit.insite.InsiteConfig"]=true;dojo.provide("fw.ui.dijit.insite.InsiteConfig");fw.ui.dijit.insite.InsiteConfig={"date":{widget:{cls:"fw.ui.dijit.DateTextBox",params:{popupClass:"fw.ui.dijit.TimestampPicker"}},dialogType:"MultiValuedAttributesDialog",attrType:"dateField"},"float":{widget:{cls:"dijit.form.NumberTextBox"},dialogType:"MultiValuedAttributesDialog",attrType:"textField"},"int":{widget:{cls:"fw.ui.dijit.insite.IntegerTextBox"},dialogType:"MultiValuedAttributesDialog",attrType:"textField"},"text":{widget:{cls:"dijit.form.Textarea"},dialogType:"MultiValuedAttributesDialog",attrType:"textField"},"long":{widget:{cls:"dijit.form.NumberTextBox"},dialogType:"MultiValuedAttributesDialog",attrType:"textField"},"money":{widget:{cls:"dijit.form.CurrencyTextBox"},dialogType:"MultiValuedAttributesDialog",attrType:"textField"},"string":{widget:{cls:"dijit.form.ValidationTextBox"},dialogType:"MultiValuedAttributesDialog",attrType:"textField"},"blob":{widget:{cls:"fw.ui.dijit.insite.UploadEditor"},dialogType:"MultiValuedUploadDialog",attrType:"fileField"},"asset":{widget:{cls:""},dialogType:"MultiValuedAssetDialog",attrType:"assetField"},"list":{widget:{cls:""},dialogType:"MultiValuedAssetDialog",attrType:"assetField"}};}