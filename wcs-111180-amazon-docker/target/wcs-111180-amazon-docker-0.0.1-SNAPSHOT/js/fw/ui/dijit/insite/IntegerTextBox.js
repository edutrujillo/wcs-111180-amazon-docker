/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.insite.IntegerTextBox"]){dojo._hasResource["fw.ui.dijit.insite.IntegerTextBox"]=true;dojo.provide("fw.ui.dijit.insite.IntegerTextBox");dojo.require("dijit.form.NumberTextBox");dojo.declare("fw.ui.dijit.insite.IntegerTextBox",dijit.form.NumberTextBox,{_setConstraintsAttr:function(_1){_1.places=0;this.inherited(arguments);}});}