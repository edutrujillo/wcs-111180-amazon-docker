/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.MultiValuedCKEditor"]){dojo._hasResource["fw.ui.dijit.MultiValuedCKEditor"]=true;dojo.provide("fw.ui.dijit.MultiValuedCKEditor");dojo.require("fw.ui.dijit.MultiValuedAttributes");dojo.require("fw.ui.dijit.insite.CKEdit");dojo.declare("fw.ui.dijit.MultiValuedCKEditor",fw.ui.dijit.MultiValuedAttributes,{uId:1,buildItem:function(_1,_2){var id=this.uId++;var _3=dojo.create("div",{"class":"listItemField",id:"ckeditor"+id,innerHTML:_2},_1);var ep={};for(var _4 in this.editorParams){ep[_4]=this.editorParams[_4];}var _5=new fw.ui.dijit.insite.InlineEditBox({editor:"fw.ui.dijit.insite.CKEdit",renderAsHtml:true,editorParams:dojo.mixin(ep,{editorName:"ckeditor"+id,isMultiValued:true})},_3);_5.startup();}});}