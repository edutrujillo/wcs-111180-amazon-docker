/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.form.Form"]){dojo._hasResource["fw.ui.dijit.form.Form"]=true;dojo.provide("fw.ui.dijit.form.Form");dojo.require("dijit.form.Form");dojo.declare("fw.ui.dijit.form._FormMixin",null,{_getValueAttr:function(){var _1={};dojo.forEach(this.getDescendants(),function(_2){var _3=_2.name;if(!_3||_2.disabled){return;}var _4=_2.get("value");if(typeof _2.checked=="boolean"){if(/Radio/.test(_2.declaredClass)){if(_4!==false){dojo.setObject(_3,_4,_1);}else{_4=dojo.getObject(_3,false,_1);if(_4===undefined){dojo.setObject(_3,null,_1);}}}else{var _5=dojo.getObject(_3,false,_1);if(!_5){_5=[];dojo.setObject(_3,_5,_1);}if(_4!==false){_5.push(_4);}}}else{var _6=dojo.getObject(_3,false,_1);if(typeof _6!="undefined"){if(dojo.isArray(_6)){if(dojo.isArray(_4)){for(var x=0;x<_4.length;x++){_6.push(_4[x]);}}else{_6.push(_4);}}else{dojo.setObject(_3,[_6,_4],_1);}}else{dojo.setObject(_3,_4,_1);}}});return _1;}});dojo.declare("fw.ui.dijit.form.Form",[dijit.form.Form,fw.ui.dijit.form._FormMixin],{});}