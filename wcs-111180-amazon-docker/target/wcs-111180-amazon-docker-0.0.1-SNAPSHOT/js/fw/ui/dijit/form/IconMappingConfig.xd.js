/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.form.IconMappingConfig"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.form.IconMappingConfig"]){_4._hasResource["fw.ui.dijit.form.IconMappingConfig"]=true;_4.provide("fw.ui.dijit.form.IconMappingConfig");(function(){var _7="excel",_8="pdf",_9="powerpoint",_a="word";fw.ui.dijit.form.DEFAULT_ICON="default";fw.ui.dijit.form.iconMapping={"xls":_7,"xlsx":_7,"pdf":_8,"ppt":_9,"txt":_a,"doc":_a};})();}}};});