/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.form.DropZone"]){dojo._hasResource["fw.ui.dijit.form.DropZone"]=true;dojo.provide("fw.ui.dijit.form.DropZone");dojo.require("dijit._Widget");dojo.require("dijit._Templated");dojo.declare("fw.ui.dijit.form.DropZone",[dijit._Widget,dijit._Templated],{templateString:dojo.cache("fw.ui.dijit.form","templates/DropZone.html","<div>\r\n\t<div class=\"DropOuter\">\r\n\t\t<div class=\"MainDropDiv\">\r\n\t\t\t<div class=\"DropHand\"></div>\r\n\t\t\t<div class=\"DropText\">${strDropZone}</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"),baseClass:"DropZone",strDropZone:fw.util.getString("UI/UC1/JS/DropZone")});}