/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.CheckedMenuItem"]){dojo._hasResource["fw.ui.dijit.CheckedMenuItem"]=true;dojo.provide("fw.ui.dijit.CheckedMenuItem");dojo.require("dijit.CheckedMenuItem");dojo.declare("fw.ui.dijit.CheckedMenuItem",dijit.CheckedMenuItem,{templateString:dojo.cache("fw.ui.dijit","templates/CheckedMenuItem.html","<div class=\"dijitReset dijitMenuItem\" dojoAttachPoint=\"focusNode\" role=\"menuitemcheckbox\" tabIndex=\"-1\" dojoAttachEvent=\"onmouseenter:_onHover,onmouseleave:_onUnhover,ondijitclick:_onClick\">\r\n\t<span class=\"dijitReset dijitMenuItemIconCell\" role=\"presentation\">\r\n\t\t<img src=\"${_blankGif}\" alt=\"\" class=\"dijitMenuItemIcon dijitCheckedMenuItemIcon\" dojoAttachPoint=\"iconNode\" />\r\n\t\t<span class=\"dijitCheckedMenuItemIconChar\">&#10003;</span>\r\n\t</span>\r\n\t<span class=\"dijitReset dijitMenuItemLabel\" dojoAttachPoint=\"containerNode,labelNode\"></span>\r\n</div>\r\n"),previewWrapper:false,previewSegment:false,_onClick:function(e){this.inherited(arguments);if(this.checked){var _1=this._getEnclosingDialog();if(_1){setTimeout(_1.onCancel,200);}}},_getEnclosingDialog:function(_2){_2=_2||this.domNode.parentNode;var _3=dijit.getEnclosingWidget(_2);if(!_3){return null;}else{if(_3.isInstanceOf(dijit.TooltipDialog)){return _3;}}return this._getEnclosingDialog(_3.domNode.parentNode);}});}