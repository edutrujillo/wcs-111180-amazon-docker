/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.Menu"]){dojo._hasResource["fw.ui.dijit.Menu"]=true;dojo.provide("fw.ui.dijit.Menu");dojo.require("dijit.Menu");dojo.declare("fw.ui.dijit.Menu",dijit.Menu,{templateString:"<div class=\"dijit dijitMenu dijitMenuPassive dijitReset dijitMenuTable\" role=\"menu\" tabIndex=\"${tabIndex}\" dojoAttachEvent=\"onkeypress:_onKeyPress\"><div class=\"dijitReset\" dojoAttachPoint=\"containerNode\"></div></div>"});}