/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.TooltipDialog"],["require","dijit.TooltipDialog"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.TooltipDialog"]){_4._hasResource["fw.ui.dijit.TooltipDialog"]=true;_4.provide("fw.ui.dijit.TooltipDialog");_4.require("dijit.TooltipDialog");_4.declare("fw.ui.dijit.TooltipDialog",_5.TooltipDialog,{templateString:_4.cache("fw.ui.dijit","templates/TooltipDialog.html","<div role=\"presentation\" tabIndex=\"-1\">\r\n\t<div class=\"dijitTooltipContainer\" role=\"presentation\">\r\n\t\t${!_roundedCornerTemplateString}\r\n\t\t<div class =\"dijitTooltipContents dijitTooltipFocusNode\" dojoAttachPoint=\"containerNode\" role=\"dialog\"></div>\r\n\t</div>\r\n\t<div class=\"dijitTooltipConnector\" role=\"presentation\"></div>\r\n</div>\r\n"),_roundedCornerTemplateString:_4.cache("fw.ui.dijit","templates/RoundedCorners.html","<div class=\"background\"></div>\r\n<div class=\"topLeft corner\"></div>\r\n<div class=\"top horizontal\"><div></div></div>\r\n<div class=\"topRight corner\"></div>\r\n<div class=\"left vertical\"><div></div></div>\r\n<div class=\"right vertical\"><div></div></div>\r\n<div class=\"bottomLeft corner\"></div>\r\n<div class=\"bottom horizontal\"><div></div></div>\r\n<div class=\"bottomRight corner\"></div>\r\n"),baseClass:"fwTooltipDialog"});}}};});