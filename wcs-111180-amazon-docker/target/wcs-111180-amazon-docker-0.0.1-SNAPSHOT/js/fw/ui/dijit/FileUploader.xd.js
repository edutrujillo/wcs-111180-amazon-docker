/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.FileUploader"],["require","dojox.form.FileUploader"],["require","dijit.ProgressBar"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.FileUploader"]){_4._hasResource["fw.ui.dijit.FileUploader"]=true;_4.provide("fw.ui.dijit.FileUploader");_4.require("dojox.form.FileUploader");_4.require("dijit.ProgressBar");_4.declare("fw.ui.dijit.FileUploader",_6.form.FileUploader,{getHiddenWidget:function(){var _7=this.inherited(arguments);if(_7&&_4.position(_7.domNode).h>0){return null;}return _7;},postMixInProperties:function(){this.inherited(arguments);if(this.progressWidgetId&&this.progressWidgetContainer){var _8=_4.create("div",{id:this.progressWidgetId,innerHTML:fw.util.getString("UI/UC1/JS/Uploading")},_4.byId(this.progressWidgetContainer));this._progressBar=new _5.ProgressBar({},_8);this._progressBar.startup();_4.addClass(this._progressBar.domNode,"fileUploaderLoading");}},getText:function(_9){var cn=_4.trim(_9.innerHTML);if(cn.indexOf("<")>-1&&this.uploaderType=="flash"){cn=escape(cn);}return cn;}});}}};});