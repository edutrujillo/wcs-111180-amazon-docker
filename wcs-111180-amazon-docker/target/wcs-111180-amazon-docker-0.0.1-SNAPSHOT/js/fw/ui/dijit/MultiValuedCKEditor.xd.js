/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.MultiValuedCKEditor"],["require","fw.ui.dijit.MultiValuedAttributes"],["require","fw.ui.dijit.insite.CKEdit"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.MultiValuedCKEditor"]){_4._hasResource["fw.ui.dijit.MultiValuedCKEditor"]=true;_4.provide("fw.ui.dijit.MultiValuedCKEditor");_4.require("fw.ui.dijit.MultiValuedAttributes");_4.require("fw.ui.dijit.insite.CKEdit");_4.declare("fw.ui.dijit.MultiValuedCKEditor",fw.ui.dijit.MultiValuedAttributes,{uId:1,buildItem:function(_7,_8){var id=this.uId++;var _9=_4.create("div",{"class":"listItemField",id:"ckeditor"+id,innerHTML:_8},_7);var ep={};for(var _a in this.editorParams){ep[_a]=this.editorParams[_a];}var _b=new fw.ui.dijit.insite.InlineEditBox({editor:"fw.ui.dijit.insite.CKEdit",renderAsHtml:true,editorParams:_4.mixin(ep,{editorName:"ckeditor"+id,isMultiValued:true})},_9);_b.startup();}});}}};});