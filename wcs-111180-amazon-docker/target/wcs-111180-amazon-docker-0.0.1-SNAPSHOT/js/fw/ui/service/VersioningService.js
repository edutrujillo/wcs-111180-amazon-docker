/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.service.VersioningService"]){dojo._hasResource["fw.ui.service.VersioningService"]=true;dojo.provide("fw.ui.service.VersioningService");dojo.declare("fw.ui.service.VersioningService",null,{VERSIONING:"UI/Layout/CenterPane/Versioning",checkout:"UI/Actions/Asset/RevisionTracking/Checkout",checkin:"UI/Actions/Asset/RevisionTracking/Checkin",undocheckout:"UI/Actions/Asset/RevisionTracking/UndoCheckout",constructor:function(_1){var _2=fw.ui.ObjectFactory;this.requestService=_2.createServiceObject("request");},invoke:function(_3,_4,_5){var _6=this,xn=_4&&dojo.indexOf(["checkout","checkin","undocheckout"],_4)>-1?_4:"",_7,_8,_5=_5||{},_9={"assetIds":_3,"keepCheckedOut":_5.keepCheckedOut,"comment":_5.comment};if(_9.assetIds){var _8={elementName:this[xn],responseType:"json",params:_9,requestType:"post"};return dojo.when(this.requestService.sendControllerRequest(_8),function(_a){return _a.status;});}else{_7=new dojo.Deferred();_7.reject(fw.util.getString("UI/UC1/JS/CannotCheckin1"));return _7.promise;}}});}