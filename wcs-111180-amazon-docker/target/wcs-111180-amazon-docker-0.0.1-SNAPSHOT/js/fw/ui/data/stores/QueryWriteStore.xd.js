/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.data.stores.QueryWriteStore"],["require","dojox.data.QueryReadStore"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.data.stores.QueryWriteStore"]){_4._hasResource["fw.ui.data.stores.QueryWriteStore"]=true;_4.provide("fw.ui.data.stores.QueryWriteStore");_4.require("dojox.data.QueryReadStore");_4.declare("fw.ui.data.stores.QueryWriteStore",_6.data.QueryReadStore,{constructor:function(_7){this._features["dojo.data.api.Write"]=true;this._features["dojo.data.api.Notification"]=true;},referenceIntegrity:true,_assert:function(_8){if(!_8){throw new Error("assertion failed in ItemFileWriteStore");}},_getIdentifierAttribute:function(){var _9=this.getFeatures()["dojo.data.api.Identity"];return _9;},newItem:function(_a,_b){},setValues:function(_c,_d,_e){this._assertIsItem(_c);if(!_4.isString(_d)){throw new Error(this._className+".getValue(): Invalid attribute, string expected!");}this._assert(typeof _e!=="undefined");var _f=this._getIdentifierAttribute();if(_d==_f){throw new Error("ItemFileWriteStore does not have support for changing the value of an item's identifier.");}return true;},});}}};});