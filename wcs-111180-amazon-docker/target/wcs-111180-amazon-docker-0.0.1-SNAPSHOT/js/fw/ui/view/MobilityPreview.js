/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.view.MobilityPreview"]){dojo._hasResource["fw.ui.view.MobilityPreview"]=true;dojo.provide("fw.ui.view.MobilityPreview");dojo.require("fw.ui.view.Preview");dojo.declare("fw.ui.view.MobilityPreview",[fw.ui.view.Preview],{init:function(){this.inherited(arguments);this.deviceImageWidget&&this.deviceImageWidget.showViewLabel();}});}