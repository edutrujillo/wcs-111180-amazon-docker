/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.view.DashboardView"]){dojo._hasResource["fw.ui.view.DashboardView"]=true;dojo.provide("fw.ui.view.DashboardView");dojo.require("fw.ui.service.StorageService");dojo.require("fw.ui.view.ServerView");dojo.require("fw.ui.view.TabbedViewMixin");dojo.declare("fw.ui.view.DashboardView",[fw.ui.view.ServerView,fw.ui.view.TabbedViewMixin],{viewElement:"UI/Layout/CenterPane/DashBoard",closable:false,refetchOnRefresh:true,getViewArgs:function(_1){var _2=fw.ui.ObjectFactory;var _3=_2.getService("storage");var _4=_3.getAttribute("dashboard");var _5;if(_4){_5=_4;}dojo.mixin(_1,{"persistedPositions":_5});}});}