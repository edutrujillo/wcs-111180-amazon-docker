/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.view.SimpleView"]){dojo._hasResource["fw.ui.view.SimpleView"]=true;dojo.provide("fw.ui.view.SimpleView");dojo.require("fw.ui.view.ServerView");dojo.require("fw.ui.view.TabbedViewMixin");dojo.require("fw.ui.controller.BaseDocController");dojo.declare("fw.ui.view.SimpleView",[fw.ui.controller.BaseDocController,fw.ui.view.ServerView,fw.ui.view.TabbedViewMixin],{viewElement:"UI/Layout/CenterPane/GenericView",mode:"view",constructor:function(_1){this.mode=_1.name==="new"?"create":"view";},inspect:function(){this.focus();this.set("mode","view");this.draw();},edit:function(){this.focus();this.set("mode","edit");this.draw();},draw:function(){var _2,_3={},_4=this.get("model"),_5,_6;this.set("toolbarMode",this.get("mode"));this.inherited(arguments);_3.mode=this.get("mode");if(this.element){_2=this.requestService.getControllerURL(this.element,this.getElementArgs(_3));}else{if(this.url){_2=this.url;}}if(_2){this.contentPane.src=_2;}else{console.error("Unable to render view ",this.viewType,": could not find 'element' or 'url' property");}},getElementArgs:function(_7){var _8=this.get("model"),_9;_7=_7||{};if(_8){_9=_8.get("id");if(_9){_7.id=_9.getId();_7.type=_9.getType();}}return _7;}});}