/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.data.transport.XhrGet"]){dojo._hasResource["fw.data.transport.XhrGet"]=true;dojo.provide("fw.data.transport.XhrGet");fw.data.transport.XhrGet={sendFetchRequest:function(_1,_2,_3,_4,_5){var _6={url:_1,content:_2,handleAs:"json-comment-optional"};if(_3&&dojo.isObject(_3)){dojo.mixin(_6,_3);}if(_4){_6.load=_4;}if(_5){_6.error=_5;}return dojo.xhrGet(_6);}};}