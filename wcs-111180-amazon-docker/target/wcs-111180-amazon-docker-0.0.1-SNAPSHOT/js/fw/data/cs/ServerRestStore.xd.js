/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.data.cs.ServerRestStore"],["require","fw.data.ServerStore"],["require","fw.data.cs._Resources"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.data.cs.ServerRestStore"]){_4._hasResource["fw.data.cs.ServerRestStore"]=true;_4.provide("fw.data.cs.ServerRestStore");_4.require("fw.data.ServerStore");_4.require("fw.data.cs._Resources");_4.declare("fw.data.cs.ServerRestStore",fw.data.ServerStore,{resource:"",params:null,constructor:function(_7){var _8=fw.data.cs._Resources.resolveStoreArgs(_7);_4.mixin(this,_8);}});}}};});