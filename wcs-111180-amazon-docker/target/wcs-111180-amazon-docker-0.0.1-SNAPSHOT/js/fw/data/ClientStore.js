/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.data.ClientStore"]){dojo._hasResource["fw.data.ClientStore"]=true;dojo.provide("fw.data.ClientStore");dojo.require("dojo.data.ItemFileReadStore");dojo.require("fw.data._BaseStore");dojo.declare("fw.data.ClientStore",[fw.data._BaseStore,dojo.data.ItemFileReadStore],{postscript:function(_1){if(!this._checkProperties()){return;}this._jsonFileUrl=true;this._loadInProgress=true;var _2=this.transport.sendFetchRequest(this.url,{},this.transportOptions,dojo.hitch(this,"_receiveData"),function(_3){console.error(_3);});},_receiveData:function(_4){var _5={};var _6=this.receiver.getData(_4);this.onReceiveData(_6);_5.items=_6.items;if(!_5.items){_5.items=[];}else{if(!dojo.isArrayLike(_5.items)){_5.items=[_5.items];}}if(_6.idAttribute){_5.identifier=_6.idAttribute;}if(_6.labelAttribute){_5.label=_6.labelAttribute;}this._jsonData=_5;this._loadInProgress=false;delete this._jsonFileUrl;delete this._ccUrl;this._forceLoad();this._handleQueuedFetches();}});}