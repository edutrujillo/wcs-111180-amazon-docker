/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.data.ClientStore"],["require","dojo.data.ItemFileReadStore"],["require","fw.data._BaseStore"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.data.ClientStore"]){_4._hasResource["fw.data.ClientStore"]=true;_4.provide("fw.data.ClientStore");_4.require("dojo.data.ItemFileReadStore");_4.require("fw.data._BaseStore");_4.declare("fw.data.ClientStore",[fw.data._BaseStore,_4.data.ItemFileReadStore],{postscript:function(_7){if(!this._checkProperties()){return;}this._jsonFileUrl=true;this._loadInProgress=true;var _8=this.transport.sendFetchRequest(this.url,{},this.transportOptions,_4.hitch(this,"_receiveData"),function(_9){console.error(_9);});},_receiveData:function(_a){var _b={};var _c=this.receiver.getData(_a);this.onReceiveData(_c);_b.items=_c.items;if(!_b.items){_b.items=[];}else{if(!_4.isArrayLike(_b.items)){_b.items=[_b.items];}}if(_c.idAttribute){_b.identifier=_c.idAttribute;}if(_c.labelAttribute){_b.label=_c.labelAttribute;}this._jsonData=_b;this._loadInProgress=false;delete this._jsonFileUrl;delete this._ccUrl;this._forceLoad();this._handleQueuedFetches();}});}}};});