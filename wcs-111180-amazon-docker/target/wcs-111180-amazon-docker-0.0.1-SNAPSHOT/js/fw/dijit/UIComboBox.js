/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UIComboBox"]){dojo._hasResource["fw.dijit.UIComboBox"]=true;dojo.provide("fw.dijit.UIComboBox");dojo.require("dijit.form.ComboBox");dojo.require("fw.dijit.UIComboBoxMixin");dojo.declare("fw.dijit.UIComboBox",[dijit.form.ComboBox,fw.dijit.UIComboBoxMixin],{});}