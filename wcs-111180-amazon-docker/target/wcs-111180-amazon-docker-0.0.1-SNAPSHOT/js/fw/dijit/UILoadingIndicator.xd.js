/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.UILoadingIndicator"],["require","dijit._Widget"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.UILoadingIndicator"]){_4._hasResource["fw.dijit.UILoadingIndicator"]=true;_4.provide("fw.dijit.UILoadingIndicator");_4.require("dijit._Widget");_4.declare("fw.dijit.UILoadingIndicator",_5._Widget,{loadingMessage:"Loading...",attributeMap:_4.delegate(_5._Widget.prototype.attributeMap,{loadingMessage:{node:"domNode",type:"innerHTML"}}),_subscriptions:null,_overlayElement:null,constructor:function(_7){this._subscriptions=[_4.subscribe(fw.dijit.UILoadingIndicator.startLoadEvent,this,"show"),_4.subscribe(fw.dijit.UILoadingIndicator.stopLoadEvent,this,"hide")];},postCreate:function(){this.inherited(arguments);_4.addClass(this.domNode,"UILoadingIndicator");this.hide();},uninitialize:function(){for(var i=0;i<this._subscriptions.length;i++){_4.unsubscribe(this._subscriptions[i]);}this._subscriptions=null;this.inherited(arguments);},show:function(){_4.style(this.domNode,"display","block");},hide:function(){_4.style(this.domNode,"display","none");}});fw.dijit.UILoadingIndicator.startLoadEvent="fw/wem/StartLoad";fw.dijit.UILoadingIndicator.stopLoadEvent="fw/wem/StopLoad";}}};});