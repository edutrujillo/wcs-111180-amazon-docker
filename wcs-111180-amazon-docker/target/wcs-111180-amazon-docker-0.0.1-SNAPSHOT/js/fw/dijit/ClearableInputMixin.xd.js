/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.ClearableInputMixin"],["require","dijit.form.ValidationTextBox"],["require","fw.dijit.UIValidationMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.ClearableInputMixin"]){_4._hasResource["fw.dijit.ClearableInputMixin"]=true;_4.provide("fw.dijit.ClearableInputMixin");_4.require("dijit.form.ValidationTextBox");_4.require("fw.dijit.UIValidationMixin");_4.declare("fw.dijit.ClearableInputMixin",null,{clearButton:false,postCreate:function(){this.inherited(arguments);if(this.clearButton){var _7=_4.create("a",{"class":"UIInputCross",id:this.id+"_btncross",innerHTML:"&nbsp;",style:{display:"none"}},this.textbox,"after");this.connect(_7,"onclick","_clearInput");_4.addClass(this.textbox.parentNode,"hasClearButton");}},_clearInput:function(){_4.byId(this.id+"_btncross").style.display="none";this.set("value","");this.onClearInput();},_updateClearButton:function(e){var _8,_9,_a;this.inherited(arguments);if(this.clearButton){_8=this.get("displayedValue");if(typeof _8=="undefined"){_8=this.get("value");}_9=_8?"inline":"none";_a=_4.byId(this.id+"_btncross");if(_a){_4.style(_4.byId(this.id+"_btncross"),"display",_9);}}},onClearInput:function(){}});}}};});