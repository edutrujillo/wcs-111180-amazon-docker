/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.ClearableInputMixin"]){dojo._hasResource["fw.dijit.ClearableInputMixin"]=true;dojo.provide("fw.dijit.ClearableInputMixin");dojo.require("dijit.form.ValidationTextBox");dojo.require("fw.dijit.UIValidationMixin");dojo.declare("fw.dijit.ClearableInputMixin",null,{clearButton:false,postCreate:function(){this.inherited(arguments);if(this.clearButton){var _1=dojo.create("a",{"class":"UIInputCross",id:this.id+"_btncross",innerHTML:"&nbsp;",style:{display:"none"}},this.textbox,"after");this.connect(_1,"onclick","_clearInput");dojo.addClass(this.textbox.parentNode,"hasClearButton");}},_clearInput:function(){dojo.byId(this.id+"_btncross").style.display="none";this.set("value","");this.onClearInput();},_updateClearButton:function(e){var _2,_3,_4;this.inherited(arguments);if(this.clearButton){_2=this.get("displayedValue");if(typeof _2=="undefined"){_2=this.get("value");}_3=_2?"inline":"none";_4=dojo.byId(this.id+"_btncross");if(_4){dojo.style(dojo.byId(this.id+"_btncross"),"display",_3);}}},onClearInput:function(){}});}