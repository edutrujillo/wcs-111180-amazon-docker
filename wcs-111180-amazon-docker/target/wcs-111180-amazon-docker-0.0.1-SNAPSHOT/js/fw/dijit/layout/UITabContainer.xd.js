/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.layout.UITabContainer"],["require","dijit.layout.TabContainer"],["require","fw.dijit.layout.UIScrollingTabController"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.layout.UITabContainer"]){_4._hasResource["fw.dijit.layout.UITabContainer"]=true;_4.provide("fw.dijit.layout.UITabContainer");_4.require("dijit.layout.TabContainer");_4.require("fw.dijit.layout.UIScrollingTabController");_4.declare("fw.dijit.layout.UITabContainer",_5.layout.TabContainer,{controllerWidget:"fw.dijit.layout.UIScrollingTabController",_tabHistory:null,constructor:function(_7){this._tabHistory=[];},postCreate:function(){this.inherited(arguments);var tl=this.tablist;this.connect(tl,"onRemoveChild","_popHistory");this.connect(tl,"onSelectChild","_addToHistory");},_removeFromHistory:function(_8){var i;while((i=_4.indexOf(this._tabHistory,_8))>-1){this._tabHistory.splice(i,1);}},_addToHistory:function(_9){this._removeFromHistory(_9);this._tabHistory[this._tabHistory.length]=_9;},_popHistory:function(_a){var _b=this._tabHistory,_c=_b.length;if(_c<1){return;}if(_a==_b[_c-1]){_b.pop();if(--_c>0){this.selectChild(_b[_c-1]);}}else{this._removeFromHistory(_a);}}});}}};});