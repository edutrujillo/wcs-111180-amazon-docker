/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.layout.UITabContainer"]){dojo._hasResource["fw.dijit.layout.UITabContainer"]=true;dojo.provide("fw.dijit.layout.UITabContainer");dojo.require("dijit.layout.TabContainer");dojo.require("fw.dijit.layout.UIScrollingTabController");dojo.declare("fw.dijit.layout.UITabContainer",dijit.layout.TabContainer,{controllerWidget:"fw.dijit.layout.UIScrollingTabController",_tabHistory:null,constructor:function(_1){this._tabHistory=[];},postCreate:function(){this.inherited(arguments);var tl=this.tablist;this.connect(tl,"onRemoveChild","_popHistory");this.connect(tl,"onSelectChild","_addToHistory");},_removeFromHistory:function(_2){var i;while((i=dojo.indexOf(this._tabHistory,_2))>-1){this._tabHistory.splice(i,1);}},_addToHistory:function(_3){this._removeFromHistory(_3);this._tabHistory[this._tabHistory.length]=_3;},_popHistory:function(_4){var _5=this._tabHistory,_6=_5.length;if(_6<1){return;}if(_4==_5[_6-1]){_5.pop();if(--_6>0){this.selectChild(_5[_6-1]);}}else{this._removeFromHistory(_4);}}});}