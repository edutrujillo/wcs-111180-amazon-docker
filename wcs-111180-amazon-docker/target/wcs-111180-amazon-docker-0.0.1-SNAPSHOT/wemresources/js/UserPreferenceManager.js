/*
 *********************************************************************************************
	 UserPreferenceManager is a utility class which provides methods for accessing 
	 User Data Attributes / Preferences that is managed and maintained in the 
	 System  "SystemUserPreferences" Table ;

	 Note: Used by WemContext for preserving user attribute / key pair values  
		 
 *********************************************************************************************/

// Constructor for the UserPreferenceManager
function UserPreferenceManager(args) {
	// Get the CS context path
	this.contextPath = args.csPath;

	// Generate the get and fetch All Keys URL
	this.getAllURL = args.csPath + 'wem/fatwire/wem/ui/getAllUserPreferences';

	// Generate the put add key/value record url
	this.addURL = args.csPath + 'wem/fatwire/wem/ui/addUserPreference';

	// Generate the get and fetch All Keys URL
	this.getURL = args.csPath + 'wem/fatwire/wem/ui/getUserPreference';

	// Generate the remove key /value record URL
	this.removeURL = args.csPath + 'wem/fatwire/wem/ui/removeUserPreference';

	// Hash Table OF Key/Values
	this.hashtable = new Array();

	this.getAllUserPreferences(null);
}

// Methods for UserPreferenceManager class.
UserPreferenceManager.prototype = {

	// Get User Preference attribute value

	// Sets key/name using
	// @param data
	// key: Name of the preference
	// value: Value for the key
	save : function(data) {
		if (data !== 'undefined') {
			var key = data.key;
			var value = data.value;
			var persist = data.persist;
			this.hashtable[key] = value;
		}
	},

	get : function(key) {
		return this.hashtable[key];
	},

	remove : function(data) {
		var key = data.key;
		var rtn = this.hashtable[key];
		this.hashtable[key] = null;
	},

	// returns attributenames.
	// Retrieves attribute names from local storage
	// Local storage retrieved all user preferences save in DB table .
	getUserPreferenceNames : function() {
		var keys = new Array();
		for ( var i in this.hashtable) {
			if (this.hashtable[i] != null)
				keys.push(i);
		}
		return keys;
	},

	getAllUserPreferences : function(siteId) {

		var params = "siteid=" + siteId;
		var args = {
				url : this.getAllURL,
				params : params
			};
		var prefs = this._doAjax(args , false);
		if (prefs && prefs.items) {
			for ( var c = 0; c < prefs.items.length; ++c) {
				var data = prefs.items[c];
				if(data){
					this.hashtable[data.name] = data.value;
				}
			}
		}
	},

	// Add user preference attribute
	addUserPreference : function(siteid, key, value) {

		var params = "name=" + key + "&value=" + value
				+ "&siteid=" + siteid;
		if (siteid == null) {
			params = "name=" + key + "&value=" + value;
		}
		var args = {
				url : this.addURL,
				params : params
			};
		this._doAjax(args , false);
	},

	// Get User Preference attribute value
	getUserPreference : function(siteid, key) {

		var params = "name=" + key + "&siteid=" + siteid;

		var args = {
			url : this.getURL,
			params : params
		};

		// Fetch attribute value by key
		var data = this._doAjax(args , false);
		if(data){
			return data.value;
		}
	},

	// returns attribute value.Retrieves attribute value from browser storage.
	// @param attributename
	removeUserPreference : function(siteid, attributename) {
		var key = attributename;

		// asynchronously but certainly
		var params = "name=" + key + "&siteid=" + siteid;
		var args = {
				url : this.removeURL,
				params : params
			};
		this._doAjax(args , true);
		this.hashtable[key] = null;
	},

	_doAjax : function(args , async) {
		var URL = args.url, params = args.params, xhr;
		
		params = encodeURI(params);
		
		try {
			if (window.XMLHttpRequest) {
				/* Firefox, Safari, Opera... */
				xhr = new XMLHttpRequest();
			} else if (window.ActiveXObject) {
				/* Internet Explorer 6 */
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}

			// Must send synchronous and wait to get the users key/attribute
			// value
			xhr.open("GET", URL + "?" + params, async);
			xhr.send(null);
			if(!async)
			{	
				if (xhr.readyState == 4 && xhr.status == 200) {
					var data = eval("(" + xhr.responseText + ")");	
					return data;
				} else {
					this.error("could not process the request");
				}
			}	
		} catch (err) {
			this.error("could not process the request");
		}
	} ,
	
	debug : function(message) {
		if(window.console) {
			if(window.console.debug) {
				console.debug(message);
			}
		}
	} ,
	
	log : function(message) {
		if(window.console) {
			if(window.console.log) {
				console.log(message);
			}
		}	
	} ,
	error : function(message) {
		if(window.console) {
			if(window.console.error) {
				console.error(message);
			}
		}	
	}

}
