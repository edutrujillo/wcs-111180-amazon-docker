<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.Hashtable"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%-- 
	 Translator Utility for cachetool jsps
	  Searches the keys from resource bundle based on "locale" ( e.g.,ApplicationResource_en_US.properties )
	  if key not found loads the value from "defaultLocale" 
      By default translated text will be stored in request scope with same key,if "render" attribute value is true streams to out object
	  Buffers the keys into Hashtable with the following format: locale:key -> value 
	  'locale' argument takes priority over session locale.  
 	  Optionally provides escapeHtml and escapeJavaScript
 --%>
<%!
/*
keyMap will be caching the locale strings
*/
private Hashtable keyMap = new Hashtable();
private static String applicationResources = "com.fatwire.systemtools.cachetool.ApplicationResources";

private String getTranslation(String key, String locale)
{
	try
	{
		String buildKey = locale+":"+key;
		String transVal = (String)keyMap.get(buildKey);
		if(transVal != null)
		  return transVal;
		transVal = getString(key,locale);
		if(transVal != null)
			keyMap.put(buildKey,transVal);
		return transVal;
	}catch(java.util.MissingResourceException missingResource){
		//log error here	
	}
	catch(Throwable ignore)
	{
	}
	return null;
}

private String getString(String key, String locale)
{
	try
	{
		String transVal = null;
		if(key!=null && locale!=null)
		{
			String[] localeTokenArr =  locale.split("[_]");	
			Locale localeObj = new Locale(localeTokenArr[0],localeTokenArr[1]);
			ResourceBundle bundle = ResourceBundle.getBundle(applicationResources,localeObj);
			transVal = bundle.getString(key);
		}
		return transVal;
	}catch(java.util.MissingResourceException missingResource){
		//log error here	
	}
	catch(Throwable ignore)
	{
	}
	return null;
}
%>
<%
/* providing locale argument to the translator*/
String locale = (String)request.getParameter("locale");
if(locale == null)
	locale = (String)session.getAttribute("locale");

String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;

String key = request.getParameter("key");
try
{	
	request.removeAttribute(key);
	String translation =  getTranslation(key,locale);
	
	if(translation == null)
	{	
		translation = getTranslation(key,defaultLocale);
	}
	if(translation != null )
	{
		if("true".equalsIgnoreCase(request.getParameter("encode"))) //escapeHtml
		{	
			translation = StringEscapeUtils.escapeHtml(translation);
		}
		else if("true".equalsIgnoreCase(request.getParameter("escape"))) //escapeJavaScript
		{ 
			translation = StringEscapeUtils.escapeJavaScript(translation);
		}
	}
	if("true".equalsIgnoreCase(request.getParameter("render")))
	{
		out.write(translation);
	}
	else
	{
		request.setAttribute(key,translation);
	}
}
catch( Throwable ignore)
{
	
}
%>

