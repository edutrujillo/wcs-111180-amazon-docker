<%
String locale = (String)session.getAttribute("locale");
String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
		
if(locale==null || "".equals(locale))
{
	locale = defaultLocale;
}
%>
<html>
<head>
<LINK
	href='<%=request.getContextPath()%>/Xcelerate/data/css/<%=session.getAttribute("locale")%>/cacheTool.css'
	rel="stylesheet" type="text/css">
</head>
<body class="title-background">
<table width="100%">
	<tr>
		<td class="tool-fatwire-logo">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="title.companyLogo"/>
			<jsp:param name="render" value="true"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		</td>
		<td class="tool-title-text">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="title.toolTitle"/>
			<jsp:param name="render" value="true"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		</td>
	</tr>
</table>
</body>
</html>