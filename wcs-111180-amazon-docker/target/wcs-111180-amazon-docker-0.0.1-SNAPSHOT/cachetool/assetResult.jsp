<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.fatwire.assetcache.AssetCacheProvider"%>
<%@page import="com.fatwire.assetcache.AssetCache"%>
<%@page import="java.util.Date"%>
<%@page import="com.fatwire.cache.ehcache.EhCacheObjectCache.Dependency"%>
<%@page import="com.fatwire.cache.Cacheable"%>
<%@page
	import="com.fatwire.cache.ehcache.EhCacheObjectCache.CachedObjectWithDependencies"%>
<%@page import="net.sf.ehcache.Element"%>
<%@page import="net.sf.ehcache.Cache"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@page import="com.fatwire.cache.PageCache"%>
<%@page import="com.fatwire.cache.ehcache.PageCacheProvider"%>
<%@ page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@ page import="java.util.Set"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    
	String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
		
	String authorize = (String) session.getAttribute("userAuthorized");
    if(Boolean.valueOf(authorize))
    {
        String contextPath = request.getContextPath();
        int pagesPerView = 7;
        //define some convenient vars
        String[] rowColors =
            { "tile-row-normal", "tile-row-highlight" };

        Integer rowsPerPageObject =
            (Integer) session.getAttribute("rowsPerPage");
        int rowsPerPage =
            rowsPerPageObject != null ? rowsPerPageObject.intValue()
                : 10;

        String selectPage = request.getParameter("selectPage");
        if(selectPage == null || "".equals(selectPage))
            selectPage = "1";

        int pageNum = Integer.parseInt(selectPage);
        String cacheName = request.getParameter("cachename");
        if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName)))
            cacheName = CacheUtil.SS_CACHE;
            

        // Display a list of keys
        CacheManager cacheMgr = CacheUtil.getCacheManager(cacheName);
        if(null == cacheMgr)
		{
            	%><jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="assetCacheDep.noCacheManager"/>
					<jsp:param name="render" value="true"/>
					<jsp:param name="encode" value="true"/>
				</jsp:include><%
		}
        else
        {
        AssetCache accessor = AssetCacheProvider.getCache(cacheName);
        
        if(accessor.isAvailable())
        {
            Set<String> keyIterable = null;
            keyIterable = (Set<String>) session.getAttribute("result");
            int totalCount =
                ((Integer) session.getAttribute("totalCount"))
                    .intValue();

            //reset pageNum after flushing last page items
            if(totalCount / rowsPerPage == (pageNum - 1)
                && (totalCount % rowsPerPage == 0))
            {
                pageNum = 1;
            }
            
            Map<String,Cache> assetCaches = new HashMap<String,Cache>();
            for(String cname : cacheMgr.getCacheNames())
            {
                if(!cname.equals("AssetCache") && cname.startsWith("AssetCache"))
                {
                    assetCaches.put(cname.substring("AssetCache".length()), cacheMgr.getCache(cname));
                }
            }

            Cache assetCache = cacheMgr.getCache("AssetCache");
            Cache depRep = cacheMgr.getCache("dependencyRepository");            
            int count = 0;
            StringBuilder rows = new StringBuilder();
            int lowerBound = rowsPerPage * (pageNum - 1);
            int upperBound = lowerBound + rowsPerPage;
            for(String key : keyIterable)
            {
                Element e = null;
                String[] a = key.split(":");
                Cache keyInCache = assetCaches.get(a[0]);
 				e = (null != keyInCache) ? keyInCache.getQuiet(key) : assetCache.getQuiet(key);
 				
                if(e != null)
                {
                    count++;
                    if(count > lowerBound)
                    {
                        if(count <= upperBound)
                        {
                            CachedObjectWithDependencies cbwd =
                                (CachedObjectWithDependencies) e
                                    .getObjectValue();
                            Cacheable pc = (Cacheable) cbwd.getCachedObject();
                            
                            //build deps output
                            StringBuilder deps = new StringBuilder();
                            StringBuilder depsbox = new StringBuilder();
                            for(Dependency d : cbwd.getDeps())
                            {
                                String dep = d.getKey();
                                if(!dep.startsWith("pagename"))
                                {
                                    //check if this dep is valid or not
                                    Element ex = depRep.getQuiet(dep);
                                    if(!d.equals((ex == null ? null
                                        : ex.getObjectValue())))
                                    {
                                        deps.append(
                                            "<div class=invalidateDep>")
                                            .append(dep)
                                            .append(
                                                "<sup class=\"supText\">")
                                            .append(
                                                cbwd.getDepGenCount(dep))
                                            .append("</sup></div>");
                                        depsbox
                                            .append(
                                                "<div class='dep'><input type='checkbox' class='checkboxLookup' name='dep' id='dep' DISABLED value='")
                                            .append(dep)
                                            .append("'></div>");
                                    }
                                    else
                                    {
                                        deps.append("<div class='dep'>")
                                            .append(dep)
                                            .append(
                                                "<sup class=\"supText\">")
                                            .append(
                                                cbwd.getDepGenCount(dep))
                                            .append("</sup></div>");
                                        depsbox
                                            .append(
                                                "<div class='dep'><input type='checkbox' class='checkboxLookup' onclick=\"toggleParentOnCheck('dep',this);\" name='dep' id='"+ dep+ "' value='")
                                            .append(dep)
                                            .append("'></div>");
                                    }
                                }
                            }

                            String keyBox =
                                "<input type='checkbox' onclick=\"toggleParentOnCheck('key',this);\" name='key' id='"+ key+"' value='"
                                    + key + "'>";
                            String bgc = rowColors[count % 2];
                            if(deps.indexOf("invalidateDep") != -1)
                            {
                                bgc = "strikeBackGround";
                            }
                            
                            long hitCount = e.getHitCount();
                            Date lastAccessDate =
                                new Date(e.getLastAccessTime());
                            Date createdTime =
                                new Date(e.getCreationTime());

                            rows.append("<tr class=")
                                .append(bgc)
                                .append(
                                    " ><td><BR/></td><td align=center>")
                                .append(keyBox)
                                .append(
                                    "</td><td class='break-all-line padded'>")
                                .append(key)
                                .append("</td>");
                            rows.append(
                                "<td class=padded align=center>")
                                .append(hitCount).append("</td>");
                            rows.append("<td class=padded>")
                                .append(CacheUtil.getLocalizedDate(createdTime,locale)).append("</td>");
                            rows.append("<td class=padded>")
                                .append(CacheUtil.getLocalizedDate(lastAccessDate,locale)).append("</td>");

                            
                            rows.append("<td class='padded nowrap'>")
                                .append(deps.toString())
                                .append("</td>")
                                .append("<td align=center>")
                                .append(depsbox.toString())
                                .append(
                                    "</td><td><BR/></td></tr>");
                        }
                    }
                }
            }
%>
<form name="pageForm" method="post">
<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
<table>
	<%
	    if(totalCount > 0)
	            {
	%>
	
	<tr>
		<td align="left">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCache.noItemsInCache"/>
		</jsp:include>
		<%=((String)request.getAttribute("assetCache.noItemsInCache")).replace("{0}",Integer.toString(totalCount))%>
		</td>
		<td>
		<div id="searchResults" align="right"></div>
		</td>
	</tr>
	<tr>
		<td align="left">
			<div onclick="submitSearchForm('flush', '<%=pageNum%>');">
			<jsp:include page="/cachetool/addTextButton.jsp">
				<jsp:param value="assetCacheDep.button.flushCache" name="key"/>
			</jsp:include>
			</div>
		</td>
		<td><div onclick="submitAssetLookUpForm();" style='float:right'>
			<jsp:include page="/cachetool/addTextButton.jsp">
				<jsp:param value="assetCacheDep.button.assetLookUp" name="key"/>
			</jsp:include>
		</div></td>
	</tr>
	<tr>
		<td colspan=2>
		<table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
			<tr>
				<td></td>
				<td height="1" valign="TOP" class="tile-dark"><img height="1"
					width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
			<tr>
				<td width="1" valign="top" class="tile-dark"><br />
				</td>
				<td>
				<table cellspacing="0" cellpadding="0" bgcolor="#ffffff"
					width="100%">
					<tr>
						<td colspan="9" class="tile-highlight"><img
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'
							width="1" height="1"></td>
					</tr>
					<tr>
						<td class="tile-a"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b" align="center"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
							<input type='checkbox' class='checkboxLookup' name='selectAll' 
							id='selectAll' onclick="checkAll('key');"/>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
							align="center" >
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.key"/>
							<jsp:param name="render" value="true"/>
							<jsp:param name="encode" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.hitCount"/>
							<jsp:param name="render" value="true"/>
							<jsp:param name="encode" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif' align="center" >
						<span class="new-table-title">
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCache.createdTime"/>
							<jsp:param name="render" value="true"/>
							<jsp:param name="encode" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif' align="center" >
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.lastAccessTime"/>
							<jsp:param name="render" value="true"/>
							<jsp:param name="encode" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.dependency"/>
							<jsp:param name="render" value="true"/>
							<jsp:param name="encode" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
							<input type='checkbox' class='checkboxLookup' name='selectAll_1' 
							id='selectAll_1' onclick="checkAll('dep');"/></td>
						<td
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
							class="tile-c">&nbsp;</td>
					</tr>
					<%=rows.toString()%>
				</table>
				</td>
				<td width="1" valign="top" class="tile-dark"><br />
				</td>
			</tr>
			<tr>
				<td height="1" valign="TOP" class="tile-dark" colspan="3"><img
					height="1" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
			</tr>
			<tr>
				<td></td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'><img
					height="5" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
		</table>
		</td>
	</tr>
	
	<%
	    }
	            else
	            {
	%>
	<tr>
		<td class="message-bg-color">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCacheDep.noitemsFound"/>
			<jsp:param name="render" value="true"/>
			<jsp:param name="encode" value="true"/>
		</jsp:include>
		</td>
		<%
		    }
		%>
	
</table>
</form>
</body>

<script language="javascript" type="text/javascript">
    var page = new Paging({    renderTop:"searchResults",
        recordCount:<%=totalCount%>,
        onSuccess:'gotoPage',
        currentPage: <%=pageNum%>,
        rowsPerPage:<%=rowsPerPage%>,
        pagePerView:<%=pagesPerView%>
    });
</script>
<%
    	}
      }
    }
   else
   {
%>
	<jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="common.accessError"/>
		<jsp:param name="render" value="true"/>
	</jsp:include>
<%
    }
%>