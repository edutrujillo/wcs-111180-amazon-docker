<%@page import="java.io.FileReader"%>
<%@page import="java.util.Properties"%>
<%@page import="com.openmarket.Satellite.Application"%>
<%@page import="com.openmarket.Satellite.RequestContext"%>
<%@page import="com.openmarket.Satellite.servlet.Flush"%>
<%@page import="COM.FutureTense.Servlet.ServletRequest"%>
<%@page import="org.springframework.web.context.support.ServletContextResource"%>
<%
    String authorize = (String) session.getAttribute("userAuthorized");
    if(Boolean.valueOf(authorize))
    {
        Application app = new Flush().constructApp(System.getProperty("java.tmpdir"));
        ServletRequest req = new ServletRequest(null, request,response);
        RequestContext rc = new RequestContext(req, app.getConfigInfo(), app.getHostInfo(), true);
        rc.setResponseContentType("text/html; charset=UTF-8");
        ServletContextResource resource =
                new ServletContextResource(getServletConfig().getServletContext(), "/WEB-INF/classes/satellite.properties");
        if(resource != null && resource.getFile() !=null) {
            Properties prop = new Properties();
            prop.load(new FileReader(resource.getFile()));
            rc.getRequestParameters().setValString("username", prop.getProperty("username") );
            rc.getRequestParameters().setValString("method", "POST");
            rc.getRequestParameters().setValString("reset", "true");
            rc.getRequestParameters().setValString("password", prop.getProperty("password") );
            app.execute(rc);
        }
    }
    else
    {
%>
<jsp:include page="/cachetool/translator.jsp">
    <jsp:param name="key" value="common.accessError"/>
    <jsp:param name="render" value="true"/>
    <jsp:param name="encode" value="true"/>
</jsp:include><%
    }
%>