<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="net.sf.ehcache.Element"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Set"%>
<%@page import="net.sf.ehcache.Cache"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@ page import="com.fatwire.cache.Cacheable"%>
<%@ page import="com.fatwire.cache.PageCache"%>
<%@ page import="com.fatwire.cache.PageCache.QueryOp"%>
<%@ page
	import="com.fatwire.cache.ehcache.EhCacheObjectCache.CachedObjectWithDependencies"%>
<%@ page import="com.fatwire.cache.ehcache.PageCacheProvider"%>
<%@ page import="java.util.regex.Pattern"%>
<%@ page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
    String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
	
	String authorize = (String) session.getAttribute("userAuthorized");
    if("true".equals(authorize))
    {
        String contextPath = request.getContextPath();
%>
<body>
<%
    //default
        int pagesPerView = 7;
        //define some convenient vars
        String[] rowColors =
            { "tile-row-normal", "tile-row-highlight" };

        //get param values
        String action = request.getParameter("action");
        String searchKey = request.getParameter("searchKey");
		searchKey = StringEscapeUtils.escapeHtml(searchKey);
        String matchTypeSelect =
            request.getParameter("matchTypeSelect");

        int rowsPerPage =
            ((Integer) session.getAttribute("rowsPerPage")).intValue();
        String selectPage = request.getParameter("selectPage");
        if(selectPage == null)
            selectPage = "1";
        int pageNum = Integer.parseInt(selectPage);
        String cacheName = request.getParameter("cachename");
        if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName)))
            cacheName = CacheUtil.SS_CACHE;
        QueryOp queryOp =
            "ALL".equals(matchTypeSelect) ? QueryOp.ALL : QueryOp.ANY;
        PageCache accessor = PageCacheProvider.getCache(cacheName);
        CacheManager cacheMgr = CacheUtil.getCacheManager(cacheName);
        if(null == cacheMgr)
        {
		%><jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCacheDep.noCacheManager"/>
			<jsp:param name="render" value="true"/>
		</jsp:include><%
		}
        else
        {
            Cache pageByQryCache = cacheMgr.getCache("pageByQry");
            Cache depRep = cacheMgr.getCache("dependencyRepository");

            int totalCount = 0;
            String[] deps = null;
            if(searchKey == null || "".equals(searchKey))
            {
                deps = new String[] { "*" };
            }
            else
            {
                deps = searchKey.split(",");
            }
            int inputNum = deps.length;
            boolean refresh = false;

            Integer sessionInputNum =
                (Integer) session.getAttribute("inputNum");
            if(sessionInputNum != null
                && deps.length != sessionInputNum.intValue())
            { //mismatch between input and session value, always get new items from cache
                refresh = true;
                pageNum = 1;
            }
            session.setAttribute("inputNum", new Integer(inputNum));
%>
<table id="noItemFoundTable" name="noItemFoundTable" border="0"
	style='visibility: hidden'>
	<tr>
		<td class="message-bg-color">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCacheDep.noitemsFound"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</td>
	</tr>
</table>
<form name="pageForm" method="post">
<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
<table id="searchResultTable" name="searchResultTable" border="0">
	<tr>
		<td align="left">
		<b><jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCacheDep.Results"/>
			<jsp:param name="render" value="true"/>
		</jsp:include> :</b>
		</td>
		<td>
		<div id="searchResults" name="searchResults" align="right"></div>
		</td>
	</tr>
	<tr>
		<td align="left">
		<div onclick="submitSearchForm('flush', '<%=pageNum%>');">
				<jsp:include page="/cachetool/addTextButton.jsp">
					<jsp:param  name="key" value="assetCacheDep.button.flushCache"/>
				</jsp:include>
		</div>
		</td>
		<td><div onclick="submitPageForm();" style='float:right'>
				<jsp:include page="/cachetool/addTextButton.jsp">
					<jsp:param  name="key" value="revDepPage.pageLookUp"/>
				</jsp:include>
				</div>
		</td>
		
	</tr>
	<tr>
		<td colspan="2">
		<table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
			<tr>
				<td></td>
				<td height="1" valign="TOP" class="tile-dark"><img height="1"
					width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
			<tr>
				<td width="1" valign="top" class="tile-dark"><br>
				</td>
				<td>
				<table cellspacing="0" cellpadding="0" bgcolor="#ffffff"
					width="100%">
					<tr>
						<td colspan="10" class="tile-highlight"><img
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'
							width="1" height="1"></td>
					</tr>
					<tr>
						<td class="tile-a"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
							width="1%">&nbsp;</td>
						<td class="tile-b" align="center"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'><input type='checkbox' class='checkboxLookup'
							name='selectAll' id='selectAll' onclick="checkAll('key');" /></td>
						<td class="tile-b" nowrap="nowrap"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.key"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b" nowrap="nowrap"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.hitCount"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b" background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b" nowrap="nowrap"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.lastAccessTime"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b" nowrap="nowrap"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="revDepPage.expireTime"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b" nowrap="nowrap"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.dependency"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b" align="center" nowrap="nowrap"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
							width="3%"><input type='checkbox' class='checkboxLookup'
							name='selectAll_1' id='selectAll_1' onclick="checkAll('dep');" />
						</td>
						<td
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
							class="tile-c" width="">&nbsp;</td>
					</tr>
					<%
					    int count = 0;
					            int lowerBound = rowsPerPage * (pageNum - 1);
					            int upperBound = lowerBound + rowsPerPage;
					            for(int i = 0; i < inputNum; i++)
					            {
					                String dep = deps[i];
					                Set<String> pageKeys = null;
					                List<String> dependencies =
					                    "ALL".equals(matchTypeSelect) ? Arrays.asList(deps)
					                        : Arrays.asList(dep.trim());
					                if("paging".equals(action) && !refresh)
					                {
					                    pageKeys =
					                        (Set<String>) session
					                            .getAttribute("result" + i);
					                }
					                else
					                {
					                    pageKeys =
					                        accessor
					                            .getKeysByDeps(dependencies, queryOp, 0);
					                    session.setAttribute("result" + i, pageKeys);
					                }

					                totalCount += pageKeys.size();

					                //reset pageNum after flushing last page items
					                if(totalCount / rowsPerPage == (pageNum - 1)
					                    && (totalCount % rowsPerPage == 0))
					                {
					                    pageNum = 1;
					                }

					                if(count >= lowerBound && count <= upperBound)
					                {
					%>
					<tr>
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="revDepPageResult.pageDependencyMsg"/>
							<jsp:param name="encode" value="true"/>
						</jsp:include>
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="revDepPageResult.pagesDependencyMsg"/>
							<jsp:param name="encode" value="true"/>
						</jsp:include>
						<%
							String depString = "<b><FONT COLOR=blue>"+ (queryOp == QueryOp.ANY ? dep : searchKey) + "</FONT></b>";
							StringBuilder sb = new StringBuilder();
							if(pageKeys.size() > 1 )
							{
								sb.append(((String)request.getAttribute("revDepPageResult.pagesDependencyMsg")).replace("{0}", Integer.toString(pageKeys.size())).replace("{1}", depString)); 
							}
							else
							{
								sb.append(((String)request.getAttribute("revDepPageResult.pageDependencyMsg")).replace("{0}", Integer.toString(pageKeys.size())).replace("{1}", depString));
							}
						%>
						<td colspan="10" align="center"><%=sb.toString()%></td>
					</tr>
					<%
					    }
					                if(!pageKeys.isEmpty())
					                {
					                    for(String key : pageKeys)
					                    {
											key = StringEscapeUtils.escapeHtml(key);
					                        Element e = pageByQryCache.getQuiet(key.trim());
					                        if(e != null)
					                        {
					                            count++;
					                            if(count > lowerBound)
					                            {
					                                if(count <= upperBound)
					                                {

					                                    CachedObjectWithDependencies cbwd =
					                                        (CachedObjectWithDependencies) e
					                                            .getObjectValue();
					                                    Cacheable pc =
					                                        (Cacheable) cbwd
					                                            .getCachedObject();
					                                    int hash =
					                                        e.getObjectValue().hashCode();
					                                    long hitCount = e.getHitCount();
					                                    String lastAccessDate =
					                                        CacheUtil.getLocalizedDate(new Date(e.getLastAccessTime()),locale);

					                                    StringBuilder props =
					                                        new StringBuilder();
					                                    props.append(CacheUtil.getLocalizedDate(pc.getExpireTime(),locale));

					                                    StringBuilder sbDeps =
					                                        new StringBuilder();
					                                    StringBuilder sbDepsbox =
					                                        new StringBuilder();
					                                    for(String d : pc.getDeps())
					                                    {
					                                        if(depRep.getQuiet(d) == null) //or compare genCount if not null. later...
					                                        {
					                                            sbDeps
					                                                .append(
					                                                    "<div class=invalidateDep>")
					                                                .append(d)
					                                                .append(
					                                                    "<sup class=\"supText\">")
					                                                .append(
					                                                    cbwd.getDepGenCount(d))
					                                                .append("</sup>")
					                                                .append("</div>");
					                                            sbDepsbox
					                                                .append(
					                                                    "<div class='dep'><input type='checkbox' class='checkboxLookup' name='dep' id='dep' DISABLED value='"
					                                                        + d + "'>")
					                                                .append("</div>");
					                                        }
					                                        else
					                                        {
					                                            sbDeps
					                                                .append("<div class='dep'>");
					                                            if(queryOp == QueryOp.ANY)
					                                            {
					                                                if(!"*".equals(dep)
					                                                    && (Pattern
					                                                        .matches(dep, d)
					                                                        || d.indexOf(dep) != -1 || Pattern
					                                                        .matches(".*"
					                                                            + dep
					                                                            + ".*", d)))
					                                                    sbDeps
					                                                        .append(
					                                                            "<font class=matchDepColor>")
					                                                        .append(d)
					                                                        .append(
					                                                            "</font>");
					                                                else
					                                                    sbDeps.append(d);
					                                            }
					                                            else if(queryOp == QueryOp.ALL)
					                                            {
					                                                boolean matched = false;
					                                                for(String input : deps)
					                                                {
					                                                    if(!"*"
					                                                        .equals(input)
					                                                        && (Pattern
					                                                            .matches(
					                                                                input,
					                                                                d)
					                                                            || d.indexOf(input) != -1 || Pattern
					                                                            .matches(
					                                                                ".*"
					                                                                    + input
					                                                                    + ".*",
					                                                                d)))
					                                                        matched = true;
					                                                }
					                                                if(matched)
					                                                    sbDeps
					                                                        .append(
					                                                            "<font class=matchDepColor>")
					                                                        .append(d)
					                                                        .append(
					                                                            "</font>");
					                                                else
					                                                    sbDeps.append(d);
					                                            }
					                                            sbDeps
					                                                .append(
					                                                    "<sup class=\"supText\">")
					                                                .append(
					                                                    cbwd.getDepGenCount(d))
					                                                .append("</sup>")
					                                                .append("</div>");
					                                            sbDepsbox
					                                                .append(
					                                                    "<div class='dep'><input type='checkbox' onclick=\"toggleParentOnCheck('dep',this);\" class='checkboxLookup' name='dep' id='"
					                                                        + d
					                                                        + "' value='"
					                                                        + d + "'>")
					                                                .append("</div>");
					                                        }
					                                    }
					                                    String bgc = rowColors[count % 2];
					                                    if(sbDeps.indexOf("invalidateDep") != -1)
					                                        bgc = "strikeBackGround";
					                                    %>
					                                    <jsp:include page="/cachetool/translator.jsp">
					                        				<jsp:param name="key" value="assetCacheDep.viewPageDetails"/>
					                        			</jsp:include>
					                        			<%
					                                    String pageLink =
					                                        "<div style='text-align:center;'><a href='"
					                                            + contextPath
					                                            + "/cachetool/pageContent.jsp?cachename="
					                                            + cacheName
					                                            + "&"
					                                            + key
					                                            + "' onclick=\"return pagePopup('"
					                                            + contextPath
					                                            + "/cachetool/pageContent.jsp?"
					                                            + key
					                                            + "')\">"
					                                            + request.getAttribute("assetCacheDep.viewPageDetails")
					                                            + "</a></div>";
														pageLink = StringEscapeUtils.escapeHtml(pageLink);
					%>
					<tr class='<%=bgc%>'>
						<td><BR />
						</td>
						<td align=center><input type='checkbox' name='key' id='<%=key%>'
							value='<%=key%>' onclick="toggleParentOnCheck('key',this);"></td>
						<td class='break-all-line padded'><%=key%><%=pageLink%></td>
						<td class=padded align=center><%=hitCount%></td>
						<td style='width:30px'><br/></td>
						<td class=padded><%=lastAccessDate%></td>
						<td class='padded'><%=props.toString()%></td>
						<td class='padded nowrap'><%=sbDeps.toString()%></td>
						<td align=center><%=sbDepsbox.toString()%></td>
						<td><BR />
						</td>
					</tr>
					<%
					    }
					                            }
					                        }
					                    }//end of pageKeys loop   
					                }
					                if(queryOp == QueryOp.ALL)
					                    break;
					            }//end of deps loop
					%>
				</table>
				</td>
				<td width="1" valign="top" class="tile-dark"><br />
				</td>
			<tr>
				<td height="1" valign="TOP" class="tile-dark" colspan="3"><img
					height="1" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
			</tr>
			<tr>
				<td></td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'><img
					height="5" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
<script language="javascript" type="text/javascript">
     if (<%=totalCount%> == 0 )
	{
		document.getElementById('searchResultTable').style.visibility='hidden';
		document.getElementById('noItemFoundTable').style.visibility='visible';
	}
	else
	{	var page = new Paging({	renderTop:"searchResults",
							recordCount:<%=totalCount%>,
							onSuccess:'gotoPage',
							currentPage: <%=pageNum%>,
							rowsPerPage:<%=rowsPerPage%>,
							pagePerView:<%=pagesPerView%>
						 });
	}
</script>
<%
    }
    }
    else
    {
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="common.accessError"/>
		<jsp:param name="render" value="true"/>
	</jsp:include>
<%    }
%>