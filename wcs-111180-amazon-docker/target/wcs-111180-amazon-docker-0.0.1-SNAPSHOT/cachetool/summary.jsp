<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="net.sf.ehcache.Statistics"%>
<%@page import="net.sf.ehcache.Cache"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@page import="java.util.Calendar"%>
<%@ page import="java.net.URL"%>
<%@ page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="java.util.Date"%>
<%@ page import="com.fatwire.wem.sso.SSO" %>
<%!

Map<String,String> cacheNamesTrans = new HashMap<String,String>();
/**
 * This method returns the translated cacheNames.
 */


private String getCacheNamesForDisplay(String cacheName)
{
	if(cacheName.contains("AssetCache"))
	{
		return "AssetCache".equals(cacheName) ? cacheNamesTrans.get(cacheName) : cacheNamesTrans.get(cacheName)+" ("
                        + cacheName.substring("AssetCache".length()) + ")";
	}
	return cacheNamesTrans.get(cacheName); 
}
%>
<jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="cacheName.ticketcache"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include><%
	cacheNamesTrans.put("ticketcache",(String)request.getAttribute("cacheName.ticketcache"));
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="cacheName.sessioncache"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include><%
	cacheNamesTrans.put("sessioncache",(String)request.getAttribute("cacheName.sessioncache"));
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="cacheName.notifier"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include>
<%-- localized cache names for dependencyRepository, pageByQry, AssetCache --%>
	<jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="cacheName.dependencyRepository"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include><%
	cacheNamesTrans.put("dependencyRepository",(String)request.getAttribute("cacheName.dependencyRepository"));
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="cacheName.pageByQry"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include><%
	cacheNamesTrans.put("pageByQry",(String)request.getAttribute("cacheName.pageByQry"));
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="cacheName.AssetCache"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include><%
	cacheNamesTrans.put("AssetCache",(String)request.getAttribute("cacheName.AssetCache"));
	%><jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="cacheName.urlCache"/>
	<jsp:param name="encode" value="true"/>
	</jsp:include><%
	cacheNamesTrans.put("URLCache",(String)request.getAttribute("cacheName.urlCache"));
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="common.selectCacheMsg"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include>
<%

	String cacheName = request.getParameter("cachename");
	String cachesToClear = StringEscapeUtils.escapeHtml(request.getParameter("cachesToClear"));
	boolean usingTraditionalCache = cachesToClear!=null && CacheUtil.CS_CACHE.equals(cacheName);
    String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
	String[] localeToken = locale.split("[_]");
	
	Locale localeObj = new Locale(localeToken[0],localeToken[1]);
	String authorize = (String) session.getAttribute("userAuthorized");
    if(Boolean.valueOf(authorize))
    {
		String contextPath = request.getContextPath();
        String scheme = request.getScheme();
        String hostName = request.getServerName();
        int portNum = request.getServerPort();
      
		StringBuffer systemDetails = new StringBuffer();
		systemDetails.append(scheme).append("://")
            .append(hostName).append(":").append(portNum);
        
		cacheName = StringEscapeUtils.escapeHtml(cacheName);
		
		if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName) || CacheUtil.CAS_CACHE.equals(cacheName)))
		    cacheName = CacheUtil.SS_CACHE;
		
		ArrayList<Cache> allCaches = new ArrayList<Cache>();
		if(cacheName.equals(CacheUtil.CAS_CACHE)){
			CacheManager cacheMgr = CacheUtil.getCacheManager(cacheName);
	        
	        for(String cache : cacheMgr.getCacheNames())
	        {
	        	if(!("dependencyRepository".equalsIgnoreCase(cache) || "notifier".equalsIgnoreCase(cache)))
					allCaches.add(cacheMgr.getCache(cache));
	        }
		} 
		else {
			CacheManager cacheMgr = CacheUtil.getCacheManager(cacheName);
	        if(CacheUtil.isPageCacheEnabled())
	            allCaches.add(cacheMgr.getCache("pageByQry"));
	        
	        for(String cache : cacheMgr.getCacheNames())
	            if(!"pageByQry".equals(cache)&&!"notifier".equals(cache))
	            	allCaches.add(cacheMgr.getCache(cache));
    	}
	        int value = 4 + 3 * allCaches.size();
	        String[][] summaryData = new String[12][1 + allCaches.size()];
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.totalCount"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[0][0] =  (String)request.getAttribute("summary.totalCount"); //Total Count
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.memoryStoreCount"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
			summaryData[1][0] = (String)request.getAttribute("summary.memoryStoreCount"); //Memory Store Count
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.diskStoreCount"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[2][0] =  (String)request.getAttribute("summary.diskStoreCount"); //Disk Store Count
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.cacheHits"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[3][0] =  (String)request.getAttribute("summary.cacheHits"); //Cache Hits
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.inMemoryHits"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[4][0] =  (String)request.getAttribute("summary.inMemoryHits"); //In Memory Hits
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.onDiskHits"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[5][0] =  (String)request.getAttribute("summary.onDiskHits");//On Disk Hits
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.cacheMisses"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[6][0] =  (String)request.getAttribute("summary.cacheMisses");//Cache Misses
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.evictionCount"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[7][0] =  (String)request.getAttribute("summary.evictionCount");//Eviction Count
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.averageGetTime"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[8][0] =  (String)request.getAttribute("summary.averageGetTime");//Average Get Time
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.maxElementsInMemory"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[9][0] =  (String)request.getAttribute("summary.maxElementsInMemory"); //Max Elements In Memory
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.maxElementsOnDisk"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[10][0] = (String)request.getAttribute("summary.maxElementsOnDisk"); //Max Elements On Disk
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="summary.overflowToDisk"/>
				<jsp:param name="encode" value="true"/>
			</jsp:include><%
	        summaryData[11][0] = (String)request.getAttribute("summary.overflowToDisk");//Overflow To Disk

	        int count = 1;
	        for(Cache cache : allCaches)
	        {
	            Statistics cacheStatistics = cache.getStatistics();
	            summaryData[0][count] = cacheStatistics.getObjectCount() + "";//Total Count
	            summaryData[1][count] =
	                cacheStatistics.getMemoryStoreObjectCount() + "";//Memory Store Count
	            summaryData[2][count] =
	                cacheStatistics.getDiskStoreObjectCount() + "";//Disk Store Count
	            summaryData[3][count] = cacheStatistics.getCacheHits() + "";//Cache Hits
	            summaryData[4][count] =
	                cacheStatistics.getInMemoryHits() + "";//In Memory Hits
	            summaryData[5][count] =
	                cacheStatistics.getOnDiskHits() + "";//On Disk Hits
	            summaryData[6][count] =
	                cacheStatistics.getCacheMisses() + "";//Cache Misses
	            summaryData[7][count] =
	                cacheStatistics.getEvictionCount() + "";//Eviction Count
	            summaryData[8][count] =
	                cacheStatistics.getAverageGetTime() + "";//Average Get Time
	            summaryData[9][count] =
	                cache.getCacheConfiguration().getMaxElementsInMemory()
	                    + "";//Max Elements In Memory
	            summaryData[10][count] =
	                cache.getCacheConfiguration().getMaxElementsOnDisk()
	                    + "";//Max Elements On Disk
	            summaryData[11][count] =
	                cache.getCacheConfiguration().isOverflowToDisk() + "";//Overflow To Disk
	            count++;
	        }
%>
<html>
<head>

<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css'
	rel="stylesheet" type="text/css">
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css'
	rel="stylesheet" type="text/css">
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/cacheTool.css'
	rel="stylesheet" type="text/css">	
	
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.selectCacheMsg"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.clearCacheConfirmMsg"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.clearSessionCacheConfirmMsg"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<%
if(usingTraditionalCache) {
%>
<script type="text/javascript" src='<%=contextPath + "/js/dojo/dojo.js"%>'></script>
<%}%>
<script language="javascript" type="text/javascript">
<%
if(usingTraditionalCache) {
%>

function clearCoResCache() {

	dojo.require("fw.xhr");
	xhrArgs = {
		url: '<%=contextPath%>/cachetool/clearTraditionalCache.jsp',
		content: {}
		};
	dojo.xhrPost(xhrArgs).then(function(result) {
		clearSPCMessageId.style.display = 'block';
		progressID.style.display = 'none';
		
	}, function(e) {
		progressID.style.display = 'none';
		errorMessageID.style.display = 'block';
		errorMessageID.innerHTML =e.message;
	});
}

function clearSystemPageCache() 
{
	var agree=confirm("<%=request.getAttribute("common.clearCacheConfirmMsg")%>"),xhrArgs,deferred, clearSPCMessageId, errorMessageID,progressID;
	if(agree) {

		clearSPCMessageId = dojo.byId('clearSPCMessageId');
		errorMessageID = dojo.byId('errorMessageID');
		progressID =dojo.byId('progressID');
		
		clearSPCMessageId.style.display = 'none';
		errorMessageID.style.display = 'none';
		progressID.style.display='block';
		
		dojo.require("fw.xhr");

		xhrArgs = {
			url: '<%=contextPath%>/CatalogManager',
			content: {'ftcmd' :'flushpages'}
			};
		dojo.xhrPost(xhrArgs).then(function(result) {
			clearCoResCache();
		}, function(e) {
			progressID.style.display = 'none';
			errorMessageID.style.display = 'block';
			errorMessageID.innerHTML =e.message;
		});
	}
}
<%}%>
function writeToDisk()
{
	if(!checkSelected(document.summaryForm.WriteCache))
		{
		alert("<%=request.getAttribute("common.selectCacheMsg")%>");
		return;
		}
	document.summaryForm.cacheAction.value = '<%= CacheUtil.FLUSH%>';
	document.summaryForm.submit();
}

function checkSelected(cache)
{
	var len = cache.length;
	for( var i=0;i<len;i++)
	 if(cache[i].checked) return true;
	return false;
}
function checkSessionCacheSelected(cache)
{
	var len = cache.length;
	for( var i=0;i<len;i++)
	 if(cache[i].value=='sessioncache' & cache[i].checked) return true;
	return false;
}
function clearCache() {
	if(!checkSelected(document.summaryForm.FlushCache))
		{
		alert("<%=request.getAttribute("common.selectCacheMsg")%>");
		return;
		}
	var agree;
	if(checkSessionCacheSelected(document.summaryForm.FlushCache))
	{
		agree=confirm("<%=request.getAttribute("common.clearSessionCacheConfirmMsg")%>");
	}
	else 
	{
		agree=confirm("<%=request.getAttribute("common.clearCacheConfirmMsg")%>");
	}
	if (agree)
	{
		document.summaryForm.cacheAction.value = '<%= CacheUtil.CLEAR%>';
		document.summaryForm.submit();
	}
}
</script>
</head>
<body>
<form name="summaryForm" method="post" action='<%=contextPath%>/cachetool/manageEhcache.jsp' >
<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
<input type="hidden" name="cacheAction" value="flush" />
<input type="hidden" name="cachename" value='<%=cacheName%>' />
<%
if(usingTraditionalCache) {%><input type="hidden" name="cachesToClear" value='<%=cachesToClear%>' /><%
}
%>
<table border="0" bgcolor="#ffffff">
	<tr>
		<td height="3px"></td>
	</tr>
	<tr>
		<td class="sub-title-text">
		<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="summary.summary"/>
					<jsp:param name="render" value="true"/>
					<jsp:param name="encode" value="true"/>
		</jsp:include></td>
	</tr>
	<jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="common.systemDetails"/>
		<jsp:param name="encode" value="true"/>
	</jsp:include>
	<%
	String systemDetailsKey = (String)request.getAttribute("common.systemDetails");
	systemDetailsKey = systemDetailsKey.replace("{0}",systemDetails.toString()).replace("{1}",CacheUtil.getLocalizedDate(Calendar.getInstance().getTime(),locale));
	%>
	<tr>
		<td class="system-info-text"><%=systemDetailsKey%></td>
	</tr>
	<tr>
		<td height="7px"></td>
	</tr>
	<tr>
		<td>
		<table BORDER="0" CELLSPACING="0" CELLPADDING="0">
			<tr>
				<td></td>
				<td class="tile-dark" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1"
					src='<%=contextPath%>/Xcelerate//graphics/common/screen/dotclear.gif' /></td>
				<td></td>
			</tr>
			<tr>
				<td class="tile-dark" VALIGN="top" WIDTH="1" NOWRAP="nowrap"><BR/></td>
				<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0"
					bgcolor="#ffffff">
					<tr>
						<td colspan='<%=value%>' class="tile-highlight"><IMG
							WIDTH="1" HEIGHT="1"
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
					</tr>
					<tr>
						<td class="tile-a"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="summary.name"/>
							<jsp:param name="render" value="true"/>
							<jsp:param name="encode" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<%
						    for(Cache cache : allCaches)
						        {
						%>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title"><%=getCacheNamesForDisplay(cache.getName())%>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;</td>
						<%
						    }
						%>
						<td background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
							class="tile-c">&nbsp;</td>
					</tr>
					<tr>
						<td colspan='<%=value%>' class="tile-dark"><IMG WIDTH="1"
							HEIGHT="1"
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
					</tr>
					<%
					        for(int i = 0; i < summaryData.length; i++)
					        {
					            String[] row = summaryData[i];
					            if((i & 1) != 0)
					            {
					%>
					<tr class="tile-row-highlight">
						<%}else{%>
					<tr class="tile-row-normal">
						<%}%><td><BR/></td>
						<%
					    for(int j = 0; j < row.length; j++)
					    {
							%>
							<td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT">
								<DIV class="small-text-inset"><%=row[j]%></DIV>
							</td>
							<td><BR/></td>
							<%
						 }
						%>
					</tr>
					<%}%>
					<%-- Write to disk --%>
					<tr>
						<td>&nbsp;</td>
						<td  style="height:40px">
							<div onclick="writeToDisk();">	
								<jsp:include page="/cachetool/addTextButton.jsp">
									<jsp:param  name="key" value="summary.button.writeToDisk"/>
								</jsp:include>
							</div>
						</td>
						<td></td>
						<%
						for(Cache cache : allCaches)
					        {
						    
						 %><td>
						 <div>
						 <input type="checkbox" name='WriteCache'
						 value='<%= cache.getName()%>' /></div>
						 </td>
						 <td>&nbsp;</td>
						<%
					        }
						%>
						<td>&nbsp;</td>
					</tr>
					<%-- Clear cache --%>
					<tr class="tile-row-highlight">
						<td>&nbsp;</td>
						<td style="height:40px">
							<div onclick="clearCache();">
							<jsp:include page="/cachetool/addTextButton.jsp">
									<jsp:param  name="key" value="summary.button.flushCache"/>
								</jsp:include>
							</div>
						</td>
						<td></td>
						<%
						for(Cache cache : allCaches)
					        {
						 %><td>
						 <div>
						 <input type="checkbox" name='FlushCache'
						 value='<%= cache.getName()%>' /></div>
						 </td>
						 <td>&nbsp;</td>
						<%
					        }
						%>
						<td>&nbsp;</td>
					</tr>
				</table>
				<td class="tile-dark" VALIGN="top" WIDTH="1" NOWRAP="nowrap"><BR /></td>
			</tr>
			<tr>
				<td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG
					WIDTH="1" HEIGHT="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
			</tr>
			<tr>
				<td></td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'>
				<IMG WIDTH="1" HEIGHT="5"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
				<td></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="7px"></td>
	</tr>
	
	<tr>
		<td height="7px"></td>
	</tr>
	<tr>
		<td><%
			String message = "";
			String status_message_key = "";
			if(request.getParameter("message") != null ){
			    message = (String)request.getParameter("message");
				message = StringEscapeUtils.escapeHtml(message);
				%><jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="summary.flush.message"/>
					<jsp:param name="locale" value="<%=defaultLocale%>"/>
				</jsp:include>
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="summary.clear.message"/>
					<jsp:param name="locale" value="<%=defaultLocale%>"/>
				</jsp:include>
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="summary.logoutCurrentUser"/>
					<jsp:param name="locale" value="<%=defaultLocale%>"/>
				</jsp:include><%
				//comparing default locale message and providing translation besed on user locale. 
				if(message.equals((String)request.getAttribute("summary.clear.message")))
					status_message_key = "summary.clear.message";
				if(message.equals((String)request.getAttribute("summary.flush.message")))
					status_message_key = "summary.flush.message";
				%><jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value='<%=status_message_key%>'/>
					<jsp:param name="encode" value="true"/>
				</jsp:include><%				
				message =  (String)request.getAttribute(status_message_key);
			}
			out.write(message);
			%></td>
	</tr>
	
</table>
<div><br/></div>

<%
		
	if(usingTraditionalCache)
	{
		String buttonName = request.getParameter("clearPageCacheMessage");
		%>
		<div onclick="clearSystemPageCache();">
			<div style="margin-left:10px">
				<jsp:include page="/cachetool/addTextButton.jsp">
					<jsp:param  name="text" value="<%=buttonName%>"/>
				</jsp:include>
			</div>		
		</div>
		<div id="progressID" style="display:none; ">
			<img src='<%=contextPath%>/Xcelerate/graphics/common/icon/wait_ax.gif'/>
			</div>
		<div style="clear:both"><br/></div>
		<div id="clearSPCMessageId" class="small-text-inset" style="display:none">
		<%=cachesToClear%> - 
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="summary.clear.message"/>
			<jsp:param name="locale" value="<%=defaultLocale%>"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>

		</div>
		<div id="errorMessageID" class="small-text-inset" style="display:none">
		</div>
<%
	}

%>
<%
// Log off the user and reload the page after clearing the session cache.
if(cacheName.equals(CacheUtil.CAS_CACHE))
{
	String s = request.getParameter("logoffuser");
	if(s!=null && s.equals("true"))
	{
%>
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="summary.logoutCurrentUser"/>
			<jsp:param name="locale" value="<%=defaultLocale%>"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>

		<iframe src='<%=SSO.getSSOSession().getSignoutUrl()%>' style="position: absolute; top: -100px; visibility: hidden;">
		</iframe>
		<script type="text/javascript">
		window.top.location.reload();
		</script>
<% 
	}
} %>

</form>
</body>

</html>
<%
    }
    else
    {
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.accessError"/>
	<jsp:param name="render" value="true"/>
	<jsp:param name="encode" value="true"/>
</jsp:include>
<%
    }
%>