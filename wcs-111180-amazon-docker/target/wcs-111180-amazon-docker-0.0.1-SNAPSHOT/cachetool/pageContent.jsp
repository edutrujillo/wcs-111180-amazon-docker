<%@page import="com.fatwire.cache.PageCacheBean"%>
<%@page import="net.sf.ehcache.Element"%>
<%@page import="net.sf.ehcache.Cache"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@ page
	import="com.fatwire.cache.ehcache.EhCacheObjectCache.CachedObjectWithDependencies"%>
<%@ page import="com.fatwire.cs.systemtools.util.CacheUtil"%>

<%
    String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
	String cacheName = request.getParameter("cachename");
	String queryStr = request.getQueryString();
	String modQueryStr = queryStr.substring(0,queryStr.lastIndexOf('&'));
	String authorize = (String) session.getAttribute("userAuthorized");
    if(Boolean.valueOf(authorize))
    {
        String contextPath = request.getContextPath();
%>
<html>
<head>
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/cacheTool.css'
	rel="stylesheet" type="text/css">
</head>
<body>
<%
    	
		if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName)))
		    cacheName = CacheUtil.SS_CACHE;
        
        CacheManager cacheMgr = CacheUtil.getCacheManager(cacheName);
        if(null == cacheMgr)
        {
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCacheDep.noCacheManager"/>
	<jsp:param name="encode" value="true"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
<%
    }
        else
        {
            Cache pageByQry = cacheMgr.getCache("pageByQry");
            Element e = pageByQry.getQuiet(modQueryStr);
            PageCacheBean pcb = null;
%>

<table BORDER="0" CELLSPACING="0" CELLPADDING="0">
	<tr>
		<td></td>
		<td class="tile-dark" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1"
			src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
		<td></td>
	</tr>
	<tr>
		<td class="tile-dark" VALIGN="top" WIDTH="1"><BR/></td>
		<td>
		<table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"
			width="100%">
			<tr>
				<td colspan="5" class="tile-highlight"><img
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'
					width="1" height="1" /></td>
			</tr>
			<tr>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
					class="tile-a">&nbsp;</td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
					class="tile-b">&nbsp;</td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
					class="tile-b">&nbsp;</td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
					class="tile-b">&nbsp;</td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
					class="tile-c">&nbsp;</td>
			</tr>
			<tr>
				<td class="tile-dark" colspan="5"><img height="1" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
			</tr>
			<%
			    if(e != null)
			            {
			                CachedObjectWithDependencies cbwd =
			                    (CachedObjectWithDependencies) e.getObjectValue();
			                pcb = (PageCacheBean) cbwd.getCachedObject();
			%>
			<tr>
				<td>&nbsp;</td>
				<td><b>
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="pageContent.headers"/>
					<jsp:param name="encode" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</b></td>
				<td>&nbsp;</td>
				<td class="break-all-line"><%=pcb.getHeaders()%></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><b>
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="pageContent.encoding"/>
					<jsp:param name="encode" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</b></td>
				<td>&nbsp;</td>
				<td><%=pcb.getEncoding()%></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><b>
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="pageContent.body"/>
					<jsp:param name="encode" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</b>
				</td>
				<td>&nbsp;</td>
				<td class="break-all-line"><%=pcb.getBodyAsString()%></td>
				<td>&nbsp;</td>
			</tr>
			<%
			    }
			            else
			            {
			%>
			<tr>
				<td></td>
				<td colspan="3">
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="pageContent.elementNoFound"/>
					<jsp:param name="encode" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</td>
				<td></td>
			</tr>
			<%}%>
		</table>
		</td>
		<td width="1" valign="top" class="tile-dark"><br /></td>
	</tr>
	<tr>
		<td height="1" valign="TOP" class="tile-dark" colspan="3"><img
			height="1" width="1"
			src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
	</tr>
	<tr>
		<td></td>
		<td
			background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'>
		<img height="5" width="1"
			src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
		<td></td>
	</tr>
</table>

</body>
</html>
<%
    }
    }
    else
    {
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.accessError"/>
	<jsp:param name="encode" value="true"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
<%
    }
%>
