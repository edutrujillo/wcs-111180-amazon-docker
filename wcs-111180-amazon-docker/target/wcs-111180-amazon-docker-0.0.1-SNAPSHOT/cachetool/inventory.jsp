<html><head>
		<title>Oracle WebCenter Sites Remote Satellite Server Tool</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<%
		String context = request.getContextPath();
		%>
	
	   	<style type="text/css" media="screen">
			@import '<%=request.getContextPath()%>/cachetool/css/login.css';
		</style>
		<link rel="shortcut icon" href="<%=context%>/sitesFavicon.ico" type="image/x-icon">
		<script>
		 function checkSubmit(e)
			{
			   if(e && e.keyCode == 13)
			   {
			      document.forms[0].submit();
			   }
			}
		</script>
	</head>

	<body class="fw" onKeyPress="return checkSubmit(event)">
		<form id="fm1" class="fm-v clearfix" method="post" action="<%=context%>/Inventory">
		
			<div class="centerboxcontainer" id="container">
				<div class="centerbox">
					<div class="loginbox" id="loginbox">
						<div class="topshadow"></div>
						<div class="leftshadow"></div>
						<div class="rightshadow"></div>
						<div class="bottomshadow"></div>

						<div class="loginBevelBottom"></div>
						
						<div class="loginheader"><div class="item logoWebCenterSites"></div></div>
						
						<div class="logincontent">
							<div class="logo"></div>
							<div class="loginform">
								<div style="display: none">
								</div>
									<div class="right"><b>User Login</b></div>						
									<div class="clear"></div>
									<div class="pt40"></div>
									<div class="right"><div class="UIValidationWrapper dijitInlineTable" id="username"><div class="errorArea"><span class="message"></span></div><div class="dijit dijitReset dijitInlineTable dijitLeft dijitTextBox dijitValidationTextBox UIInput" id="widget_username" role="presentation" widgetid="username"><div class="dijitReset dijitValidationContainer"></div><div class="dijitReset dijitInputField dijitInputContainer"><div class="fwInputWrapper" dojoattachpoint="inputWrapper"><input type="text" name="username" id="username" class="dijitReset dijitInputInner" value=""></div></div></div></div></div>
									<div class="title right">Username</div>
									<div class="clear"></div>
									<div class="pt10"></div>
									<div class="right"><div class="UIValidationWrapper dijitInlineTable" id="password"><div class="errorArea"><span class="message"></span></div><div class="dijit dijitReset dijitInlineTable dijitLeft dijitTextBox dijitValidationTextBox UIInput" id="widget_password" role="presentation" widgetid="password"><div class="dijitReset dijitValidationContainer"></div><div class="dijitReset dijitInputField dijitInputContainer"><div class="fwInputWrapper" dojoattachpoint="inputWrapper"><input type="password" name="password" id="password" class="dijitReset dijitInputInner" value=""></div></div></div></div></div>		
									<div class="title right">Password</div>
									<div class="clear"></div>						
									<div class="pt10"></div>
									
									 <div style="margin-left: 90px;" onclick="javascript:document.forms[0].submit();">

										<span class="dijit dijitReset dijitInline dijitButton fwButton fwGreyButton" ><span class="dijitReset dijitInline dijitButtonNode"><span class="dijitReset dijitInline fwButtonLeft"></span><span class="dijitReset dijitStretch dijitButtonContents"><span class="dijitReset dijitInline dijitIcon"></span><span class="dijitReset dijitToggleButtonIconChar">●</span><span class="dijitReset dijitInline dijitButtonText">
											LOGIN
										</span></span><span class="dijitReset dijitInline fwButtonRight"></span></span></span>
										<br/>
										<%

									 	if(request.getParameter("error")!=null) {
									 	out.println("<br/>");
									 		out.println("Invalid username/password.");

									 		}
									 	%>
									
									</div>
									<div class="clear"></div>
									<div class="pt10"></div>
							</div>
						</div>
					</div>
					<div class="center copyright">
						Copyright &copy 2013, Oracle and/or its affiliates. All rights reserved.<br>
						Oracle is a registered trademark of Oracle and/or its affiliates. Other names may be trademarks of their respective owners.
					</div>
				</div>
			</div>
			<div class="loginscreen"></div>
			<div class="loginscreenbg"></div>	
		</form>
</body>
</html>