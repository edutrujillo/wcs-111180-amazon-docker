<%@page import="net.sf.ehcache.Element"%>
<%@page import="com.fatwire.cache.InvalidationData"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="net.sf.ehcache.Cache"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@page import="com.fatwire.cache.PageCache"%>
<%@page import="com.fatwire.cache.ehcache.PageCacheProvider"%>
<%@page import="java.util.Set"%>
<%@ page import="COM.FutureTense.Cache.CacheHelper"%>
<%@ page import="COM.FutureTense.Util.ftStatusCode"%>
<%@ page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@ page import="COM.FutureTense.Util.ftMessage"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%@ page import="org.apache.commons.lang.*"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%!private static final Log log_jsp = LogFactory.getLog(ftMessage.JSP_DEBUG);%>
<%
    
	String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}
		
	String authorize = (String) session.getAttribute("userAuthorized");
    if(Boolean.valueOf(authorize))
    {
        String cacheName = request.getParameter("cachename");
		cacheName = StringEscapeUtils.escapeHtml(cacheName);
        if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName)))
            cacheName = CacheUtil.SS_CACHE;
        
        String contextPath = request.getContextPath();
        int rowsPerPage = 10;
        int pagesPerView = 7;
        String[] rowColors =
            { "tile-row-normal", "tile-row-highlight" };

        try
        {
            String rowsPerPageStr = request.getParameter("rowsPerPage");
            if(rowsPerPageStr != null)
            {
                rowsPerPage = Integer.parseInt(rowsPerPageStr);
                if(rowsPerPage <= 0)
                    rowsPerPage = Integer.MAX_VALUE;
                session.setAttribute("rowsPerPage", new Integer(
                    rowsPerPage));
            }
            else
            {
                Integer rowsPerPageInSession =
                    (Integer) session.getAttribute("rowsPerPage");
                if(rowsPerPageInSession != null)
                    rowsPerPage = rowsPerPageInSession.intValue();
            }

        }
        catch (Exception e)
        {
            if(log_jsp.isErrorEnabled())
            {
                log_jsp
                    .error(
                        "Unexpected error while processing cachetool/dep.jsp.",
                        e);
            }
        }

        String action = request.getParameter("action");
        String searchKey = request.getParameter("searchKey");
		searchKey = StringEscapeUtils.escapeHtml(searchKey);
        String flushKeys = request.getParameter("flushKeys");

        String selectPage = request.getParameter("selectPage");
        if(selectPage == null || "".equals(selectPage))
            selectPage = "1";
        int pageNum = Integer.parseInt(selectPage);

        Set<String> keyIterable = null;
        int totalCount = 0;
        
        PageCache accessor = PageCacheProvider.getCache(cacheName);
        if(!accessor.isAvailable())
        {%><jsp:forward page="/cachetool/message.jsp" /><% 
        }
        CacheManager cacheMgr =
                CacheUtil.getCacheManager(cacheName);

            if(null == cacheMgr)
            {
				%><jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="assetCacheDep.noCacheManager"/>
					<jsp:param name="escape" value="true"/>
					<jsp:param name="render" value="true"/>
				</jsp:include><%
				return;
            }
            Cache depsCache = cacheMgr.getCache("dependencyRepository");
%>
<html>
<head>
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css'
	rel="stylesheet" type="text/css" />
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css'
	rel="stylesheet" type="text/css"/ >
<LINK
	href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/cacheTool.css'
	rel="stylesheet" type="text/css"/ >
<%@ include file="./paging.jsp"%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="dep.validDepMessage"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>

<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="assetCache.invalidSearch"/>
	<jsp:param name="escape" value="true"/>
</jsp:include>
<script language="javascript" type="text/javascript">

function submitSearchForm(action, selectPage)
{
	var url = '<%=contextPath%>/cachetool/dep.jsp?&action='+action+'&cachename=<%=cacheName%>';
	if (document.searchForm.searchKey.value == "Please enter dependency")
	{
		alert("<%=request.getAttribute("dep.validDepMessage")%>");
		return false;
	}
	
	//set flush keys if request comes from flush
	if (action== 'flush')
		document.searchForm.flushKeys.value = getFlushKeys();
	document.searchForm.selectPage.value = selectPage;
	
	var searchKeyObj = document.searchForm.searchKey;
	if(searchKeyObj.value.indexOf("<",0) == -1 
			&& searchKeyObj.value.indexOf(">",0) == -1)
	{
	document.searchForm.action = url;
	document.searchForm.submit();
	}else 
		alert("<%=request.getAttribute("assetCache.invalidSearch")%>");
}

function submitResultForm() {

	var url = '<%=contextPath%>/cachetool/page.jsp?cachename=<%=cacheName%>&searchBy=Dependency&searchKey=';
	var chkboxNum = document.resultForm.dep.length;
	var count = 0;
	if (chkboxNum != undefined)
	{
		for (var idx = 0; idx < chkboxNum; idx++) 
		{
			if (document.resultForm.dep[idx].checked == true)
			{
				count++;
				if (count == 1)
				{
					url = url + document.resultForm.dep[idx].value;
					
				}
				else
					url = url + "," + document.resultForm.dep[idx].value;
			}
		}
	}
	else
	{
		if(document.resultForm.dep.checked == true)
			url = url + document.resultForm.dep.value;
	}
	document.resultForm.action = url;
	document.resultForm.submit();
}

function getFlushKeys() {

	var keys = "";
	var chkboxNum = document.resultForm.key.length;
	var count = 0;
	if (chkboxNum != undefined)
	{
	for (var idx = 0; idx < chkboxNum; idx++) 
	{
		if (document.resultForm.key[idx].checked == true)
		{
			count++;
			if (count == 1)
			{
				keys = keys + document.resultForm.key[idx].value;
				
			}
			else
				keys = keys + "," + document.resultForm.key[idx].value;
		}
	}
	}
	else
	{
		if(document.resultForm.key.checked == true)
			keys = keys + document.resultForm.key.value;
	}
	return keys;
}
</script>
</head>
<body>
<jsp:include page="/cachetool/serverTimeZoneInfo.jsp"/>
<table border="0" bgcolor="#ffffff">
	<tr>
		<td style="height: 3px"></td>
	</tr>
	<tr>
		<td class="sub-title-text">
			<jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="list.dependencies"/>
				<jsp:param name="encode" value="true"/>
				<jsp:param name="render" value="true"/>
			</jsp:include>
		</td>
	</tr>
	<tr>
		<td style="height: 7px"></td>
	</tr>
	<tr>
		<td>
		<form name="searchForm" method="post" action="<%=contextPath%>/cachetool/dep.jsp">
		<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
		<table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
			<tr>
				<td></td>
				<td height="1" valign="TOP" class="tile-dark"><img height="1"
					width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
			<tr>
				<td width="1" valign="top" class="tile-dark"><br>
				</td>
				<td>
				<table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"
					width="100%">
					<tr>
						<td colspan="2" class="tile-highlight"><img
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'
							width="1" height="1"></td>
					</tr>
					<tr>
						<td
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
							class="tile-a">&nbsp;</td>
						<td
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'
							class="tile-c">&nbsp;</td>
					</tr>
					<tr>
						<td class="tile-dark" colspan="2"><img height="1" width="1"
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
					</tr>
					<tr class="form-shade">
						<td colspan="2" height="2px"></td>
					</tr>
					<tr class="form-shade">
						<td width=25% nowrap>
						<div class="form-label-inset">
							<jsp:include page="/cachetool/translator.jsp">
								<jsp:param name="key" value="search.itemsPerPage"/>
								<jsp:param name="encode" value="true"/>
								<jsp:param name="render" value="true"/>
							</jsp:include>
						</div>
						</td>
						<td width="*%" align=left><input type="text"
							name="rowsPerPage" id="rowsPerPage" class="form-inset" size=10
							value='<%=rowsPerPage%>' /></td>
					</tr>
					<tr class="form-shade">
						<td colspan="2" height="5px"></td>
					</tr>
					<tr valign=middle>
						<td colspan="2"><textarea class="form-inset" name="searchKey"
							id="searchKey" rows="4" cols="60"><%=(null == searchKey) ? "" : searchKey%></textarea>
						</td>
					</tr>
				</table>
				</td>
				<td width="1" valign="top" class="tile-dark"><br />
				</td>
			</tr>
			<tr>
				<td height="1" valign="TOP" class="tile-dark" colspan="3"><img
					height="1" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
			</tr>
			<tr>
				<td></td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'><img
					height="5" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
		</table>
		<input type="hidden" name="flushKeys" id="flushKeys" />
		<input type="hidden" name="selectPage" id="selectPage" />
		<div onclick="submitSearchForm('search','1');" style="float:right">
		<jsp:include page="/cachetool/addTextButton.jsp">
			<jsp:param  name="key" value="search.button.search"/>
		</jsp:include>
		</div>
		</form>
		
		</td>
	</tr>
	
</table>
<%
    if(searchKey == null || "".equals(searchKey))
                searchKey = "*";

            if("paging".equals(action))
            {
                keyIterable =
                    (Set<String>) session.getAttribute("result");
                totalCount =
                    ((Integer) session.getAttribute("totalCount"))
                        .intValue();
            }
            else
            {
                if(flushKeys != null && !"".equals(flushKeys))
                {
                    List<String> invalData =
                        Arrays.asList(flushKeys.split(","));
                    accessor
                        .invalidateDependencies(new InvalidationData(
                            invalData, System.currentTimeMillis()));
                }

                String[] deps = searchKey.split(",");
                keyIterable = accessor.findDependencies(deps, 0);

                totalCount = keyIterable.size();
                session.setAttribute("result", keyIterable);
                session.setAttribute("totalCount", new Integer(
                    totalCount));

                if(totalCount / rowsPerPage == (pageNum - 1)
                    && totalCount % rowsPerPage == 0)
                    pageNum = 1;
            }

            if(totalCount > 0)
            {
                
%>
<form name="resultForm" method="post">
<input type="hidden" name="_authkey_" value='<%=session.getAttribute("_authkey_")%>'/>
<p>
<table border="0">
	<jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="dep.sysGenCountMessage"/>
	</jsp:include>
	<tr>
		<td conspan=2><%=((String)request.getAttribute("dep.sysGenCountMessage")).replace("{0}",Integer.toString(accessor.getCurrentDepGenCount()))%></b></td>
	</tr>
	<tr>
		<td height="7px"></td>
	</tr>
	<jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="assetCache.noItemsInCache"/>
	</jsp:include>
	<tr>
		<td align=left><%=((String)request.getAttribute("assetCache.noItemsInCache")).replace("{0}",Integer.toString(totalCount))%></b></td>
		<td>
		<div id="searchResults" align="right"></div>
		</td>
	</tr>
	<tr>
			<td align="left">
		<div onclick="submitSearchForm('flush', '<%=pageNum%>');">
					<jsp:include page="/cachetool/addTextButton.jsp">
						<jsp:param  name="key" value="summary.button.flushCache"/>
					</jsp:include>
				</div>
		</td>
		<td><div onclick="submitResultForm();" style='float:right'>
					<jsp:include page="/cachetool/addTextButton.jsp">
						<jsp:param  name="key" value="revDepPage.pageLookUp"/>
					</jsp:include>
				</div>
			</td>
	</tr>
	<tr>
		<td colspan="2">
		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td></td>
				<td height="1" valign="TOP" class="tile-dark"><img height="1"
					width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
			<tr>
				<td width="1" valign="top" class="tile-dark"><br />
				</td>
				<td>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td colspan="11" class="tile-highlight"><img
							src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'
							width="1" height="1"></td>
					</tr>
					<tr>
						<td class="tile-a"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b" align="center"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
							<input type='checkbox' class='checkboxLookup' name='selectAll' id='selectAll' onclick="checkAll('key');"/></td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="assetCacheDep.key"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="dep.generationCount"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b" width="30"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
						<span class="new-table-title">
						<jsp:include page="/cachetool/translator.jsp">
							<jsp:param name="key" value="dep.valid"/>
							<jsp:param name="render" value="true"/>
						</jsp:include>
						</span>
						</td>
						<td class="tile-b" width="30"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
						<td class="tile-b"
							align="center" background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
							<input type='checkbox' class='checkboxLookup' 
							name='selectAll_1' id='selectAll_1' onclick="checkAll('dep');"/></td>
						<td class="tile-c"
							background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
					</tr>
					<%
		    			int count = 0;
		                int lowerbound = rowsPerPage * (pageNum - 1);
		                int upperbound = lowerbound + rowsPerPage;
		                for(String key : keyIterable)
		                {
		                    Element e = depsCache.getQuiet(key);
		                    String value =
		                        null == e ? null : e.getObjectValue()
		                            .toString();
		                    if(++count > lowerbound)
		                    {
		                        if(count > upperbound)
		                            break;
		                        String keyBox =
		                            "<input type='checkbox' onclick=\"toggleParentOnCheck('key',this);\" name='key' id='key' value='"
		                                + key + "'>";
		                        StringBuilder depsbox = new StringBuilder();

		                        //need to check
		                        depsbox
		                            .append(
		                                "<div class='dep'><input type='checkbox' onclick=\"toggleParentOnCheck('dep',this);\" name='dep' id='"+ key+"' ")
		                            .append(" value='").append(key)
		                            .append("'></div>");
					%>
					<tr class='<%=rowColors[count % 2]%>'>
						<td>&nbsp;</td>
						<td align=center  ><%=keyBox%></td>
						<td>&nbsp;</td>
						<td><%=CacheUtil.extractFieldValue(value, 0)%></td>
						<td>&nbsp;</td>
						<td align=middle><%=CacheUtil.extractFieldValue(value, 1)%></td>
						<td width="30">&nbsp;</td>
						<td align=middle><%=CacheUtil.extractFieldValue(value, 2)%></td>
						<td width="30">&nbsp;</td>
						<td align=middle><%=depsbox%></td>
						<td>&nbsp;</td>
					</tr>
					<%
					    }
					                }
					%>
				</table>
				</td>
				<td width="1" valign="top" class="tile-dark"><br />
				</td>
			</tr>
			<tr>
				<td height="1" valign="TOP" class="tile-dark" colspan="3"><img
					height="1" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
			</tr>
			<tr>
				<td></td>
				<td
					background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'><img
					height="5" width="1"
					src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif'></td>
				<td></td>
			</tr>
		</table>
		</td>
	</tr>
	<%
	    }
	            else
	            {
	%>
	<tr>
		<td class="message-bg-color">
		<jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="assetCacheDep.noitemsFound"/>
			<jsp:param name="encode" value="true"/>
			<jsp:param name="render" value="true"/>
		</jsp:include>
		</td>
	</tr>
	<%
	    }
	%>
</table>
</form>
</body>
<script language="javascript" type="text/javascript">
	var page = new Paging({	renderTop:"searchResults",
							recordCount:<%=totalCount%>,
							onSuccess:'gotoPage',
							currentPage: <%=pageNum%>,
							rowsPerPage:<%=rowsPerPage%>,
							pagePerView:<%=pagesPerView%>
						 });
</script>
</html>
<%  
    }
    else
    {
%>
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.accessError"/>
	<jsp:param name="encode" value="true"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
<%
    }
%>