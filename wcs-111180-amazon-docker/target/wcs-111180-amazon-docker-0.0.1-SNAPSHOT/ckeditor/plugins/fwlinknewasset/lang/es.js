/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwlinknewasset', 'es',
{
	fwlinknewasset :
	{
	  assetCreatorIncludeDlgTitle : 'Creador de activos',
	  assetCreatorIncludeTitleDesc : 'Crear e incluir un activo nuevo',
	  assetCreatorLinkTitleDesc : 'Crear y enlazar un activo nuevo',
	  PleaseSelectLinkTextFrom	: 'Es necesario seleccionar primero el texto de enlace al activo' 
	
	
	  	    
	}
});

