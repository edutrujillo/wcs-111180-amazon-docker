/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwlinkasset', 'ja',
{
	fwlinkasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: 'アセット・リンクの追加',
	   IncludeEmbeddedLinkDlgTitle	: 'アセットを含む' ,
	   PleaseSelectLinkTextFrom	    : 'アセットへリンクするテキストを先に選択する必要があります' ,
	   PleaseSelectAssetFromTree	: 'ツリーからアセットを選択します。',
	   PleaseSelectAssetFromSearchResults : '検索結果からアセットの選択',
	   PleaseSelectOnlyOneAsset 	: 'アセットを1つだけ選択できます。',
	   CannotAddSelfInclude	        : 'Includeを通じた自己参照はできません。\n他のアセットを選択してください。',
       CannotLinkOrIncludeThisAsset : 'このアセットをリンクまたは含めることはできません。\n次のタイプのアセットを選択してください: \n'
			    
	}
});


