/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwlinkasset', 'zh-cn',
{
	fwlinkasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: '添加资产链接',
	   IncludeEmbeddedLinkDlgTitle	: '包括资产' ,
	   PleaseSelectLinkTextFrom	    : '必须先选择要链接到资产的文本' ,
	   PleaseSelectAssetFromTree	: '从树中选择资产。',
	   PleaseSelectAssetFromSearchResults : '从搜索结果中选择资产',
	   PleaseSelectOnlyOneAsset 	: '只能选择一个资产。',
	   CannotAddSelfInclude	        : '不允许通过包括操作来进行自引用。\n请选择其他资产。',
       CannotLinkOrIncludeThisAsset : '不允许链接或包括此资产。\n请选择以下类型的资产:\n'
			    
	}
});


