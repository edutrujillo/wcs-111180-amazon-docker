/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'zh-cn',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  '更改包括的资产',
         EditProperties :       '编辑属性',
         RemoveIncludedAsset :  '删除包括的资产',
         OpenLink :             '打开链接',
         ChangeLinkedAsset :    '更改链接的资产',
         RemoveLinkedAsset :    '删除链接的资产',
         PleaseSelectAssetFromSearchResults : '从搜索结果中选择资产',
         CannotLinkOrIncludeThisAsset :       '不允许链接或包括此资产。\n请选择以下类型的资产:\n',

         CutIncludedAsset :            '剪切包括的资产',
         CopyIncludedAsset :           '复制包括的资产',
         CutLinkedAsset :              '剪切链接的资产',
         CopyLinkedAsset :             '复制链接的资产',
         PasteIncludedAsset :          '粘贴包括的资产',
         PasteLinkedAsset :            '粘贴链接的资产' 		

	}
});




