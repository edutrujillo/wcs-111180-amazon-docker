/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'ko',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  '포함된 자산 변경',
         EditProperties :       '속성 편집',
         RemoveIncludedAsset :  '포함된 자산 제거',
         OpenLink :             '링크 열기',
         ChangeLinkedAsset :    '링크된 자산 변경',
         RemoveLinkedAsset :    '링크된 자산 제거',
         PleaseSelectAssetFromSearchResults : '검색 결과에서 자산 선택',
         CannotLinkOrIncludeThisAsset :       '이 자산은 링크 또는 포함이 허용되지 않습니다.\n다음 유형의 자산을 선택하십시오.\n',

         CutIncludedAsset :            '포함된 자산 잘라내기',
         CopyIncludedAsset :           '포함된 자산 복사',
         CutLinkedAsset :              '링크된 자산 잘라내기',
         CopyLinkedAsset :             '링크된 자산 복사',
         PasteIncludedAsset :          '포함된 자산 붙여넣기',
         PasteLinkedAsset :            '링크된 자산 붙여넣기' 		

	}
});




