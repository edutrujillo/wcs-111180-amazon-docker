/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'zh-cn',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'是否删除包括的资产?',
	 RemoveLinkedAssetQuestion:'是否删除链接的资产?'
	}
});
