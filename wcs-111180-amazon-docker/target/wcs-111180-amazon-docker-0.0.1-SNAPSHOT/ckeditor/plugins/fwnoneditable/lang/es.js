/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'es',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'¿Desea eliminar el activo incluido?',
	 RemoveLinkedAssetQuestion:'¿Desea eliminar el activo enlazado?'
	}
});
