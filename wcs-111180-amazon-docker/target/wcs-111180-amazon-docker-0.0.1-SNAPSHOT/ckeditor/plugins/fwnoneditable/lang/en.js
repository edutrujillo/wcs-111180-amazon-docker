﻿/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable","en",{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'Do you want to remove the included asset ?',
	 RemoveLinkedAssetQuestion:'Do you want to remove the linked asset ?'
	}
});