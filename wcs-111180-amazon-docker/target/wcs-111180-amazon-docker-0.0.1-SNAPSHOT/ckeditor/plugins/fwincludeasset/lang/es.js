/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwincludeasset', 'es',
{
	fwincludeasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: 'Agregar enlace de activo',
	   IncludeEmbeddedLinkDlgTitle	: 'Incluir activo' ,
	   PleaseSelectLinkTextFrom	    : 'Es necesario seleccionar primero el texto de enlace al activo' ,
	   PleaseSelectAssetFromTree	: 'Seleccione un activo en el árbol.',
	   PleaseSelectAssetFromSearchResults : 'Seleccione un activo en los resultados de la búsqueda',
	   PleaseSelectOnlyOneAsset 	: 'Sólo se puede seleccionar un activo.',
	   CannotAddSelfInclude	        : 'No se permiten autorreferencias mediante Incluir.\nSeleccione otro activo.',
       CannotLinkOrIncludeThisAsset : 'Este activo no permite enlaces ni inclusiones.\nSeleccione un activo de uno de los tipos siguientes:\n'
			    
	}
});


