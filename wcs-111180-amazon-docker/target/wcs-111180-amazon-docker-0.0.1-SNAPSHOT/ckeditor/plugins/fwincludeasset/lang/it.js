/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwincludeasset', 'it',
{
	fwincludeasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: 'Aggiungi collegamento ad asset',
	   IncludeEmbeddedLinkDlgTitle	: 'Includi asset' ,
	   PleaseSelectLinkTextFrom	    : 'Selezionare prima il testo da collegare all\'asset' ,
	   PleaseSelectAssetFromTree	: 'Selezionare un asset dalla struttura ad albero.',
	   PleaseSelectAssetFromSearchResults : 'Selezionare un asset dai risultati della ricerca',
	   PleaseSelectOnlyOneAsset 	: 'È possibile selezionare un solo asset.',
	   CannotAddSelfInclude	        : 'Non è consentito il riferimento automatico mediante inclusione.\nSelezionare un altro asset.',
       CannotLinkOrIncludeThisAsset : 'Non è consentito effettuare il collegamento a questo asset o includerlo.\nSelezionare un asset dei tipi seguenti:\n'
			    
	}
});


