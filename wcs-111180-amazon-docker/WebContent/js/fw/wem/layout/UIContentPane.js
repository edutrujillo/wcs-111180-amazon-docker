/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.wem.layout.UIContentPane"]){dojo._hasResource["fw.wem.layout.UIContentPane"]=true;dojo.provide("fw.wem.layout.UIContentPane");dojo.require("dojox.layout.ContentPane");dojo.declare("fw.wem.layout.UIContentPane",dojox.layout.ContentPane,{_layoutChildren:function(){var gc=this.getChildren;this.getChildren=function(){return [];};this.inherited(arguments);this.getChildren=gc;},_isQuerying:false,_load:function(){this._isQuerying=true;var _1=dojo.create("div",{innerHTML:this.onDownloadStart()},this.domNode,"first");dojo.style(_1,{backgroundColor:"#fff",padding:"10px",position:"absolute",zIndex:"100",left:((this.domNode.offsetWidth-_1.offsetWidth)/2)+"px",top:"150px"});this.inherited(arguments);},_setContent:function(_2,_3){if(!this._isQuerying){this.inherited(arguments);}else{this._isQuerying=false;}}});}