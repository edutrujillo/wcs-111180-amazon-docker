/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.UITree"],["require","dijit.Tree"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.UITree"]){_4._hasResource["fw.dijit.UITree"]=true;_4.provide("fw.dijit.UITree");_4.require("dijit.Tree");_4.declare("fw.dijit.UITree",_5.Tree,{dndAccept:false,dataAttr:"name",persist:true,typeAttr:"",postCreate:function(){this.dndParams=this.dndParams.concat(["accept","dataAttr","typeAttr"]);this.inherited(arguments);this._openedItemIds={};this.persist=true;},getIconStyle:function(_7,_8){var _9=this.model,_a=_9.store,_b,_c;if(_a.isItem(_7)&&_a.hasAttribute(_7,"iconUrl")){_b=_a.getValue(_7,"iconUrl");_c={backgroundImage:"url('"+_b+"')"};return _c;}}});}}};});