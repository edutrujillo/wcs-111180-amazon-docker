/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UIMenuWithShadow"]){dojo._hasResource["fw.dijit.UIMenuWithShadow"]=true;dojo.provide("fw.dijit.UIMenuWithShadow");dojo.require("dijit.Menu");dojo.declare("fw.dijit.UIMenuWithShadow",dijit.Menu,{templateString:"<table class=\"dijit dijitMenu dijitMenuPassive dijitReset dijitMenuTable\" waiRole=\"menu\" tabIndex=\"${tabIndex}\" dojoAttachEvent=\"onkeypress:_onKeyPress\"><tr><td class=\"dijitReset dijitTableBody\" dojoAttachPoint=\"containerNode\"></td></tr></table>"});}