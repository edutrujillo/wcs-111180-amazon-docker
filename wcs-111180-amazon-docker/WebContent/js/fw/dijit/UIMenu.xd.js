/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.UIMenu"],["require","fw.dijit.UIMenuWithShadow"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.UIMenu"]){_4._hasResource["fw.dijit.UIMenu"]=true;_4.provide("fw.dijit.UIMenu");_4.require("fw.dijit.UIMenuWithShadow");_4.declare("fw.dijit.UIMenu",fw.dijit.UIMenuWithShadow,{unhoverTimeout:100,menuTimer:null,outItem:null,cleanOnClose:false,postCreate:function(){this.inherited(arguments);if(this.cleanOnClose){_4.connect(this,"onOpen",this,function(){this.destroyDescendants();});}},onItemUnhover:function(_7){this.inherited(arguments);this.outItem=_7;},onItemHover:function(){if(this.parentMenu&&this.parentMenu.hidePopupTimer){clearTimeout(this.parentMenu.hidePopupTimer);this.parentMenu.hidePopupTimer=null;}clearTimeout(this.menuTimer);this.menuTimer=null;this.inherited(arguments);},onClose:function(){this.inherited(arguments);if(this.menuTimer){clearTimeout(this.menuTimer);this.menuTimer=null;}},_mouseGone:function(){this.onCancel();}});}}};});