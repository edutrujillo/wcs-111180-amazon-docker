/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.ValidationMultiSelect"]){dojo._hasResource["fw.dijit.ValidationMultiSelect"]=true;dojo.provide("fw.dijit.ValidationMultiSelect");dojo.require("dijit.form.MultiSelect");dojo.require("fw.dijit.UIValidationMixin");dojo.extend(dijit.form.MultiSelect,{postCreate:function(){this._onChange();this.inherited("postCreate",arguments);}});dojo.declare("fw.dijit.ValidationMultiSelect",[dijit.form.MultiSelect,fw.dijit.UIValidationMixin],{baseClass:"ValidationMultiSelect",postCreate:function(){this.inherited(arguments);this.connect(this,"onChange","validate");},isValid:function(){return !this.required||this.get("value").length>0;}});}