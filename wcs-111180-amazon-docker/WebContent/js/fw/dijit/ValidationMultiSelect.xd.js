/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.ValidationMultiSelect"],["require","dijit.form.MultiSelect"],["require","fw.dijit.UIValidationMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.ValidationMultiSelect"]){_4._hasResource["fw.dijit.ValidationMultiSelect"]=true;_4.provide("fw.dijit.ValidationMultiSelect");_4.require("dijit.form.MultiSelect");_4.require("fw.dijit.UIValidationMixin");_4.extend(_5.form.MultiSelect,{postCreate:function(){this._onChange();this.inherited("postCreate",arguments);}});_4.declare("fw.dijit.ValidationMultiSelect",[_5.form.MultiSelect,fw.dijit.UIValidationMixin],{baseClass:"ValidationMultiSelect",postCreate:function(){this.inherited(arguments);this.connect(this,"onChange","validate");},isValid:function(){return !this.required||this.get("value").length>0;}});}}};});