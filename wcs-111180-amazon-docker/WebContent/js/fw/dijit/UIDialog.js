/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.dijit.UIDialog"]){dojo._hasResource["fw.dijit.UIDialog"]=true;dojo.provide("fw.dijit.UIDialog");dojo.require("dijit.Dialog");dojo.declare("fw.dijit.UIDialog",dijit.Dialog,{templateString:dojo.cache("fw.dijit","templates/UIDialog.html","<div class=\"dijitDialog UIDialogContainer\" tabindex=\"-1\" waiRole=\"dialog\" waiState=\"labelledby-${id}_title\">\r\n <div class=\"UIDialog\">\r\n  <div dojoAttachPoint=\"titleBar\" class=\"dijitDialogTitleBar\">\r\n   <span dojoAttachPoint=\"titleNode\" class=\"dijitDialogTitle\" id=\"${id}_title\"></span>\r\n   <span dojoAttachPoint=\"closeButtonNode\" class=\"dijitDialogCloseIcon\" dojoAttachEvent=\"onclick: onCancel\" title=\"${buttonCancel}\">\r\n    <span dojoAttachPoint=\"closeText\" class=\"closeText\" title=\"${buttonCancel}\">x</span>\r\n   </span>\r\n  </div>\r\n  <div dojoAttachPoint=\"containerNode\" class=\"dijitDialogPaneContent\"></div>\r\n </div>\r\n</div>\r\n")});}