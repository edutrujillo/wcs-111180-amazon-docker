/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.dijit.UIActionLink"],["require","dijit._Widget"],["require","dijit._Templated"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.dijit.UIActionLink"]){_4._hasResource["fw.dijit.UIActionLink"]=true;_4.provide("fw.dijit.UIActionLink");_4.require("dijit._Widget");_4.require("dijit._Templated");_4.declare("fw.dijit.UIActionLink",[_5._Widget,_5._Templated],{disabled:false,templateString:"<a href=\"javascript:void(0)\" class=\"UIActionLink\" dojoAttachPoint=\"containerNode\"></a>",attributeMap:_4.delegate(_5._Widget.prototype.attributeMap,{label:{node:"containerNode",type:"innerHTML"}}),postCreate:function(){this.inherited(arguments);this.connect(this.domNode,"onclick","_onClick");},_setDisabledAttr:function(_7){if(_7){_4.addClass(this.domNode,"UIActionDisabled");}else{_4.removeClass(this.domNode,"UIActionDisabled");}this.disabled=_7;},_onClick:function(_8){if(!this.get("disabled")){this.onClick();}},onClick:function(_9){}});}}};});