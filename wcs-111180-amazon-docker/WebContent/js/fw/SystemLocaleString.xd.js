/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.SystemLocaleString"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.SystemLocaleString"]){_4._hasResource["fw.SystemLocaleString"]=true;_4.provide("fw.SystemLocaleString");(function(){var _7=_4.config.fw_csPath,_8=_4.config.locale,_9="fatwire/ui/util/GetSLSObj",_a=_7+"ContentServer?pagename="+_9+"&user_locale="+_8,_b=function(_c){document.write("<script type=\"text/javascript\" src=\""+_c+"\"><"+"/script>");};if(typeof (_4._xdIsXDomainPath)==="function"&&_4._xdIsXDomainPath(_a)){_b(_a);}else{if(window.top===window.self){_b(_a);}}})();}}};});