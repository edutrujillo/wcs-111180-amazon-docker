/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.data.translator.CSRestTranslator"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.data.translator.CSRestTranslator"]){_4._hasResource["fw.data.translator.CSRestTranslator"]=true;_4.provide("fw.data.translator.CSRestTranslator");fw.data.translator.CSRestTranslator={translate:function(_7){var _8={};if(!_7){_7={};}if(_7.start){_8.startindex=_7.start;}if(_7.count&&isFinite(_7.count)){_8.count=_7.count;}if(_7.sort){for(var i=0;i<_7.sort.length;i++){_8["sortfield:"+_7.sort[i].attribute+(_7.sort[i].descending?":des":"")]="";}}if(_7.query){for(var k in _7.query){if(!_7.query.hasOwnProperty(k)){continue;}if(k=="q"){_8.q=_7.query[k];}else{if(_7.query[k]){_8["field:"+k]=_7.query[k];}}}}return _8;}};}}};});