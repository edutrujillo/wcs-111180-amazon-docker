/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.data.cs.ServerRestStore"]){dojo._hasResource["fw.data.cs.ServerRestStore"]=true;dojo.provide("fw.data.cs.ServerRestStore");dojo.require("fw.data.ServerStore");dojo.require("fw.data.cs._Resources");dojo.declare("fw.data.cs.ServerRestStore",fw.data.ServerStore,{resource:"",params:null,constructor:function(_1){var _2=fw.data.cs._Resources.resolveStoreArgs(_1);dojo.mixin(this,_2);}});}