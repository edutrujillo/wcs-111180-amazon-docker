/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.data.util"],["require","dojo.data.util.sorter"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.data.util"]){_4._hasResource["fw.data.util"]=true;_4.provide("fw.data.util");_4.require("dojo.data.util.sorter");fw.data.util={caseInsensitiveComparator:function(a,b){if(_4.isString(a)&&_4.isString(b)){return _4.data.util.sorter.basicComparator(a.toLowerCase(),b.toLowerCase());}else{return _4.data.util.sorter.basicComparator(a,b);}},serializeStoreItem:function(_7,_8,_9){var _a={},i,_b,_c;if(!_7){return _a;}if(!_9||!_9.length){_9=_7.getAttributes(_8);}_b=_9.length;for(i=_b;i--;){_c=_7.getValues(_8,_9[i]);if(_c.length==1){_a[_9[i]]=_c[0];}else{if(_c.length>1){_a[_9[i]]=_c;}}}return _a;}};}}};});