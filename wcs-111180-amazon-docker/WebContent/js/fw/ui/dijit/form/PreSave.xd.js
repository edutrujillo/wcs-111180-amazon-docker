/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dijit.form.PreSave"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dijit.form.PreSave"]){_4._hasResource["fw.ui.dijit.form.PreSave"]=true;_4.provide("fw.ui.dijit.form.PreSave");_4.declare("fw.ui.dijit.form.PreSave",null,{execute:function(_7){var _8="extensions.sites."+_7.PubName+".assettypes."+_7.AssetType+".PreSave";try{_4["require"](_8);}catch(e){return;}_4.ready(function(){var _9=_4.getObject(_8);var _a=new _9();_a.execute(_7);});}});fw.ui.dijit.form._preSave=null;fw.ui.dijit.form.preSave=function(){if(!fw.ui.dijit.form._preSave){fw.ui.dijit.form._preSave=new fw.ui.dijit.form.PreSave();}return fw.ui.dijit.form._preSave;};}}};});