/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dijit.AlertDialog"]){dojo._hasResource["fw.ui.dijit.AlertDialog"]=true;dojo.provide("fw.ui.dijit.AlertDialog");dojo.require("fw.ui.dijit.FlexibleDialog");dojo.require("fw.ui.dijit.Button");dojo.declare("fw.ui.dijit.AlertDialog",fw.ui.dijit.FlexibleDialog,{templateString:dojo.cache("fw.ui.dijit","templates/AlertDialog.html","<div class=\"alertDialog\">\r\n\t<span dojoAttachPoint=\"closeButtonNode\"></span>\r\n\t<div class=\"iconNode\"></div>\r\n\t<div class=\"titleNode\" dojoAttachPoint=\"titleNode\"></div>\r\n\t<div class=\"messageNode\" dojoAttachPoint=\"messageNode\"></div>\r\n\t<div class=\"buttonNode\">\r\n\t\t<div dojoType=\"fw.ui.dijit.Button\" buttonStyle=\"greySmall\" dojoAttachEvent=\"onClick:closeDialog\">OK</div>\r\n\t</div>\r\n</div>\r\n"),widgetsInTemplate:true,showCloseButton:false,message:"",title:"",_setMessageAttr:function(_1){this.messageNode.innerHTML=_1;this.message=_1;},_setTitleAttr:function(_2){this.titleNode.innerHTML=_2;this.title=_2;},closeDialog:function(){this.hide();this.destroyRecursive();}});}