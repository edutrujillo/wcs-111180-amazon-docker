/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dnd._treeSourceMixin"]){dojo._hasResource["fw.ui.dnd._treeSourceMixin"]=true;dojo.provide("fw.ui.dnd._treeSourceMixin");dojo.declare("fw.ui.dnd._treeSourceMixin",null,{dndDisableAttr:"disableDnd",constructor:function(_1){this.singular=true;},onDndStart:function(_2,_3,_4){if(_2==this){var _5,_6,_7,_8,_9=this.tree.model.store,_a=dijit.getEnclosingWidget,_b=fw.data.util.serializeStoreItem;if((_5=(_3&&_3.length&&_3[0]))&&(_6=_a(_5))&&(_7=_6.item)&&(_8=_b(_9,_7))&&(this._isDisableTreeNodeDnd(_8))){this.cancelDnd();return false;}}return this.inherited(arguments);},cancelDnd:function(){this.onDndCancel();this._hideDndAvatar();},_isDisableTreeNodeDnd:function(_c){_c=_c||{};var _d=_c[this.dndDisableAttr]||"";return ((_d==1)||(_d==true)||(_d=="1")||(_d.toString().toLowerCase()=="true")||(_d.toString().toLowerCase()=="yes"));},_hideDndAvatar:function(){var _e,_f,_10;if((_e=dojo.dnd.manager())&&(_f=_e.avatar)&&(_10=_f.node)){dojo.style(_10,"display","none");}}});}