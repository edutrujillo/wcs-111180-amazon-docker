/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dnd._treeSourceMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dnd._treeSourceMixin"]){_4._hasResource["fw.ui.dnd._treeSourceMixin"]=true;_4.provide("fw.ui.dnd._treeSourceMixin");_4.declare("fw.ui.dnd._treeSourceMixin",null,{dndDisableAttr:"disableDnd",constructor:function(_7){this.singular=true;},onDndStart:function(_8,_9,_a){if(_8==this){var _b,_c,_d,_e,_f=this.tree.model.store,gew=_5.getEnclosingWidget,ssi=fw.data.util.serializeStoreItem;if((_b=(_9&&_9.length&&_9[0]))&&(_c=gew(_b))&&(_d=_c.item)&&(_e=ssi(_f,_d))&&(this._isDisableTreeNodeDnd(_e))){this.cancelDnd();return false;}}return this.inherited(arguments);},cancelDnd:function(){this.onDndCancel();this._hideDndAvatar();},_isDisableTreeNodeDnd:function(_10){_10=_10||{};var obj=_10[this.dndDisableAttr]||"";return ((obj==1)||(obj==true)||(obj=="1")||(obj.toString().toLowerCase()=="true")||(obj.toString().toLowerCase()=="yes"));},_hideDndAvatar:function(){var _11,_12,_13;if((_11=_4.dnd.manager())&&(_12=_11.avatar)&&(_13=_12.node)){_4.style(_13,"display","none");}}});}}};});