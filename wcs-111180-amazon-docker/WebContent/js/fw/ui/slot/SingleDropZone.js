/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.slot.SingleDropZone"]){dojo._hasResource["fw.ui.slot.SingleDropZone"]=true;dojo.provide("fw.ui.slot.SingleDropZone");dojo.require("fw.ui.slot.BaseSlotSource");dojo.declare("fw.ui.slot.SingleDropZone",fw.ui.slot.BaseSlotSource,{_pubEvent:"/fw/dnd/newSlotItemDrop",_noswapPrompt:"The current contents of the target slot cannot be swapped "+"to the originating slot.  Proceed anyway?",_yesClickHandler:function(_1,_2,_3){this.onDropExternal(_1,_2,_3,true);_1._publishEvent();},markupFactory:function(_4,_5){_4._skipStartup=true;return new fw.ui.slot.SingleDropZone(_5,_4);},onDropExternal:function(_6,_7,_8,_9){if(!_9&&(_6 instanceof fw.ui.slot.SingleSlotSource||_6 instanceof fw.ui.slot.SingleDropZone)){if(_6.checkAcceptance(this,this.getAllNodes())){var _a=this.getAllNodes();this.inherited(arguments);_6.onDropExternal(this,_a,false,true);this._publishEvent();}else{this._askConfirmation(this._noswapPrompt,dojo.hitch(this,"_yesClickHandler",_6,_7,_8));}}else{var _b=this.getAllNodes(),i;this.inherited(arguments);this._publishEvent();}}});}