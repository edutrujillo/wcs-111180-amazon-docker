/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.slot.SingleSlotSource"]){dojo._hasResource["fw.ui.slot.SingleSlotSource"]=true;dojo.provide("fw.ui.slot.SingleSlotSource");dojo.require("fw.ui.slot.SingleDropZone");dojo.require("fw.ui.slot.ActivatorMixin");dojo.declare("fw.ui.slot.SingleSlotSource",[fw.ui.slot.SingleDropZone,fw.ui.slot.ActivatorMixin],{buttons:[],overlayTitle:"",constructor:function(_1){this._addActivator();dojo.connect(this,"onDndStart",this,function(){if(this._tooltip){this._tooltip.close();}});this.skipForm=true;},markupFactory:function(_2,_3){_2._skipStartup=true;return new fw.ui.slot.SingleSlotSource(_3,_2);},sync:function(){this.inherited(arguments);this._addActivator();},getOverlayParentNode:function(){var n=dojo.query(".dojoDndItem",this.getNode())[0];if(!n){n=this._emptyNode;}return n;}});}