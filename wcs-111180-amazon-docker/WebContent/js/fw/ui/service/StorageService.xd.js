/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.service.StorageService"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.service.StorageService"]){_4._hasResource["fw.ui.service.StorageService"]=true;_4.provide("fw.ui.service.StorageService");(function(){var _7="CONTRIBUTORUI:",_8=window.parent.WemContext,_9=_8?_8.getInstance():null,_a=function(_b){return _7+_b;};_4.declare("fw.ui.service.StorageService",null,{setAttribute:function(_c,_d,_e){if(_9){_9.setAttribute(_a(_c),_d,_e);}},getUserPreference:function(_f){var _10;if(_9){_10=_9.getUserPreference(_a(_f));}return _10;},getAttribute:function(_11){var _12;if(_9){_12=this.getUserPreference(_11);}return _12;},removeAttribute:function(_13){if(_9){_9.removeAttribute(_a(_13));}}});}());}}};});