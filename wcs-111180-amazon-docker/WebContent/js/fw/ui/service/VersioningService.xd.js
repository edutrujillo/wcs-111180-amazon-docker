/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.service.VersioningService"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.service.VersioningService"]){_4._hasResource["fw.ui.service.VersioningService"]=true;_4.provide("fw.ui.service.VersioningService");_4.declare("fw.ui.service.VersioningService",null,{VERSIONING:"UI/Layout/CenterPane/Versioning",checkout:"UI/Actions/Asset/RevisionTracking/Checkout",checkin:"UI/Actions/Asset/RevisionTracking/Checkin",undocheckout:"UI/Actions/Asset/RevisionTracking/UndoCheckout",constructor:function(_7){var _8=fw.ui.ObjectFactory;this.requestService=_8.createServiceObject("request");},invoke:function(_9,_a,_b){var _c=this,xn=_a&&_4.indexOf(["checkout","checkin","undocheckout"],_a)>-1?_a:"",_d,_e,_b=_b||{},_f={"assetIds":_9,"keepCheckedOut":_b.keepCheckedOut,"comment":_b.comment};if(_f.assetIds){var _e={elementName:this[xn],responseType:"json",params:_f,requestType:"post"};return _4.when(this.requestService.sendControllerRequest(_e),function(_10){return _10.status;});}else{_d=new _4.Deferred();_d.reject(fw.util.getString("UI/UC1/JS/CannotCheckin1"));return _d.promise;}}});}}};});