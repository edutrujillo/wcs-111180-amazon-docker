/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.data.stores.CSForestStoreModel"]){dojo._hasResource["fw.ui.data.stores.CSForestStoreModel"]=true;dojo.provide("fw.ui.data.stores.CSForestStoreModel");dojo.require("dijit.tree.ForestStoreModel");dojo.declare("fw.ui.data.stores.CSForestStoreModel",dijit.tree.ForestStoreModel,{getChildren:function(_1,_2,_3){var _4;if(_1.root==true){var _5="init";var id="0";var qs={oper:"init",startQpnt:0};var _6="Root";dojo.mixin(_1,{loadUrl:"init",refreshKey:"Root"});}else{var _5=this.store.getValue(_1,"loadUrl");var id=this.store.getValue(_1,"id");var _7=this.store.getValue(_1,"assetType");var qs=fw.ui.app.getFetchCommand();}var _4={loadUrl:_5,id:id,oper:qs.oper,startQpnt:qs.startQpnt};if(!_5){var _8="Unable to locate loadUrl.";console.warn(_8);_2([]);return;}var _9=this;this.store.fetch({query:dojo.mixin(_4,this.query),onComplete:_2,onError:function(_a){dojo.publish("/fw/ui/app/onerror",[_a]);}});},mayHaveChildren:function(_b){return dojo.some(this.childrenAttrs,function(_c){return this.store.hasAttribute(_b,_c)?this.store.getValue(_b,_c,true)==="true":false;},this);}});}