/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.manager.TreeManager"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.manager.TreeManager"]){_4._hasResource["fw.ui.manager.TreeManager"]=true;_4.provide("fw.ui.manager.TreeManager");(function(){function _7(){return _5.registry.byClass("fw.dijit.UITree").filter(function(w){return (w.dndController&&w.dndController.declaredClass=="fw.ui.dnd.TreeSource");});};_4.declare("fw.ui.manager.TreeManager",null,{updateActiveFrame:function(_8){var _9=_4.query("iframe",_8.containerNode)[0],_a=_7(),_b=this;if(_9){_a.forEach(function(_c){_c.dndController.setTargetFrame(_9);});}else{if(_8._xhrDfd){var _d=_4.connect(_8,"onLoad",function(){if(_8._isShown()){_b.updateActiveFrame(_8);}_4.disconnect(_d);});}else{_a.forEach(function(_e){_e.dndController.setTargetFrame(null);});}}},enableDragTracking:function(_f){_7().forEach(function(_10){_10.dndController.transferEnabled=(_f!==false?true:false);});}});}());}}};});