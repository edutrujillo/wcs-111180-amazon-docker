/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.manager.inner.StandaloneSlot"]){dojo._hasResource["fw.ui.manager.inner.StandaloneSlot"]=true;dojo.provide("fw.ui.manager.inner.StandaloneSlot");dojo.require("fw.ui.manager.inner.Slot");dojo.declare("fw.ui.manager.inner.StandaloneSlot",fw.ui.manager.inner.Slot,{set:function(_1){this.inherited(arguments);this.setStatus("ED");},isContentEditable:function(){var _2=this.isEditable();if(!_2){return false;}if(!this.isEmpty()&&this.isPreset("c")&&this.isPreset("cid")){this.reason=fw.util.getString("UI/UC1/JS/SlotContentNotEditable");return false;}return true;},clear:function(){if(this.assetType==="CSElement"||this.assetType==="SiteEntry"){this.voidSlotAsset();}else{if(!this.isPreset("c")){delete this.assetType;}if(!this.isPreset("cid")){delete this.assetId;delete this.assetName;}if(!this.tname||!this.isLayoutEditable()||this.tname===this.defaultTName){this.voidSlotAsset();}else{this.setStatus("ED");}}}});}