/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.view.SimpleAssetView"]){dojo._hasResource["fw.ui.view.SimpleAssetView"]=true;dojo.provide("fw.ui.view.SimpleAssetView");dojo.require("fw.ui.view.SimpleView");dojo.declare("fw.ui.view.SimpleAssetView",fw.ui.view.SimpleView,{getElementArgs:function(_1){var _2=this.get("model"),_3;_1=_1||{};if(_2){_3=_2.get("asset");_1.id=_3.id;_1.type=_3.type;_1.assetTypeParam=_3.type;}return _1;}});}