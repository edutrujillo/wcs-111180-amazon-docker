/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.view.DashboardView"],["require","fw.ui.service.StorageService"],["require","fw.ui.view.ServerView"],["require","fw.ui.view.TabbedViewMixin"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.view.DashboardView"]){_4._hasResource["fw.ui.view.DashboardView"]=true;_4.provide("fw.ui.view.DashboardView");_4.require("fw.ui.service.StorageService");_4.require("fw.ui.view.ServerView");_4.require("fw.ui.view.TabbedViewMixin");_4.declare("fw.ui.view.DashboardView",[fw.ui.view.ServerView,fw.ui.view.TabbedViewMixin],{viewElement:"UI/Layout/CenterPane/DashBoard",closable:false,refetchOnRefresh:true,getViewArgs:function(_7){var _8=fw.ui.ObjectFactory;var _9=_8.getService("storage");var _a=_9.getAttribute("dashboard");var _b;if(_a){_b=_a;}_4.mixin(_7,{"persistedPositions":_b});}});}}};});