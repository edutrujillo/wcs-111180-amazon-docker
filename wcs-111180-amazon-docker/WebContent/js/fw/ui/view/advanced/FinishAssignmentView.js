/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.view.advanced.FinishAssignmentView"]){dojo._hasResource["fw.ui.view.advanced.FinishAssignmentView"]=true;dojo.provide("fw.ui.view.advanced.FinishAssignmentView");dojo.require("fw.ui.view.AdvancedView");dojo.require("fw.ui.view.TabbedViewMixin");dojo.declare("fw.ui.view.advanced.FinishAssignmentView",[fw.ui.controller.BaseDocController,fw.ui.view.AdvancedView,fw.ui.view.TabbedViewMixin],{getAdvancedURLParams:function(){var _1=this.model.get("asset");if(_1){return {ThisPage:"SetStatusFront",AssetType:_1.type,id:_1.id,PostPage:"SetStatusPost",contentfunctions:"true"};}else{throw new Error(fw.util.getString("UI/UC1/JS/CannotRenderView"));}},show:function(){this.set("title",this.model.get("name"));return this.inherited(arguments);}});}