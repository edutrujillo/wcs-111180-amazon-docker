/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.view.advanced.RevisionView"]){dojo._hasResource["fw.ui.view.advanced.RevisionView"]=true;dojo.provide("fw.ui.view.advanced.RevisionView");dojo.require("fw.ui.view.AdvancedView");dojo.require("fw.ui.view.TabbedViewMixin");dojo.require("fw.ui.controller.BaseDocController");(function(){dojo.declare("fw.ui.view.advanced.RevisionView",[fw.ui.controller.BaseDocController,fw.ui.view.AdvancedView,fw.ui.view.TabbedViewMixin],{getAdvancedURLParams:function(){var _1=this.model.get("asset");if(_1&&this.params&&this.params.rev){return {ThisPage:"RevisionDetailsFront",AssetType:_1.type,id:_1.id,rev:this.params.rev};}else{throw new Error(fw.util.getString("UI/UC1/JS/CannotRenderRevision"));}},init:function(){this.inherited(arguments);var _2=this.params&&this.params.rev,_3="",_4;if(_2){_3=fw.util.getString("dvin/UI/AssetMgt/Version")+" "+_2;}this.toolbar.set("version",_3);}});})();}