/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.controller.CKEditorController"],["require","fw.ui.ObjectFactory"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.controller.CKEditorController"]){_4._hasResource["fw.ui.controller.CKEditorController"]=true;_4.provide("fw.ui.controller.CKEditorController");_4.require("fw.ui.ObjectFactory");_4.declare("fw.ui.controller.CKEditorController",null,{openCKDialog:function(_7){var _8,_9,_a,_b,_c=_7.args.isMultiVal,_d,_e;if(window.parent){_d=window.parent.top.WemContext;if(_d){_e=_d.getInstance();if(!_e.isSessionValid()){_e.directToTimeOutPage();}else{_8=fw.ui.ObjectFactory.createServiceObject("request");_b=_8.getControllerURL("UI/Layout/CenterPane/CKEditor",{responseType:"html",assettypes:_7.assettypes,args:_4.toJson(_7.args||{})});_9=new _5.layout.ContentPane({href:_b,"class":"CKEditorViewPane"});_a=fw.ui.ObjectFactory.getManager("dialog");_4.connect(_9,"_layout",this,function(){_a.getEnclosingDialog(_9.domNode).layout();});if(_c){_a.openDialogPane(_9,{"overlay":true});}else{_a.openDialogPane(_9);}}}}}});}}};});