/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.controller.DashboardController"]){dojo._hasResource["fw.ui.controller.DashboardController"]=true;dojo.provide("fw.ui.controller.DashboardController");dojo.require("fw.ui.controller.BaseController");dojo.require("fw.ui.view.DashboardView");dojo.declare("fw.ui.controller.DashboardController",fw.ui.controller.BaseController,{init:function(_1){var _2=this;if(!this.initialized){this.initialized=true;this.appContext=_1;if(!this.view){this.view=new fw.ui.view.DashboardView({appContext:this.appContext});this.view.controller=this;this.view.viewType="dashboard";this.view.set("title","");dojo.when(this.view.show({closable:false}),function(){_2.view.toolbar.onClick=dojo.hitch(_2,_2.onClick);});}}},onClick:function(_3,_4,_5){SitesApp.event(_3,_4,_5);},handleEvent:function(_6){if(_6.name==="refresh"){return this.refresh();}},refresh:function(){var _7=this,_8=new dojo.Deferred();dojo.when(this.view.refresh(),function(){_7.view.toolbar.onClick=dojo.hitch(_7,_7.onClick);_8.resolve();},function(e){_8.reject(e);});return _8;}});}