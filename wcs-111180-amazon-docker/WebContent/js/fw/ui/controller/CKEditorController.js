/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.controller.CKEditorController"]){dojo._hasResource["fw.ui.controller.CKEditorController"]=true;dojo.provide("fw.ui.controller.CKEditorController");dojo.require("fw.ui.ObjectFactory");dojo.declare("fw.ui.controller.CKEditorController",null,{openCKDialog:function(_1){var _2,_3,_4,_5,_6=_1.args.isMultiVal,_7,_8;if(window.parent){_7=window.parent.top.WemContext;if(_7){_8=_7.getInstance();if(!_8.isSessionValid()){_8.directToTimeOutPage();}else{_2=fw.ui.ObjectFactory.createServiceObject("request");_5=_2.getControllerURL("UI/Layout/CenterPane/CKEditor",{responseType:"html",assettypes:_1.assettypes,args:dojo.toJson(_1.args||{})});_3=new dijit.layout.ContentPane({href:_5,"class":"CKEditorViewPane"});_4=fw.ui.ObjectFactory.getManager("dialog");dojo.connect(_3,"_layout",this,function(){_4.getEnclosingDialog(_3.domNode).layout();});if(_6){_4.openDialogPane(_3,{"overlay":true});}else{_4.openDialogPane(_3);}}}}}});}