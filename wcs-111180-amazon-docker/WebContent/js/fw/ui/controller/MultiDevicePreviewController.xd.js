/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.controller.MultiDevicePreviewController"],["require","fw.ui.controller.PreviewController"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.controller.MultiDevicePreviewController"]){_4._hasResource["fw.ui.controller.MultiDevicePreviewController"]=true;_4.provide("fw.ui.controller.MultiDevicePreviewController");_4.require("fw.ui.controller.PreviewController");_4.declare("fw.ui.controller.MultiDevicePreviewController",fw.ui.controller.PreviewController,{changeDeviceGroup:function(_7){var _8=this.view;_8.setDeviceGroupInfo(_7.params);_8.slideOutDeviceImages();if(_8.loadingScreen){_4.style(_8.loadingScreen,"display","");}_8.show();},navigate:function(_9){}});}}};});