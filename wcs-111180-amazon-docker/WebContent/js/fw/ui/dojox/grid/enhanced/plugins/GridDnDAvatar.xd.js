/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

dojo._xdResourceLoaded(function(_1,_2,_3){return {depends:[["provide","fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar"],["require","dojo.dnd.Avatar"]],defineResource:function(_4,_5,_6){if(!_4._hasResource["fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar"]){_4._hasResource["fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar"]=true;_4.provide("fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar");_4.require("dojo.dnd.Avatar");_4.declare("fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar",_4.dnd.Avatar,{construct:function(){this.isA11y=_4.hasClass(_4.body(),"dijit_a11y");var a=_4.create("table",{"border":"0","cellspacing":"0","class":"dojoxGridDndAvatar","style":{position:"absolute",zIndex:"1999",margin:"0"}}),_7=this.manager.source,b=_4.create("tbody",null,a),_8=this.manager._dndPlugin.selected[0],_9=_7.grid.store,_a=_9._items[_8],_b=_9.getValue(_a,"name"),_c=_4.create("span",{"innerHTML":_b}),tr=_4.create("tr",null,b),td=_4.create("td",null,tr);td.appendChild(_c);_4.attr(tr,{"class":"dojoDndAvatarItem",style:{opacity:0.9}});this.node=a;}});}}};});