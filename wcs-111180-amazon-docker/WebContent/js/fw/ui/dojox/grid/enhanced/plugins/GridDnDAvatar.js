/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar"]){dojo._hasResource["fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar"]=true;dojo.provide("fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar");dojo.require("dojo.dnd.Avatar");dojo.declare("fw.ui.dojox.grid.enhanced.plugins.GridDnDAvatar",dojo.dnd.Avatar,{construct:function(){this.isA11y=dojo.hasClass(dojo.body(),"dijit_a11y");var a=dojo.create("table",{"border":"0","cellspacing":"0","class":"dojoxGridDndAvatar","style":{position:"absolute",zIndex:"1999",margin:"0"}}),_1=this.manager.source,b=dojo.create("tbody",null,a),_2=this.manager._dndPlugin.selected[0],_3=_1.grid.store,_4=_3._items[_2],_5=_3.getValue(_4,"name"),_6=dojo.create("span",{"innerHTML":_5}),tr=dojo.create("tr",null,b),td=dojo.create("td",null,tr);td.appendChild(_6);dojo.attr(tr,{"class":"dojoDndAvatarItem",style:{opacity:0.9}});this.node=a;}});}