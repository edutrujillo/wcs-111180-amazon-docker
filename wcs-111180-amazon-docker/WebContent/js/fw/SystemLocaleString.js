/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */

if(!dojo._hasResource["fw.SystemLocaleString"]){dojo._hasResource["fw.SystemLocaleString"]=true;dojo.provide("fw.SystemLocaleString");(function(){var _1=dojo.config.fw_csPath,_2=dojo.config.locale,_3="fatwire/ui/util/GetSLSObj",_4=_1+"ContentServer?pagename="+_3+"&user_locale="+_2,_5=function(_6){document.write("<script type=\"text/javascript\" src=\""+_6+"\"><"+"/script>");};if(typeof (dojo._xdIsXDomainPath)==="function"&&dojo._xdIsXDomainPath(_4)){_5(_4);}else{if(window.top===window.self){_5(_4);}}})();}