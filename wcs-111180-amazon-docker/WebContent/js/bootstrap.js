var innerController = null;
parent.fw.util.fetchSLS(["UI/Insite/DisplayInfo/Loadingscripts","UI/Insite/DisplayInfo/Parsingpage","UI/Insite/DisplayInfo/Registeringwidgets","UI/Insite/DisplayInfo/Initializing"]);

(function() {
	var csPath = parent.fw.util.getCSpath(),
		execAddOnLoad = false,
		loaderDiv,
		loadCss = function (filename) {
			var link = document.createElement("link");
			link.setAttribute("rel", "stylesheet");
			link.setAttribute("type", "text/css");
			link.setAttribute("href", filename);
			document.getElementsByTagName("head")[0].appendChild(link);
		};
		
	if (!window['insiteController'])
		window.insiteController = parent.fw.util._insiteController;

	window.djConfig = djConfig = {
		afterOnLoad: true,
		locale: parent.dojo.config.locale,
		fw_csPath: insiteController._csPath,
		addOnLoad: function () {
			execAddOnLoad = true;
			// synchronously load the layer file
			dojo.xhrGet({
				url: "js/fw/fw_ui_insite.js",
				handleAs: 'javascript',
				sync: true,
				failOk: true,
				error: function(e) {
					e.log = false;
					parent.console.log('fw_ui_insite.js layer file not found: falling back on source.');
				}
			});
			
			dojo.require("fw.ui._innerbase");
			
			dojo.xhrGet({
				url: "wemresources/js/dojopatches.js",
				handleAs: 'javascript',
				sync: false,
				failOk: true,
				error: function(e) {
					e.log = false;
					parent.console.log('dojopatches.js file not found');
				}
			});
			
			// load SWFUpload and CKEDITOR before starting parsing
			// note: insiteController is in the global scope
			var defs = new dojo.DeferredList([
				dojo.io.script.get({url: csPath + "js/SWFUpload/swfupload.js", checkString: "SWFUpload"}),
				dojo.io.script.get({url: csPath + "js/SWFUpload/plugins/swfupload.swfobject.js", checkString: "swfobject"}),
				dojo.io.script.get({url: csPath + "ckeditor/ckeditor.js", checkString: "CKEDITOR"})
			]);
			
			defs.then(function() {
				var parseOk = true;
				insiteController.onParse();
				try {
					// parse widgets 
					dojo.parser.parse();
				}
				catch(e) {
					parseOk = false;
					// widget parsing error
					// dojo was unable to parse the widgets in the page
					// the most likely cause is some wrong arguments being sent through
					// one of the insite tags.
					console.error('An error occurred while parsing the widgets on the page:', e);
					loaderDiv.style.display = 'none';
					insiteController.onParseError(e);
				}
				
				if (parseOk) {
					try {
						// initializes the ObjectFactory
						fw.ui.ObjectFactory.init(fw.ui.InnerConfig.aliases);
						// build innerController (fw.ui.manager.InsiteManager)
						innerController = fw.ui.ObjectFactory.createManagerObject("insite");
						innerController.init(insiteController).then(function() {
							loaderDiv.style.display = "none";
							// notify insite controller that the page is ready 
							insiteController.ready();
							
							// disable auto scrolling feature on insite frame
							// to avoid ackward page scrolling whenever user drag between main frame and insite frame
							dojo.dnd.autoScroll = function(e) {};
						});
					}
					catch(e) {
						console.error('An error occurred while initializing the insite controller', e);
						insiteController.onInitError();
					}
				}
			});
			
			//TODO: use only .fw-namespaced styles
			dojo.addClass(dojo.body(), 'fw insite');
		}
	};
	
	if(window.fw && window.fw.insiteLocale) {
		var siteLang = window.fw.insiteLocale;
		siteLang = siteLang.toLowerCase().replace("_", "-");
		if(siteLang !== parent.dojo.config.locale) {
			djConfig.extraLocale = [siteLang];
		}
	}
	
	loadCss(csPath + "js/dijit/themes/dijit.css");
	//TODO: make new CSS specifically for insite editing / etc.
	//(no font in .fw parent!)
	loadCss(csPath + "js/fw/css/Insite.css");	
	loadCss(csPath + "js/fw/css/ui/dnd_insite.css");
	loadCss(csPath + "js/fw/css/ui/Calendar.css");
	loadCss(csPath + "js/fw/css/ui/TooltipBar.css");
	loadCss(csPath + "js/fw/css/ui/HoverableTooltip.css");
	
	loaderDiv = document.createElement("div");
	loaderDiv.style.position = "fixed";
	loaderDiv.style.left = "0";
	loaderDiv.style.top = "0";
	loaderDiv.style.width = "100%";
	loaderDiv.style.height = "100%";
	loaderDiv.style.background = "url('wemresources/images/ui/wem/bgLayer.png')";
	loaderDiv.innerHTML = "<div style='width:100%;height:100%;background:url(wemresources/images/ui/wem/loading.png) no-repeat 50% 50%'></div>";
	
	document.body.appendChild(loaderDiv);
	insiteController.onBootstrap();
	
	var addScriptTag = function(path) {
		var s = document.createElement("script");
		s.type = "text/javascript";
		document.getElementsByTagName("head")[0].appendChild(s);
		s.src = path;
	}	
	addScriptTag(csPath + 'js/dojo/dojo.js');
	
	var timeout = 100;
	var manualLoad = function() {
		addScriptTag(csPath + 'js/dojo/_base/_loader/bootstrap.js');
		setTimeout(function() {
		addScriptTag(csPath + 'js/dojo/_base/_loader/loader.js');
		setTimeout(function() {
		addScriptTag(csPath + 'js/dojo/_base/_loader/hostenv_browser.js');
		setTimeout(function() {
		addScriptTag(csPath + 'js/dojo/_base.js');
		setTimeout(function() {
		if (!execAddOnLoad) window.djConfig.addOnLoad();
		}, timeout);
		}, timeout);
		}, timeout);
		}, timeout);
	}
	
	var isUncompressedCode = function() {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", csPath + 'js/dojo/_base.js', false);
		xhr.send();
		return (xhr.responseText || '').indexOf('return dojo') > 0;
	}	

	setTimeout(function() {
		try {
			if (!window['dojo'] && isUncompressedCode()) {
				manualLoad();
			}
		} catch(e) { }
	}, timeout + 150);
})();
