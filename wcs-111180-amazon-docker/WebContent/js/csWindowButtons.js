
/**
 Steps to add a new window for toggling.
 1. Make sure the window is named when opened and in the actually page itself
 2. Add the window reference
 4. Inport csWindows.js only once per window, meaning if there is a frameset it must be done only once, in the top window
 It must be in the top window, not the frame because you need to name the top window with a constant name and then refer to the
 JS methods via window.top in the frame window.
 5. Call the proper opener method
 6. Make sure the opener window makes a callback to clone the window refs.  Cannot do it until the open page is loaded
 */
//constants
var ADVANCED_SCREEN     ='AdvancedScreen';
var DASH_SCREEN         ='DashboardScreen';
var INSITE_SCREEN       ='InsiteScreen';
var WEM_INSITE_SCREEN       ='WemInsiteScreen';
var ANALYTICS_SCREEN    ='AnalyticsScreen'

//window references  intialize when this window creates another pop up window;
var dashboardWin;
var advancedWin;
var insiteWin;
var analyticsWin

function openAdvancedWindow( url )
{
    if( validReference( advancedWin ) )
    {
        //if advanced window exists, focus
        advancedWin.focus();
    }
    else
    {
        //create the reference for this child by opening up this window
        advancedWin = window.open(url, ADVANCED_SCREEN );
        advancedWin.focus();
        //new window is not yet opened..must set via callback method
        //cloneWindowReferences();
    }
}

// Modified: Jan 06, 2011 
// Reason  PR#24909  This fixes both Advanced/Dash 
//         UI Top navigation Insite button is not launching 
//         the Insite UI Preview WINDOW. 
// Open Insite window with preview URL Asset ... 
function focusInsiteWindow( insightUrl )
{
     insightUrl = insightUrl.replace(/^\s*/, '').replace(/\s*$/, '');
     // This takes care of both http and secure http
     if (insightUrl.search(/^https?\:\/\//) != -1)
     {
       if( validReference( insiteWin ) )
       {
         insiteWin.focus();
       }
       else
       {
          openPreview( insightUrl  ) ;
       }
    }
    else
    {
       // Refresh when invalid session in essence timeout
       if(insightUrl  == "Invalid_Session")
	   {
		 // Session is invalid. Refresh the page.
         // User will be redirected to the login page automatically
		 window.top.location.href = window.top.location.href;
	   }
	   else
	   {
	     // For good measure clean and strip all leading and trailing white space from
         // url crafting Exception Error message
		 alert(insightUrl.replace(/^\s+|\s+$/g,""))  ;
	   }
    }
}



//Did not name it openPreviewWindow because of Javascript namesapce collision with exisiting method. Why risk confusion
function openPreview( url )
{
    //Always open refresh contents, do not focus
    //create the reference for this child by opening up this window
    insiteWin = window.open(url, INSITE_SCREEN, 'width=1000,height=480,scrollbars=yes,toolbar=yes,location=yes,status=yes,menubar=yes,resizable=yes,directories=no' );
    insiteWin.focus();
    //new window is not yet opened..must set via callback method
    //cloneWindowReferences();
}

/**
if the ui is in wemframework parent window will have "wem=true" flag in parent window's url
*/
function isInWemFramework()
{
	var queryString = window.location.search;
	var queryString = queryString.substring(1);
	var params = queryString.split('&');
	for(i = 0; i < params.length; i++){		
		if(params[i].indexOf('wem=')==0)
		{
			var parametervalue = params[i].split('=');			
			if(parametervalue && parametervalue[1] =='true')
			{
				return true;
				
			} else
			{
				return false;
			}
		}
	}	
	return false;
}


/**
	check whether the url already has "wem=true"
*/
function isWemParamterAvailable(url)
{
	var indexOfQuestionMark = url.indexOf('?')
	if(indexOfQuestionMark > 1)
	{	
		var queryString =  url.substring(indexOfQuestionMark);
		var params = queryString.split('&');
		for(i = 0; i < params.length; i++){		
			if(params[i].indexOf('wem=')==0)
			{
				var parametervalue = params[i].split('=');			
				if(parametervalue && parametervalue[1] =='true')
				{
					return true;
					
				} else
				{
					return false;
				}
			}
		}	
	}
	return false;
}


function openDashWindow( url )
{
    //see if it exsits first before we open it
    if( validReference( dashboardWin ) )
    {
        //if Dash window exists, focus
        dashboardWin.focus();
    }
    else
    {
        //create the reference for this child by opening up this window
        dashboardWin = window.open(url, DASH_SCREEN );
        dashboardWin.focus();
        //new window is not yet opened..must set via callback method
        //cloneWindowReferences();
    }
}

function openAnalyticsWindow( url )
{
    //see if it exsits first before we open it
    if( validReference( analyticsWin ) )
    {
        //if Analytics window exists, focus
        analyticsWin.focus();
    }
    else
    {
        //create the reference for this child by opening up this window
        analyticsWin = window.open(url, ANALYTICS_SCREEN );
        analyticsWin.focus();
        //new window is not yet opened..must set via callback method
        //cloneWindowReferences();
    }
}


//copy all window references from the current window to all windowNodes
function cloneWindowReferences()
{
    
    if( validReference( dashboardWin ) && window.name != dashboardWin.name){
        dashboardWin.copyNodeReferences( window );
    }

    if( validReference( advancedWin ) && window.name != advancedWin.name){
        advancedWin.copyNodeReferences( window );
    }

    if( validReference( insiteWin ) && window.name != insiteWin.name){
        insiteWin.copyNodeReferences( window );
    }
    if( validReference( analyticsWin ) && window.name != analyticsWin.name){
        analyticsWin.copyNodeReferences( window );
    }
}

//function clearWindowReferences()
//{
//    dashboardWin= null
//    advancedWin = null;
//    insiteWin   = null;
//    analyticsWin= null;
//}

//clone this node with all the references
function copyNodeReferences(windowNode)
{
    //alert( 'adding references to ' + windowNode.name);
    dashboardWin= validReference(windowNode.dashboardWin);
    advancedWin = validReference(windowNode.advancedWin);
    insiteWin   = validReference(windowNode.insiteWin);
    analyticsWin= validReference(windowNode.analyticsWin);
    // alert(windowNode.name + 'now contains dashboardWin:' +windowNode.dashboardWin.name + " advancedWin:" + windowNode.advancedWin.name +
    //    " insiteWin:"+ windowNode.insiteWin + " analyticsWin:"+ windowNode.analyticsWin);
}


//if node is not null and still open
function validReference( windowNode )
{
    if( windowNode != null && !windowNode.closed ){
        return windowNode;
    }
    else{
        return null;
    }
}
