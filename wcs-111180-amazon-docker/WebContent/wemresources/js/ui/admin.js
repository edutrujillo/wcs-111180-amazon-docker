//TODO: consider putting this js under fw.wem package somewhere so that we can
//also use this file for the purpose of generating admin layer's dependencies.
dojo.require('fw.xhr');
dojo.require("dojo.parser");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("fw.dijit.UIDialog");
dojo.require("fw.dijit.UIMenuBar");
dojo.require('dijit.MenuBarItem');
dojo.require("fw.util");
/* don't need these for WEM admin
dojo.require("fw.dijit.UIMenu");
dojo.require("fw.dijit.UIMenuItem");
dojo.require("fw.dijit.UIMenuWithShadow");
dojo.require("dijit.MenuItem");
dojo.require("dijit.PopupMenuBarItem");
*/
/*Will be removed later*/
dojo.require("dijit.form.Textarea");
dojo.require('dijit.form.Button');

dojo.require("fw.dijit.UIFilteringSelect");
dojo.require("fw.dijit.UITable");
dojo.require("fw.dijit.UIInput");
dojo.require("fw.dijit.UITextarea");
dojo.require("fw.dijit.UINoteBox");
dojo.require("fw.dijit.UIWarningBox");
dojo.require("fw.dijit.UIComboBox");
dojo.require("fw.dijit.UIExpandableText");
dojo.require("fw.dijit.UITransferBox");
dojo.require("fw.dijit.UIUpload");
dojo.require('fw.dijit.UIForm');
dojo.require('fw.dijit.UIActionLink');
dojo.require('fw.dijit.UIActionImage');
dojo.require("fw.ui.dijit.Button");

dojo.require('fw.data.cs.ClientRestStore');
dojo.require('fw.data.cs.ServerRestStore');
dojo.require('fw.data.cs.UsersServerRestStore');
dojo.require('fw.data.SysLocStrCache'); //for client-side SLS retrieval

//WEM Admin UI specific
dojo.require('fw.wem.layout.UIContentPane');
dojo.require('fw.wem.AdminUIManager');
dojo.require("dojox.html.entities");
//global variables used through WEM Admin UI - aliases, common handles, etc.
var AdminUIManager, populate, contentPane, wemcontext,
	wemprefix = 'WEMUI:Admin:',
	cookietimeout = 365;
if (window.parent.WemContext) {
	wemcontext = window.parent.WemContext.getInstance();
}

//functions used for table rows-per-page cookie storage/retrieval
//(called within scope of UITable widget - see containerLoaded)
function storeRPPCookie(count) {
	if (wemcontext) {
		wemcontext.setCookie(wemprefix + 'rowsPerPage', count.toString(),
			cookietimeout);
	}
}
function restoreRPPCookie() {
	if (wemcontext) {
		var temp = parseInt(wemcontext.getCookie(wemprefix + 'rowsPerPage'), 10);
		if (!isNaN(temp)) {
			this.changeCount(temp);
		}
	}
}

//TODO: this function should no longer be needed once we switch to
//validation + UIActionImage.
function disableIfNoneSelected(btn,transferbox){
	transferbox.connect(transferbox, 'updateSelected', function() {
		var btnName = btn.src.substr(0, btn.src.lastIndexOf('.'));
		var selected = transferbox.get('value');
		//Disable only if there are nothing selected or if the button is not alredy disabled
		if(selected.length === 0 && btnName.match("Disabled$")!='Disabled'){
			btn._prevonclick = btn.onclick;
			btn.onclick = null;
			btn._prevsrc = btn.src;
			btn.src = btnName + 'Disabled' +
			btn.src.substr(btn.src.lastIndexOf('.'));
			dojo.style(btn, 'cursor', 'default');
		} else if(selected.length != 0){
			if(btn._prevonclick && btn._prevsrc){
				btn.onclick = btn._prevonclick;
				btn.src=btn._prevsrc;
				btn.removeAttribute('_prevonclick');
				btn.removeAttribute('_prevsrc');
			}
		}
	});
}

function disableBtns(btns){
	//btns is now expected to be an array of widgets with settable disabled attrs
	dojo.forEach(btns,
		function(btn) {
			btn.set('disabled', true);
		}
	);
}

function validateTableAccess(table){
	var pageIdVal = dojo.byId('pageId').value;
	//As per the latest UI the validation for siteadmin needs to be done only on the site/Browse page
	//Other page like role/user/app will be disabled for siteadmin
	if(AdminUIManager._fullAccess || pageIdVal != 'site/Browse')
		return;
	var conn = dojo.connect(table, 'onShowPopup',table,function(row) {
		var popupelm = dojo.byId(this.id+"popup"+row.index);
		var identity = this.store.getIdentity(this.getItem(row.index)).toString();
		var menuItemTypes = [];
		if(dojo.indexOf(AdminUIManager.siteAdminSites,identity)!=-1){
			menuItemTypes=['Edit','Delete'];
		} else {
			//This is in the case of AdminSite
			menuItemTypes=['Edit','Delete','Assign'];
		}
		for(var j=0;j < menuItemTypes.length;j++){
			var elements = dojo.query('.UITable' + menuItemTypes[j]);
			for(var i=0; i < elements.length;i++){
				elements[i]._orighref = elements[i].href;
				elements[i].onclick = function() { return false; }
				elements[i].className = 'UITable' + menuItemTypes[j] +'Disabled';
			}
		}
		if(menuItemTypes.length < 3){
		//That means Assign needs to be enabled 
			var elements = dojo.query('.UITableAssignDisabled');
			for(var i=0; i < elements.length;i++){
				elements[i].className = 'UITableAssign';
				delete elements[i].onclick;
			}
		}
	});
}


function validateBtnAccess(btns){
	var pageIdVal = dojo.byId('pageId').value;
	var pageIdArray = ['user/AssignSites','app/AssignSites','site/AssignUsers','site/AssignApps'];
	for(i in pageIdArray){
		if(pageIdVal == pageIdArray[i]){
			return;
		}
	}
		
	//Only disable the button if its not the AssignSites page since the logic to disable buttons in AssignSites page is different then this
	if(!AdminUIManager._fullAccess){
		disableBtns(btns);
	}
}

function disableContinueIfNoneSelected(transferbox){
	disableIfNoneSelected(dojo.byId('btnContinue'),transferbox);
	//refresh the transfer box
	transferbox.updateSelected();
}

function disableSaveIfNoneSelected(transferbox){
	disableIfNoneSelected(dojo.byId('btnSaveAndClose'),transferbox);
	//refresh the transfer box
	transferbox.updateSelected();
}

//TODO: centralize populate and updateFormFields functions for table pages?

//callback hooked to onBegin of store fetches for available-vs-assigned compares
//uses then clears global object obj (a sub-object from AUIM.editObj)
function disableAddIfAllAssigned(size) {
	var btn = dijit.byId('btnAdd');
	if (size === obj.length && btn) {
		//all items are already in table; disable button
		//(TODO: revisit when image buttons get widgetified and/or a-href'd)
		btn.set('disabled', true);
	}
	delete obj; //we're done with this now
}

function disableAddSiteIfAllAssigned(items) {
	var btn = dijit.byId('btnAdd');
	
	var disabled = true;
	for(var i =0 ; i < items.length ; i++){
		var _fsitename = items[i].name[0];
		//console.log(_fsitename);
		var matched = false;
		for(var j=0;j < obj.length ; j++){
			var _esitename = obj[j].site;
			//console.log(_esitename);
			if(_fsitename == _esitename){
				matched=true;
				break;
			}
		}
		if(!matched) {
			disabled= false;
			break;
		}
	}
	if(disabled && btn){
		//all items are already in table; disable button
		btn.set('disabled', true);
	}
	delete obj; //we're done with this now
}

function showMessageIfNoneAssigned(size) {
	if (size == 0) {
		var noun = dojo.byId('Heading:title').innerHTML,
			btnName = fw.util.getString('fatwire/wem/button/Assign'); 
		if(fw.util.getString('fatwire/wem/admin/site/assignApps/Heading').localeCompare(noun) == 0){
			btnName = fw.util.getString('fatwire/wem/button/AssignApps');
			noun = fw.util.getString('fatwire/wem/admin/app/browse/Heading');
		} else if(fw.util.getString('fatwire/wem/admin/site/assignUsers/Heading').localeCompare(noun) == 0){
			btnName = fw.util.getString('fatwire/wem/button/AssignUsers');
			noun = fw.util.getString('fatwire/wem/admin/user/browse/Heading');
		} else if(fw.util.getString('fatwire/wem/admin/user/assignSites/Heading').localeCompare(noun) == 0){
			btnName = fw.util.getString('fatwire/wem/button/AssigntoSites');
			noun = fw.util.getString('fatwire/wem/admin/site/browse/Heading');
		} else if(fw.util.getString('fatwire/wem/admin/app/assignSites/Heading').localeCompare(noun) == 0){
			btnName = fw.util.getString('fatwire/wem/button/AssigntoSites');
			noun = fw.util.getString('fatwire/wem/admin/site/browse/Heading');
		} 
		noun = noun.toLocaleLowerCase();
		var msgBox = dijit.byId('msg');
		if(msgBox) {
			msgBox.show('info', '<span class=\'status\'>' + fw.util.getString('fatwire/wem/admin/common/Info') + '</span>' + fw.util.getString('fatwire/wem/admin/common/NotCurrentlyAssignedClickBtn',{type:noun,btn:btnName}));
		} else {
			new fw.dijit.UIWarningBox({
				type: 'info',
				html:'<span class=\'status\'>' + fw.util.getString('fatwire/wem/admin/common/Info') + '</span>' + 
				fw.util.getString('fatwire/wem/admin/common/NotCurrentlyAssignedClickBtn',{type:noun,btn:btnName})
			}, dojo.byId('msg'));
		}
	}
}

function encodeHtmlEntities(value)
{
	return dojox.html.entities.encode(value);
}


//simple formatter for name fields: bolds contents
function formatBold(value, rowIndex) {
	return '<b>' + value + '</b>';
}

//formatter used on our assign pages to display roles in table
function formatList(value, rowIndex, property) {
  var
    uniqid = 'format' + property + '_' + rowIndex,
    list = [],
    listStr = '',
    widgetid = 'widget_' + uniqid,
    i = 0;
  if (!this.grid.getItem(rowIndex)) return ''; //empty row
  
  list = this.grid.store.getValues(this.grid.getItem(rowIndex), property);
  if (!list) return ''; //TODO: message
  for (i = 0; i < list.length; i++) {
    if (i > 0) listStr += ', ';
    listStr += list[i];
  }
	//This is necessary right now; ideally should be taken care of by UITable...
  this.grid.cleanFromRegistry(widgetid); //avoid double-format-call issues
  //place widget inside our uniquely-identified div once it's available
  this.grid.placeWidget(new fw.dijit.UIExpandableText({
    id: widgetid,
    content: listStr,
    charsToShow: 60
  }), uniqid);
  return '<div id="' + uniqid + '"></div>';
}
function formatRoles(value, rowIndex) {
	return dojo.hitch(this, formatList, value, rowIndex, 'roles')();
}
function formatSites(value, rowIndex) {
	return dojo.hitch(this, formatList, value, rowIndex, 'sites')();
}

//formatter used for description fields
//TODO: make generic operating on passed value (stop using getValues!)
function formatDescription(value, rowIndex) {
  var
    uniqid = 'formatDescription_' + rowIndex,
    widgetid = 'widget_' + uniqid,
		desc = '';
  if (!this.grid.getItem(rowIndex)) return ''; //empty row
  
  desc = value;
  if (!desc) return '';
	//This is necessary right now; ideally should be taken care of by UITable...
  this.grid.cleanFromRegistry(widgetid); //avoid double-format-call issues
  //place widget inside our uniquely-identified div once it's available
  this.grid.placeWidget(new fw.dijit.UIExpandableText({
    id: widgetid,
    content: desc,
    charsToShow: 80
  }), uniqid);
  return '<div id="' + uniqid + '"></div>';
}

//formatter for user info column of Users browse table
function formatUserInfo(value, rowIndex) {
	//TODO: allow 3 image sizes?
	var
		store = this.grid.store,
		item = this.grid.getItem(rowIndex),
		imgsize = 45,
		imgsrc = store.getValue(item,
			'thumbimagesrc', dojo.config.fw_csPath + 'wemresources/images/emptyPhoto.png'),
		locale = store.getValue(item, 'locale'),
		localesrc = dojo.config.fw_csPath + 'wemresources/images/table/nolocale.gif',
		login = store.getValue(item, 'lastloggedin', ''),
		str = '';
	if (!login) {
		login = fw.util.getString('fatwire/wem/admin/common/Never');
	} 
	
	if (locale) {
		//replace default with actual locale image (reuse from Dash)
		localesrc = dojo.config.fw_csPath + 'images/icons/' + locale + '.gif';
	}
	//TODO: format login time
	
	//left part - user image
	str = '<table cellspacing="0" class="usersInfoTable"><tr>';
	str += '<td><div class="usersThumb">';
	//TODO: should this be done for Safari as well?
	if(dojo.isIE && imgsrc != dojo.config.fw_csPath + 'wemresources/images/emptyPhoto.png'){
		var spanid = 'userImg_' + rowIndex;
		str +='<span id="' + spanid + '"></span>';
		var conn = dojo.connect(this.grid, 'onFormatterComplete', function() {
			elementsImageReplacement.display(imgsrc.substring(imgsrc.indexOf(',')+1),
				spanid, imgsize, imgsize);
			dojo.disconnect(conn);
		});
	} else {
		if(imgsrc != dojo.config.fw_csPath + 'wemresources/images/emptyPhoto.png'){
			str +='<img src="' + imgsrc + '"/>';
		} else {
			str +='<img src="' + imgsrc + '" width="' + imgsize + '"/>';
		}
	}
	str += '&nbsp;</div></td>';
	//right part - username, locale image, e-mail, last logged in time
	str += '<td><div class="usersInfo"><div class="userName">' +
		store.getValue(item, 'name', '') +
		'</div><div class="userEmail">' +
		store.getValue(item, 'email', '') +
		'</div><div class="userLogin"><span class="userLoginLabel">' +
		fw.util.getString('fatwire/wem/admin/user/browse/LoggedIn') +
		'</span> <span class="userLoginTime">' + login + '</span></div></div></td>';
	str += '</tr></table>';
	return str;
}

//function to clear out common functions we know we redefine
function clearFuncs() {
	populate = null;
	preprocess = null;
	postprocess = null;
}

function containerDownloaded(data) {
	//TODO: consider moving some of this logic into AdminUIManager?
	
	//if this page contains a table, connect cookie-related functions to it
	var table = dijit.byId('UITable');
	if (table) {
		table.connect(table, 'changeCount', storeRPPCookie);
		if (table._started) {
			dojo.hitch(table, restoreRPPCookie)();
		} else {
			table.connect(table, 'onInitialFormatComplete', restoreRPPCookie);
		}
		//Validate access for the table rows
		validateTableAccess(table);
	}
	
	//if this page contains mainform, look likely action widget candidates
	var
		mainform = dijit.byId('mainform'),
		widgetIds = ['btnContinue', 'btnSaveAndClose', 'btnSave'];
	
	if (mainform) {
		for (var i = 0; i < widgetIds.length; i++) {
			var w = dijit.byId(widgetIds[i]);
			if (w) {
				mainform.addActionWidget(w);
			}
		}
	}
	
	//look for standard functions defined by newly loaded content and execute	
	if (typeof(populate) === 'function' &&
			AdminUIManager.dataArray.length > AdminUIManager.activePage) {
		populate(dojo.fromJson(AdminUIManager.dataArray[AdminUIManager.activePage].pageDataVal));
	}
	if (typeof(preprocess) === 'function') {
		preprocess();
	}
	//Validate access for add button
	var btnAdd = dijit.byId('btnAdd');
	if(btnAdd)
		validateBtnAccess([btnAdd]);
}

dojo.addOnLoad(function() {
	//set up aliases to commonly used wem functions/objects
	AdminUIManager = fw.wem.AdminUIManager; //alias to AdminUIManager object
	//TODO: don't need conditional once Home application is up to snuff
	contentPane = dijit.byId('wemcontent');
	if (contentPane) {
		contentPane.connect(contentPane, 'onDownloadEnd', containerDownloaded);
		contentPane.connect(contentPane, 'onDownloadStart', clearFuncs);
	}
});
