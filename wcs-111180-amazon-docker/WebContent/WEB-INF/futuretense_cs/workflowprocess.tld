<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE taglib
        PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        "http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd">

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>workflowprocess</shortname>
	<info>A tag library for Content Centre workflow process methods.</info>
	<tag>
		<name>checkprocessrequiresparticipants</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Checkprocessrequiresparticipants</tagclass>
		<info>
			Verifies whether a process requires participants.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	<tag>
		<name>scatter</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Scatter</tagclass>
		<info>
			Retrieves all fields of a loaded workflow process object and "scatters" their values into individual variables sharing a common prefix.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gather</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Gather</tagclass>
		<info>
			Retrieves name/value pairs from the environment, and sets them into a loaded workflow process object. The name/value pairs are grouped together using a prefix and a prefix separator.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getid</tagclass>
		<info>
			Gets the id of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getprocessname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getprocessname</tagclass>
		<info>
			Gets the name of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setprocessname</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setprocessname</tagclass>
		<info>
			Sets the name of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getprocessdescription</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getprocessdescription</tagclass>
		<info>
			Gets the description of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setprocessdescription</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setprocessdescription</tagclass>
		<info>
			Sets the description of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdeadlinetype</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getdeadlinetype</tagclass>
		<info>
			Gets the deadline type of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setdeadlinetype</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setdeadlinetype</tagclass>
		<info>
			Sets the deadline type of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getroles</tagclass>
		<info>
			Gets the roles for which the workflow process is valid.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setroles</tagclass>
		<info>
			Sets the roles for which the workflow process is valid.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getadminroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getadminroles</tagclass>
		<info>
			Gets the admin roles of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setadminroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setadminroles</tagclass>
		<info>
			Sets the admin roles for the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getsites</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getsites</tagclass>
		<info>
			Gets the sites for which a workflow process is valid.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setsites</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setsites</tagclass>
		<info>
			Sets the sites for which a workflow process is valid.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>gettypes</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Gettypes</tagclass>
		<info>
			Gets the asset types for which a workflow process is valid.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>settypes</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Settypes</tagclass>
		<info>
			Sets the asset types for which a workflow process is valid.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getdelegateactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getdelegateactions</tagclass>
		<info>
			Gets the delegation actions of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setdelegateactions</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setdelegateactions</tagclass>
		<info>
			Sets the delegation actions of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getfunctionprivs</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getfunctionprivs</tagclass>
		<info>
			Gets the function privileges of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setfunctionprivs</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setfunctionprivs</tagclass>
		<info>
			Sets the function privileges of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>object</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getsteps</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getsteps</tagclass>
		<info>
			Gets the workflow steps of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>setsteps</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Setsteps</tagclass>
		<info>
			Sets the workflow steps of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>prefix</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>addstep</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Addstep</tagclass>
		<info>
			Adds a step to the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getstep</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getstep</tagclass>
		<info>
			Gets the step with the given id from the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>objvarname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>deletestep</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Deletestep</tagclass>
		<info>
			Deletes the step with the given id from the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>clearsteps</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Clearsteps</tagclass>
		<info>
			Deletes all steps from the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getinitialstepid</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getinitialstepid</tagclass>
		<info>
			Gets the id of the start step in the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>getsourceroles</name>
		<tagclass>com.openmarket.xcelerate.jsp.workflowprocess.Getsourceroles</tagclass>
		<info>
			Gets the authorized roles of the start state of the workflow process.
		</info>
		<attribute>
			<name>name</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>varname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>sourcestate</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>


</taglib>
