<%
String locale = (String)session.getAttribute("locale");
String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
		
if(locale==null || "".equals(locale))
{
	locale = defaultLocale;
}

String contextPath = request.getContextPath();
%>
<html>
<HEAD>
<link href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css' rel="styleSheet" type="text/css" />
<link href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css' rel="styleSheet" type="text/css" />
</HEAD>
<body>
<br/>
<table cellspacing="0" cellpadding="0" border="1">
<tbody>
<tr>
	<td class="message-bg-color">
		<table cellspacing="0" cellpadding="3" border="0">
			<tbody>
				<tr>
				<td valign="top">
					<img height="22" width="25" src="<%=contextPath%>/Xcelerate/graphics/common/msg/info.gif">
				</td>
				<td><h3>
				<jsp:include page="/cachetool/translator.jsp">
					<jsp:param name="key" value="message.message"/>
					<jsp:param name="render" value="true"/>
				</jsp:include>
				</h3>
				</td></tr>
				</tbody>
				</table>
			</td></tr>
</tbody>
</table>
</body>
</html>