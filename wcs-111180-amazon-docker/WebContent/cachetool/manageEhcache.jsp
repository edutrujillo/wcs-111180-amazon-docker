<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.fatwire.cs.systemtools.util.CacheUtil"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
	String locale = (String)session.getAttribute("locale");
	String defaultLocale = (String)session.getAttribute("defaultLocale");
	defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
			
	if(locale==null || "".equals(locale))
	{
		locale = defaultLocale;
	}

    String authorize = (String) session.getAttribute("userAuthorized");
    if(Boolean.valueOf(authorize))
    {
        String action = request.getParameter("cacheAction");
        if(!(CacheUtil.FLUSH.equals(action) || CacheUtil.CLEAR.equals(action)))
        {
            out.println("Not supported action: " + StringEscapeUtils.escapeHtml(action) + ".");
        }
        else
        {
            String cacheName = request.getParameter("cachename");
            if(!(CacheUtil.CS_CACHE.equals(cacheName) || CacheUtil.SS_CACHE.equals(cacheName) || CacheUtil.CAS_CACHE.equals(cacheName)))
                cacheName = CacheUtil.SS_CACHE;
            
            CacheManager cacheMgr =
                CacheUtil.getCacheManager(cacheName);
            if(null == cacheMgr)
            {
			%><jsp:include page="/cachetool/translator.jsp">
				<jsp:param name="key" value="assetCacheDep.noCacheManager"/>
				<jsp:param name="render" value="true"/>
			</jsp:include><%
			}
            else
            {
                String[] caches = null;
                String logoffuser = "false";
                if(CacheUtil.CLEAR.equals(action))
                {
                    caches = request.getParameterValues("FlushCache");
                    // logg off the user if the session cache is flushed
                    for(String ch:caches)
                    {
                    	if(ch.equals("sessioncache"))
                    	{
                    		logoffuser = "true";
                    		break;
                    	}
                    }
                }
                else
                    caches = request.getParameterValues("WriteCache");
                
                Collection<String> cacheNames = Arrays.asList(caches);
                String message =
                    CacheUtil.manageCache(cacheMgr, action,cacheNames );
%>
<jsp:include page="./summary.jsp">
	<jsp:param name="message" value="<%=message%>" />
	<jsp:param name="logoffuser" value="<%=logoffuser %>" />
</jsp:include>
<%
    }
        }
    }
    else
    {
	%><jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="common.accessError"/>
		<jsp:param name="render" value="true"/>
	</jsp:include><%
    }
%>
