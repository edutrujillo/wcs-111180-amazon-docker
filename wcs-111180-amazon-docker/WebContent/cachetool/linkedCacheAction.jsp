<%@ page import="java.util.*"%>
<%@ page import="COM.FutureTense.Cache.*"%>
<%@ page import="com.fatwire.cache.*"%>
<%
String locale = (String)session.getAttribute("locale");
String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
		
if(locale==null || "".equals(locale))
{
	locale = defaultLocale;
}

String authorize = (String) session.getAttribute(
"userAuthorized");
if (Boolean.valueOf(authorize)) {
    String action = request.getParameter("act");
    if(action != null)
    {
        boolean clear = action.startsWith("clear");
        boolean invalidate = action.startsWith("invalidate");
        boolean remove = action.startsWith("remove");

        if(clear)
        {
            String [] parts = action.split("clear");
            LinkedCache lc = LinkedCacheProvider.INSTANCE.getCache(parts[1]);
            if(null != lc)
                lc.clear();
        }

        if(invalidate)
        {
            String [] parts = action.split("invalidate");
            LinkedCache lc = LinkedCacheProvider.INSTANCE.getCache(parts[1]);
            if(null != lc)
            {
                lc.invalidateDependencies(Collections.singletonList(parts[1]));
            }
        }

        if(remove)
        {
            String [] parts = action.split("remove");
            LinkedCache lc = LinkedCacheProvider.INSTANCE.getCache(parts[1]);
            if(null != lc)
                lc.remove(parts[1]);
        }

    }
}
else
{%><jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="common.accessError"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
<%
}
%>

