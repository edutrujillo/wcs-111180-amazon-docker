<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@ page import="COM.FutureTense.Interfaces.IList"%>
<%@ page import="COM.FutureTense.Cache.LinkedCache"%>
<%@ page import="COM.FutureTense.Cache.LinkedCacheProvider"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
String locale = (String)session.getAttribute("locale");
String defaultLocale = (String)session.getAttribute("defaultLocale");
defaultLocale = defaultLocale == null ? "en_US": defaultLocale;
		
if(locale==null || "".equals(locale))
{
	locale = defaultLocale;
}

String authorize = (String) session.getAttribute(
"userAuthorized");
if (Boolean.valueOf(authorize)) {
String cache = request.getParameter("cache");
String key = com.fatwire.cs.core.uri.Util.decode(request.getParameter("key"));
key = StringEscapeUtils.escapeHtml(key);
LinkedCache linkedCache = LinkedCacheProvider.INSTANCE.getCache(cache);
    IList list = (IList)linkedCache.get(key);
if("".equals(cache) || cache == null || "".equals(key) || key == null || list == null)
{
	%>
	<jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="linkedCache.message"/>
	</jsp:include>
	<%
	String message =  (String)request.getAttribute("linkedCache.message");  
	message = message.replace("{0}",key).replace("{1}",cache);
	%><%=message%><%
}
else
{
String contextPath = request.getContextPath();
int maxRows = 100;
String[] column = new String[list.numColumns()];
%>
<html>
<head>
<LINK href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/common.css' rel="stylesheet" type="text/css" />
<LINK href='<%=contextPath%>/Xcelerate/data/css/<%=locale%>/content.css' rel="stylesheet" type="text/css" />
</head>
<body>
<table>
<tr>
 <td><span class="title-text">
<jsp:include page="/cachetool/translator.jsp">
	<jsp:param name="key" value="linkedCache.resultSet"/>
	<jsp:param name="render" value="true"/>
</jsp:include>
 </span></td>
</tr>
<tr>
	<td><div class='form-inset'><%=key%></div></td>
</tr>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0">
<tr> 
    <td></td>
    <td class="tile-dark" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
	<td></td>
</tr>
<tr>
	<td class="tile-dark" VALIGN="top" WIDTH="1"><br/></td>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
	<tr>
		<td colspan='<%=2*column.length + 1 %>' class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
	</tr>
	<tr>
		<td class="tile-a" background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;</td>
			<%
			for(int c=list.numColumns()-1; c>=0; c--) {
			column[c] = list.getColumnName(c);
			%>
			<td class="tile-b" background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>
				<DIV class="new-table-title"><%=column[c]%></DIV>
			</td>
			<td class="tile-b" background='<%=contextPath%>/Xcelerate/graphics/common/screen/grad.gif'>&nbsp;&nbsp;
			</td>
			<%}%>
	</tr>
	<tr>
		<td colspan='<%= 2*column.length + 1 %>' class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' /></td>
	</tr>
		<%
		int max = list.numRows();
		if(max==0) {
		  %><td colspan='<%= 2*column.length + 1 %>'>
		  <jsp:include page="/cachetool/translator.jsp">
			<jsp:param name="key" value="linkedCache.noRows"/>
			<jsp:param name="render" value="true"/>
		  </jsp:include>
		  </td><%
		}
		if(maxRows>=0) {
			max = Math.min(max, maxRows);
		}
		int num = 0 ;
		for(int r=1; r<=max; r++) {
			list.moveTo(r);
			if (((num++) %2 ) !=0) 
			{ %>
			<tr class="tile-row-highlight"><td><br/></td>
			<% }
			else {
			  %>
			<tr class="tile-row-normal"><td><br/></td>
			<% } 
			
			for(int c=list.numColumns()-1; c>=0; c--) {
			String value = list.getValue(column[c]);
			%><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset"><%=value%></div></td>
			<td><br/></td>
			<%}%>
			</tr>
		<%}%>	
		</table>
	</td>
	<td class="tile-dark" VALIGN="top" WIDTH="1"><br/></td>
 </tr>
<tr>
	<td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' />
</tr>
<tr>
	<td></td>
	<td background='<%=contextPath%>/Xcelerate/graphics/common/screen/shadow.gif'><IMG WIDTH="1" HEIGHT="5" src='<%=contextPath%>/Xcelerate/graphics/common/screen/dotclear.gif' />
	<td></td>
</tr>
</table>
	<%
    if(max<list.numRows()) {
	  %>
	  <jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="linkedCache.more"/>
	  </jsp:include>
	  <%
	  String more =  (String)request.getAttribute("linkedCache.more");
	  more = more.replace("{0}",String.valueOf(list.numRows()-max));
	  %><%=more%><%
    }
	}
%>
</body>
</html>
<%
}
else{
%>
<jsp:include page="/cachetool/translator.jsp">
		<jsp:param name="key" value="common.accessError"/>
		<jsp:param name="render" value="true"/>
</jsp:include>
<%}%>