/*
 * 	EmbeddedLink English language file.
*/

CKEDITOR.plugins.setLang( 'fwincludeasset', 'zh',
{
	fwincludeasset :
	{
	
	   AddEmbeddedLinkDlgTitle		: '新增資產連結',
	   IncludeEmbeddedLinkDlgTitle	: '包括資產' ,
	   PleaseSelectLinkTextFrom	    : '必須先選取資產連結的文字' ,
	   PleaseSelectAssetFromTree	: '從樹狀結構中選取資產.',
	   PleaseSelectAssetFromSearchResults : '從搜尋結果中選取資產',
	   PleaseSelectOnlyOneAsset 	: '僅可選取一個資產.',
	   CannotAddSelfInclude	        : '不允許透過「包括」的自我參照.\n請選取其他資產.',
       CannotLinkOrIncludeThisAsset : '不允許連結或包括此資產.\n請選取下列類型的資產:\n'
			    
	}
});


