﻿/***************************************************************************************
                            fwincludeasset
  Created: March 25, 2010
  By:      JAG
  Reason:  The FW Tray ToolBar  Inclide Asset Command
           to perform the Include asset command and
           functionality

  Modified:  It's possible to customize every single aspect of the editor,
  from the toolbar, to the skin, to dialogs,
  and here we customize the context menu to detect all
  FW  included and linked Assets in an instance of running ckeditor.


  The editor structure is totally plugin based,
  including many of its core features.
  Plugins live in separate files, making them easy to organize and maintain.
  CKEditor  JavaScript API makes developing plugins flexible.

  Modified: Localization

  CKEditor automatically detects the user language,
  automatically configuring its interface to be localized to it.
  This editor speaks the language your users speak.

**********************************************************************************/

CKEDITOR.plugins.add('fwincludeasset',   
{
	requires : [ 'fwincludeasset' ],

	lang : ['en','fr','es','de','it','ja','ko','zh-cn','zh','pt-br'],

	// When Instace of the Editor is Created this is the Initilization Callback
	// for the FW plug-in Include Asset button with the editor instance
	// handle being passed into The Initilization

	// TODO rename editor to ckeditor
    init:function(editor)
	{ 
		var b = "fwincludeasset";
	   // The Execute Command for this Plug-in Button .... when user Clicks on the 
		// Generate command Button
		var c = editor.addCommand(b, {
			// Click on Include Asset Toolbar Button ...
			exec : function(ckeditor) // assetname, assetId, assetType,
										// fieldname, fielddesc, embedtype,
										// isFckEditor
			{
				var env,fieldname, ckconfig=ckeditor.config,
				 cs_environment =  ckconfig.cs_environment ,assettypes;
	            if(cs_environment!='ucform'&&cs_environment!='insite')
	            {
				 // Check if Site tree is Visible                
                showSiteTree = ckconfig.showSiteTree ;

                assetName =  ckconfig.assetName ;
				   assetId = ckconfig.assetId ;
				   assetType =  ckconfig.assetType ;
				   fieldname =  ckconfig.fieldName ;
				   fielddesc = ckconfig.fieldDesc ;
				   editingstyle = ckconfig.editingstyle ;
                // TODO  Review  .. 6 Functionality  Add Asset without Tree
                // Have we been called From Advanced UI
                if(showSiteTree=="true")
                {
                     CKAdvancedUI.addIncludedAssetT( assetName,assetId,assetType,fieldname,fielddesc,editingstyle,ckeditor) ;
                     // Completed Processing the Asset Include Command and Operation
                     return ; 
                }else {
					CKAdvancedUI.addIncludedAsset(ckeditor) ;
				}
			}else{
				//Strip off the postfix for fieldname depending on the editingstyle of the content attribute.
				fieldname = ((!ckconfig.fieldName)? ckeditor.name : 
							((ckconfig.editingstyle==="single")? ckconfig.fieldName : ckconfig.fieldName.substr(0,ckconfig.fieldName.lastIndexOf('_'))));
				env=(CKEDITOR.env.webkit)? "webkit" : ((CKEDITOR.env.gecko)? "gecko":"ie");
				assettypes= (!ckconfig.allowedassettypes)? "*": ckconfig.allowedassettypes;
				// CKEditor must invoke the dialogmanager which is a singleton object.
				var CK = ckconfig.isMultiValued ? new fw.ui.controller.CKEditorController : new parent.fw.ui.controller.CKEditorController;
				CK.openCKDialog({
					args : {
						action : 'insert',
						item : 'asset',
						fieldName : fieldname,
						assetId : ckconfig.assetId,
						assetType :  ckconfig.assetType,
						DEPTYPE : ckconfig.DEPTYPE,
						env : env,
						isMultiVal : ckconfig.isMultiValued,
						deviceid: ckconfig.deviceid
					},
					assettypes : assettypes	
				});
			}
				
			},
                   canUndo: false    // No support for undo/redo
		});

	
	  	c.modes={wysiwyg:1,source:0};

		c.canUndo = false;

		// Add in essence register the Include Asset Button
		editor.ui.addButton("fwincludeasset", {
			label : editor.lang.fwincludeasset.IncludeEmbeddedLinkDlgTitle,
			command : b,
			icon : this.path + "fwincludeasset.gif"
		});

	},

	// Nothing to do right now.. Future Post processing may be needed
     afterInit : function( editor )
	 {
		// Register a filter to displaying placeholders after mode change.
	}
});

// TODO Move etCKEditorSelection outside the class
/*************************************************************************************
                          CKEditor Via Advanced UI
 Created; April 15, 2010
 Reason :   FCKNonEditable  CKEditor frameworks
 This is a plugin to make the include assets non editable.
 Also includes some utility and helper methods.
*************************************************************************************/

var CKAdvancedUI =
{
	// Called to perform Asset Include into Advanced Select From Tree
    addIncludedAssetT : function ( assetname,assetId,AssetType, fieldname, fielddesc,editingstyle, editor )
    {
		// Select from Asset
        var EncodedString = parent.parent.frames["XcelWorkFrames"].frames["XcelTree"].document.TreeApplet.exportSelections()+'';
		var allowedassettypes = editor.config.allowedassettypes;

        if ( EncodedString!=null )
        {
			var idArray = EncodedString.split(',');
			var assetcheck = unescape(idArray[0]);
         if (assetcheck.indexOf('assettype=')!=-1 && assetcheck.indexOf('id=')!=-1)
         {
				var test = new String(EncodedString);
				var nameVal = test.split(",");
				if (nameVal.length != 2) {
					alert(editor.lang.fwincludeasset.PleaseSelectOnlyOneAsset);
					return false;
				}

				var treeid = unescape(nameVal[0]);
				var splitid = treeid.split(',');
           if (splitid.length==1)
           {
					alert(editor.lang.fwincludeasset.PleaseSelectAssetFromTree);
					return false;
				}
				var splitpair = splitid[0].split("=");
				var tn_id = splitpair[1];
				splitpair = splitid[1].split("=");
				var tn_AssetType = splitpair[1];
		   if ( tn_id == assetId && tn_AssetType == AssetType  )
	       {
					alert(editor.lang.fwincludeasset.CannotAddSelfInclude);
					return false;
				}
				// Check if the selected assettype is allowed
		   if(allowedassettypes && allowedassettypes.toUpperCase().indexOf(tn_AssetType.toUpperCase()) == -1){
				alert(editor.lang.fwincludeasset.CannotLinkOrIncludeThisAsset + allowedassettypes);
					return false;
				}

				// Craft The URL to be added
				var pURL = editor.config.csRootContext +"ContentServer?pagename=OpenMarket/Xcelerate/Actions/";

				// Call AddInclusion.xml for Asset Popup Dialog
				pURL = pURL + "AddInclusion";
				pURL = pURL + "&tn_id=" + tn_id;
				pURL = pURL + "&tn_AssetType=" + tn_AssetType;
				pURL = pURL + "&name=" + encodeURIComponent(assetname);
				pURL = pURL + "&AssetType=" + AssetType;
				pURL = pURL + "&fieldname=" + encodeURIComponent(fieldname);
				pURL = pURL + "&fielddesc=" + encodeURIComponent(fielddesc);
				pURL = pURL + "&formFieldName=" + encodeURIComponent(fieldname);
				pURL = pURL + "&EditingStyle=" + encodeURIComponent(editingstyle);
				pURL = pURL + "&IFCKEditor=true";

           return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=500,location=no,menubar=no,resizable=yes,status=no,toolbar=no,scrollbars=yes,width=500,height=650");
        }
        else
        {
				alert(editor.lang.fwincludeasset.PleaseSelectAssetFromTree);
			}
       }
       else
       {
			alert(editor.lang.fwincludeasset.PleaseSelectAssetFromTree);
		}
	},
	addIncludedAsset : function ( editor )
    {         
	   var ckConfig = editor.config;
	   var allowedassettypes = ckConfig.allowedassettypes;
	   var assetname =  ckConfig.assetName ;
	   var assetId = ckConfig.assetId ;
	   var AssetType =  ckConfig.assetType ;
	   var fieldname =  ckConfig.fieldName ;
	   var fielddesc = ckConfig.fieldDesc ;
	   var editingstyle = ckConfig.editingstyle ;
	   var pURL = editor.config.csRootContext +  "ContentServer?pagename=OpenMarket/Xcelerate/Actions/PickAssetPopup";	   
	
       var i=0, histString='', currentVal;
       if (parent.parent.parent['XcelBanner'])
       {
		 for(i=0;i<parent.parent.parent['XcelBanner'].hist.length;i++)
		 {
				currentVal = parent.parent.parent['XcelBanner'].hist[i];
				if (i==0)
					histString=currentVal.id;
				else
					histString=histString+","+currentVal.id;
		 }
       }
	   pURL = pURL + "&FCKName=" + encodeURIComponent(assetname);
	   pURL = pURL + "&cs_SelectionStyle=single";
       pURL = pURL + "&cs_CallbackSuffix=";
	   pURL = pURL + "&IFCKEditor=true";
       pURL = pURL + "&FCKAssetId=" + assetId;
	   pURL = pURL + "&FCKAssetType=" + AssetType;
	   pURL = pURL + "&fielddesc=" + encodeURIComponent(fielddesc);
       pURL = pURL + "&cs_FieldName=" + encodeURIComponent(fieldname);
       pURL = pURL + "&EditingStyle=" + encodeURIComponent(editingstyle);
	   pURL = pURL + "&embedtype=include";
       pURL = pURL + "&cs_History=" + encodeURIComponent(histString);
	   if(allowedassettypes){
		pURL = pURL + "&cs_AllowedAssetType=" + encodeURIComponent(allowedassettypes);
	   }
	   return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=500,location=no,menubar=no,resizable=yes,scrollbars=yes,top=50,width=500,height=550");
	}
};
