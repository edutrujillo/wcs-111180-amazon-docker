/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwincludenewasset', 'ja',
{
	fwincludenewasset :
	{
	  assetCreatorIncludeDlgTitle : 'アセット作成者',
	  assetCreatorIncludeTitleDesc : '新規アセットの作成および包含',
	  assetCreatorLinkTitleDesc : '新規アセットの作成およびリンク',
	  PleaseSelectLinkTextFrom	: 'アセットへリンクするテキストを先に選択する必要があります' 
	
	
	  	    
	}
});


