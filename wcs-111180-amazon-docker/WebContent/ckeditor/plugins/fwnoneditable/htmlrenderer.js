﻿/*
    File Name: fwcustomrenderer.js
    Author :   J. Granato

    Reason :   defines  to the data processor which is
    responsible to translate and transform the CKeditor data on input and output.
    html FORMAT AND Decoder FWCKDataRenderer

    The Customized FW CKeditor Renderer  Encoding and Decoding HTML
    for Include and Link Assets

    HTML is a standard, but it's not the only format to have data.
    There is also wiki format, bbcode, xml, docbook and thousand others.
    CKEditor is able to handle all these formats by the development
    of custom Data Processors. In essence   FW  Asset Include / Link Data Renderer 


     Technically speaking, writing the final output is a task executed by the CKEDITOR.htmlWriter class ("writer"),
     used by the CKEDITOR.htmlDataProcessor class.
     Therefore, the current writer instance for a specific editor instance can be retrieved by the <editorInstance>.dataProcessor.writer property.

     By setting the writer properties, it's possible to
     configure several output formatting options.

     // TODO Eliminate all dead code and/or functions not called
     // Place all used functions  local scope Dataprocessor
*/
    // Regex to scan for &nbsp; at the end of blocks, which are actually placeholders.
	// Safari transforms the &nbsp; to \xa0. (#4172)
	 
	


/***************************************************************************************
  Overide default Rendere and Data processor Class definition
* Returns a string representing the HTML format of "data". The returned
* value will be loaded in the editor.
* The HTML must be from <html> to </html>, eventually including
* the DOCTYPE.
 *     @param {String} data The data to be converted in the
	 *            DataProcessor specific format.
*/
(function()
{
	// Regex to scan for &nbsp; at the end of blocks, which are actually placeholders.
	// Safari transforms the &nbsp; to \xa0. (#4172)
	var tailNbspRegex = /^[\t\r\n ]*(?:&nbsp;|\xa0)$/;

	var protectedSourceMarker = '{cke_protected}';

	// Return the last non-space child node of the block (#4344).
	function lastNoneSpaceChild( block )
	{
		var lastIndex = block.children.length,
			last = block.children[ lastIndex - 1 ];
		while (  last && last.type == CKEDITOR.NODE_TEXT && !CKEDITOR.tools.trim( last.value ) )
			last = block.children[ --lastIndex ];
		return last;
	}

	function trimFillers( block, fromSource )
	{
		// If the current node is a block, and if we're converting from source or
		// we're not in IE then search for and remove any tailing BR node.
		//
		// Also, any &nbsp; at the end of blocks are fillers, remove them as well.
		// (#2886)
		var children = block.children, lastChild = lastNoneSpaceChild( block );
		if ( lastChild )
		{
			if ( ( fromSource || !CKEDITOR.env.ie ) && lastChild.type == CKEDITOR.NODE_ELEMENT && lastChild.name == 'br' )
				children.pop();
			if ( lastChild.type == CKEDITOR.NODE_TEXT && tailNbspRegex.test( lastChild.value ) )
				children.pop();
		}
	}

	function blockNeedsExtension( block, fromSource, extendEmptyBlock )
	{
		if( !fromSource && ( !extendEmptyBlock ||
			typeof extendEmptyBlock == 'function' && ( extendEmptyBlock( block ) === false ) ) )
			return false;

	// 1. For IE version >=8,  empty blocks are displayed correctly themself in wysiwiyg;
	// 2. For the rest, at least table cell and list item need no filler space.
	// (#6248)
	if ( fromSource && CKEDITOR.env.ie &&
		( document.documentMode > 7
			|| block.name in CKEDITOR.dtd.tr
			|| block.name in CKEDITOR.dtd.$listItem ) )
		return false;

		var lastChild = lastNoneSpaceChild( block );

		return !lastChild || lastChild &&
				( lastChild.type == CKEDITOR.NODE_ELEMENT && lastChild.name == 'br'
				// Some of the controls in form needs extension too,
				// to move cursor at the end of the form. (#4791)
				|| block.name == 'form' && lastChild.name == 'input' );
	}

	function getBlockExtension( isOutput, emptyBlockFiller )
	{
		return function( node )
		{
			trimFillers( node, !isOutput );

			if ( blockNeedsExtension( node, !isOutput, emptyBlockFiller ) )
			{
				if ( isOutput || CKEDITOR.env.ie )
					node.add( new CKEDITOR.htmlParser.text( '\xa0' ) );
				else
					node.add( new CKEDITOR.htmlParser.element( 'br', {} ) );
			}
		};
	}

	var dtd = CKEDITOR.dtd;

	// Define orders of table elements.
	var tableOrder = [ 'caption', 'colgroup', 'col', 'thead', 'tfoot', 'tbody' ];

	// Find out the list of block-like tags that can contain <br>.
	var blockLikeTags = CKEDITOR.tools.extend( {}, dtd.$block, dtd.$listItem, dtd.$tableContent );
	for ( var i in blockLikeTags )
	{
		if ( ! ( 'br' in dtd[i] ) )
			delete blockLikeTags[i];
	}
	// We just avoid filler in <pre> right now.
	// TODO: Support filler for <pre>, line break is also occupy line height.
	delete blockLikeTags.pre;
	var defaultDataFilterRules =
	{
		elements : {},
		attributeNames :
		[
			// Event attributes (onXYZ) must not be directly set. They can become
			// active in the editing area (IE|WebKit).
			[ ( /^on/ ), 'data-cke-pa-on' ]
		]
	};

	var defaultDataBlockFilterRules = { elements : {} };

	for ( i in blockLikeTags )
		defaultDataBlockFilterRules.elements[ i ] = getBlockExtension();

	var defaultHtmlFilterRules =
		{
			elementNames :
			[
				// Remove the "cke:" namespace prefix.
				[ ( /^cke:/ ), '' ],

				// Ignore <?xml:namespace> tags.
				[ ( /^\?xml:namespace$/ ), '' ]
			],

			attributeNames :
			[
				// Attributes saved for changes and protected attributes.
				[ ( /^data-cke-(saved|pa)-/ ), '' ],

				// All "data-cke-" attributes are to be ignored.
				[ ( /^data-cke-.*/ ), '' ],

				[ 'hidefocus', '' ]
			],

			elements :
			{
				$ : function( element )
				{
					var attribs = element.attributes;

					if ( attribs )
					{
						// Elements marked as temporary are to be ignored.
						if ( attribs[ 'data-cke-temp' ] )
							return false;

						// Remove duplicated attributes - #3789.
						var attributeNames = [ 'name', 'href', 'src' ],
							savedAttributeName;
						for ( var i = 0 ; i < attributeNames.length ; i++ )
						{
							savedAttributeName = 'data-cke-saved-' + attributeNames[ i ];
							savedAttributeName in attribs && ( delete attribs[ attributeNames[ i ] ] );
						}
					}

					return element;
				},

				// The contents of table should be in correct order (#4809).
				table : function( element )
				{
					var children = element.children;
					children.sort( function ( node1, node2 )
								   {
									   return node1.type == CKEDITOR.NODE_ELEMENT && node2.type == node1.type ?
											CKEDITOR.tools.indexOf( tableOrder, node1.name )  > CKEDITOR.tools.indexOf( tableOrder, node2.name ) ? 1 : -1 : 0;
								   } );
				},

				embed : function( element )
				{
					var parent = element.parent;

					// If the <embed> is child of a <object>, copy the width
					// and height attributes from it.
					if ( parent && parent.name == 'object' )
					{
						var parentWidth = parent.attributes.width,
							parentHeight = parent.attributes.height;
						parentWidth && ( element.attributes.width = parentWidth );
						parentHeight && ( element.attributes.height = parentHeight );
					}
				},
				// Restore param elements into self-closing.
				param : function( param )
				{
					param.children = [];
					param.isEmpty = true;
					return param;
				},

				// Remove empty link but not empty anchor.(#3829)
				a : function( element )
				{
					if ( !( element.children.length ||
							element.attributes.name ||
							element.attributes[ 'data-cke-saved-name' ] ) )
					{
						return false;
					}
				},

				// Remove dummy span in webkit.
				span: function( element )
				{
					if ( element.attributes[ 'class' ] == 'Apple-style-span' )
						delete element.name;
				},

				// Empty <pre> in IE is reported with filler node (&nbsp;).
				pre : function( element ) { CKEDITOR.env.ie && trimFillers( element ); },

				html : function( element )
				{
					delete element.attributes.contenteditable;
					delete element.attributes[ 'class' ];
				},

				body : function( element )
				{
					delete element.attributes.spellcheck;
					delete element.attributes.contenteditable;
				},

				style : function( element )
				{
					var child = element.children[ 0 ];
					child && child.value && ( child.value = CKEDITOR.tools.trim( child.value ));

					if ( !element.attributes.type )
						element.attributes.type = 'text/css';
				},

				title : function( element )
				{
					var titleText = element.children[ 0 ];
					titleText && ( titleText.value = element.attributes[ 'data-cke-title' ] || '' );
				}
			},

			attributes :
			{
				'class' : function( value, element )
				{
					// Remove all class names starting with "cke_".
					return CKEDITOR.tools.ltrim( value.replace( /(?:^|\s+)cke_[^\s]*/g, '' ) ) || false;
				}
			}
		};

	if ( CKEDITOR.env.ie )
	{
		// IE outputs style attribute in capital letters. We should convert
		// them back to lower case, while not hurting the values (#5930)
		defaultHtmlFilterRules.attributes.style = function( value, element )
		{
			return value.replace( /(^|;)([^\:]+)/g, function( match )
				{
					return match.toLowerCase();
				});
		};
	}

	function protectReadOnly( element )
	{
		var attrs = element.attributes;

		// We should flag that the element was locked by our code so
		// it'll be editable by the editor functions (#6046).
		if ( attrs.contenteditable != "false" )
			attrs[ 'data-cke-editable' ] = attrs.contenteditable ? 'true' : 1;

		attrs.contenteditable = "false";
	}
	function unprotectReadyOnly( element )
	{
		var attrs = element.attributes;
		switch( attrs[ 'data-cke-editable' ] )
		{
			case 'true':	attrs.contenteditable = 'true';	break;
			case '1':		delete attrs.contenteditable;	break;
		}
	}
	// Disable form elements editing mode provided by some browers. (#5746)
	for ( i in { input : 1, textarea : 1 } )
	{
		defaultDataFilterRules.elements[ i ] = protectReadOnly;
		defaultHtmlFilterRules.elements[ i ] = unprotectReadyOnly;
	}

	var protectElementRegex = /<(a|area|img|input)\b([^>]*)>/gi,
		protectAttributeRegex = /\b(on\w+|href|src|name)\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|(?:[^ "'>]+))/gi;

	var protectElementsRegex = /(?:<style(?=[ >])[^>]*>[\s\S]*<\/style>)|(?:<(:?link|meta|base)[^>]*>)/gi,
		encodedElementsRegex = /<cke:encoded>([^<]*)<\/cke:encoded>/gi;

	var protectElementNamesRegex = /(<\/?)((?:object|embed|param|html|body|head|title)[^>]*>)/gi,
		unprotectElementNamesRegex = /(<\/?)cke:((?:html|body|head|title)[^>]*>)/gi;

	var protectSelfClosingRegex = /<cke:(param|embed)([^>]*?)\/?>(?!\s*<\/cke:\1)/gi;

	function protectAttributes( html )
	{
		return html.replace( protectElementRegex, function( element, tag, attributes )
		{
			return '<' +  tag + attributes.replace( protectAttributeRegex, function( fullAttr, attrName )
			{
				// Avoid corrupting the inline event attributes (#7243).
				// We should not rewrite the existed protected attributes, e.g. clipboard content from editor. (#5218)
				if ( !( /^on/ ).test( attrName ) && attributes.indexOf( 'data-cke-saved-' + attrName ) == -1 )
					return ' data-cke-saved-' + fullAttr + ' ' + fullAttr;

				return fullAttr;
			}) + '>';
		});
	}

	function protectElements( html )
	{
		return html.replace( protectElementsRegex, function( match )
			{
				return '<cke:encoded>' + encodeURIComponent( match ) + '</cke:encoded>';
			});
	}

	function unprotectElements( html )
	{
		return html.replace( encodedElementsRegex, function( match, encoded )
			{
				return decodeURIComponent( encoded );
			});
	}

	function protectElementsNames( html )
	{
		return html.replace( protectElementNamesRegex, '$1cke:$2');
	}

	function unprotectElementNames( html )
	{
		return html.replace( unprotectElementNamesRegex, '$1$2' );
	}

	function protectSelfClosingElements( html )
	{
		return html.replace( protectSelfClosingRegex, '<cke:$1$2></cke:$1>' );
	}

	function protectPreFormatted( html )
	{
		return html.replace( /(<pre\b[^>]*>)(\r\n|\n)/g, '$1$2$2' );
	}

	function protectRealComments( html )
	{
		return html.replace( /<!--(?!{cke_protected})[\s\S]+?-->/g, function( match )
			{
				return '<!--' + protectedSourceMarker +
						'{C}' +
						encodeURIComponent( match ).replace( /--/g, '%2D%2D' ) +
						'-->';
			});
	}

	function unprotectRealComments( html )
	{
		return html.replace( /<!--\{cke_protected\}\{C\}([\s\S]+?)-->/g, function( match, data )
			{
				return decodeURIComponent( data );
			});
	}

	function unprotectSource( html, editor )
	{
		var store = editor._.dataStore;

		return html.replace( /<!--\{cke_protected\}([\s\S]+?)-->/g, function( match, data )
			{
				return decodeURIComponent( data );
			}).replace( /\{cke_protected_(\d+)\}/g, function( match, id )
			{
				return store && store[ id ] || '';
			});
	}

	function protectSource( data, editor )
	{
		var protectedHtml = [],
			protectRegexes = editor.config.protectedSource,
			store = editor._.dataStore || ( editor._.dataStore = { id : 1 } ),
			tempRegex = /<\!--\{cke_temp(comment)?\}(\d*?)-->/g;

		var regexes =
			[
				// Script tags will also be forced to be protected, otherwise
				// IE will execute them.
				( /<script[\s\S]*?<\/script>/gi ),

				// <noscript> tags (get lost in IE and messed up in FF).
				/<noscript[\s\S]*?<\/noscript>/gi
			]
			.concat( protectRegexes );

		// First of any other protection, we must protect all comments
		// to avoid loosing them (of course, IE related).
		// Note that we use a different tag for comments, as we need to
		// transform them when applying filters.
		data = data.replace( (/<!--[\s\S]*?-->/g), function( match )
			{
				return  '<!--{cke_tempcomment}' + ( protectedHtml.push( match ) - 1 ) + '-->';
			});

		for ( var i = 0 ; i < regexes.length ; i++ )
		{
			data = data.replace( regexes[i], function( match )
				{
					match = match.replace( tempRegex, 		// There could be protected source inside another one. (#3869).
						function( $, isComment, id )
						{
							return protectedHtml[ id ];
						}
					);

					// Avoid protecting over protected, e.g. /\{.*?\}/
					return ( /cke_temp(comment)?/ ).test( match ) ? match
						: '<!--{cke_temp}' + ( protectedHtml.push( match ) - 1 ) + '-->';
				});
		}
		data = data.replace( tempRegex,	function( $, isComment, id )
			{
				return '<!--' + protectedSourceMarker +
						( isComment ? '{C}' : '' ) +
						encodeURIComponent( protectedHtml[ id ] ).replace( /--/g, '%2D%2D' ) +
						'-->';
			}
		);

		// Different protection pattern is used for those that
		// live in attributes to avoid from being HTML encoded.
		return data.replace( /(['"]).*?\1/g, function ( match )
		{
			return match.replace( /<!--\{cke_protected\}([\s\S]+?)-->/g, function( match, data )
			{
				store[ store.id ] = decodeURIComponent( data );
				return '{cke_protected_'+ ( store.id++ )  + '}';
			});
		});
	}

	CKEDITOR.htmlDataProcessor = function( editor )
	{
		this.editor = editor;

		this.writer = new CKEDITOR.htmlWriter();
		this.dataFilter = new CKEDITOR.htmlParser.filter();
		this.htmlFilter = new CKEDITOR.htmlParser.filter();
	};

	CKEDITOR.htmlDataProcessor.prototype =
	{
		/*
	 * Returns a string representing the HTML format of "data". The returned
	 * value will be loaded in the editor.
	 * The HTML must be from <html> to </html>, eventually including
	 * the DOCTYPE.
	 *
	 @param {String} data The data to be converted in the
	 *            DataProcessor specific format.
	 */
	_includeElement:'DIV',
	_includeElementForLink:'SPAN',
	_includeAttribute:'_fwid',
	_linkElement:'A',
	_linkAttribute:'_fwhref',
	ieNoContent: true,
	toHtml : function( data, fixForBody )
		{
			// The source data is already HTML, but we need to clean
			// it up and apply the filter.
			data = protectSource( data, this.editor );
			// Before anything, we must protect the URL attributes as the
			// browser may changing them when setting the innerHTML later in
			// the code.
			
			//Commenting the following code since it creates the oopies of href
			//as data-cke-saved-href and that causes issue when we fetch data from the server.
			//data = protectAttributes( data );
			
			// Protect elements than can't be set inside a DIV. E.g. IE removes
			// style tags from innerHTML. (#3710)
			data = protectElements( data );
			// Certain elements has problem to go through DOM operation, protect
			// them by prefixing 'cke' namespace. (#3591)
			data = protectElementsNames( data );
			// All none-IE browsers ignore self-closed custom elements,
			// protecting them into open-close. (#3591)
			data = protectSelfClosingElements( data );
			// Compensate one leading line break after <pre> open as browsers
			// eat it up. (#5789)
			data = protectPreFormatted( data );
			// Call the browser to help us fixing a possibly invalid HTML
			// structure.
			var div = new CKEDITOR.dom.element( 'div' );
			// Add fake character to workaround IE comments bug. (#3801)
			div.setHtml( 'a' + data );
			data = div.getHtml().substr( 1 );
			// Unprotect "some" of the protected elements at this point.
			data = unprotectElementNames( data );
			data = unprotectElements( data );
			// Restore the comments that have been protected, in this way they
			// can be properly filtered.
			data = unprotectRealComments( data );

			// Now use our parser to make further fixes to the structure, as
			// well as apply the filter.
			var fragment = CKEDITOR.htmlParser.fragment.fromHtml( data, fixForBody ),
				writer = new CKEDITOR.htmlParser.basicWriter();

			fragment.writeHtml( writer, this.dataFilter );
			data = writer.getHtml( true );

			// Protect the real comments again.
			data = protectRealComments( data );

			var ckeditor = this.editor ;
			//Bypass server side calls during paste operation of assets or embedded links.
			//Asset has already been rendered and the contents are present in the clipboard.
			if(data.indexOf(this._includeAttribute)!=-1||data.indexOf(this._linkAttribute)!=-1) return data;
			if(data && data.length > 0 && this.editor.config.doHtml){
				var xhrargs = {
					url: 'ContentServer',
					content: {
						pagename: 'fatwire/ui/controller/controller',
						elementName: 'UI/Data/CKEditor/Renderer',
						responseType: 'text',
						fieldData: data,
						deviceid: ckeditor.config.deviceid,
						renderedData: ''
					},
					handleAs: 'text',
					sync: true,
					timeout: 2000,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
					},
					load:function(data){
						this.renderedData= data;
					},
					error: function(e) {
						throw new Error('An error occurred while trying to update the displayedvalue: ' + e.message);
					}
				};
				var PostObject = dojo.xhrPost(xhrargs);
				//Data received from the server is going to be html encoded data so we need to decode it here
				var elm = document.createElement('div');
				elm.innerHTML = PostObject.ioArgs.args.renderedData;
				var unescapedData = (elm.childNodes.length === 0 ? "" : elm.childNodes[0].nodeValue);
				//Strip any html or body or link tag in the rendered data since this would break fckeditor.Remove line feeds/tab/carriage return etc.Also remove any comments from the html string.Satellite server may send <!--FTCACHE-1--> with the html data.
				var renderedData = (unescapedData).replace(/[\r\n\t]/g,"");
				var _fullPage = String(ckeditor.config.fullPage);
				if(_fullPage == undefined || (_fullPage != null && _fullPage.toLowerCase() != 'true')){
					renderedData = renderedData.replace(/<head[^>]*>.*?<\/head[^>]*>/gi,"").replace(/<\/?(body|html)[^>]*?>/gi,"");
				}
				this.editor.config.doHtml = true ;
				return renderedData;

			} else {
				return '';			
			}
		},
		
		/*******************************************************************************************
                                FWConvertToDataFormat
			*     Converts a DOM (sub-)tree to a string in the data format.
			*     @param {Object} rootNode The node that contains the DOM tree to be
			*            converted to the data format.
			*     @param {Boolean} excludeRoot Indicates that the root node must not
			*            be included in the conversion, only its children.
			*     @param {Boolean} format Indicates that the data must be formatted
			*            for human reading. Not all Data Processors may provide it.

				  // We need to get The HTML document body.
				  // This element is always present in the DOM API,
				  // even if the tags are not present in the source document. S


			******************************************************************************************/
     FWConvertToDataFormat : function ( editor, html )
     {		
        var tags = new Array(this._includeElementForLink,this._includeElement);
		var  rootNode = document.createElement("div");		  
        rootNode.innerHTML = html ;	
		if(CKEDITOR.env.ie && this.ieNoContent){ 
	        /*
	        *	Bug 14602171: CKEDITOR - ISSUE WHEN ADDING FULL STOPS AFTER LINK ASSETS
			*
	        *	In IE (8/9), problem occurs when user inserts an asset with empty space(s) in the asset template 
	        *	after the value. When the user deletes the space near the asset and inserts some content, the 
	        *	first character always disappears after switching modes between source and WYSIWYG mode.
	        *	Ideally, the new content should be pushed after the asset.
			*   
			*	Fix: For now, we are appending a dummy node (#tempNode) to the html which has to be inserted.
			*   We only remove the dummy node when it is empty (&nbsp;). Otherwise the content is appended as a 
			*	text node to its parent. Since this check has to be done only when there is no content, a 
			*	flag (ieNoContent) is set, so the check can be skipped when it is false.
			*/
        	var tempSpans = rootNode.getElementsByTagName('SPAN');
        	var ele = rootNode.getElementsByTagName('DIV');
     		for(var x=0;x < tempSpans.length;x++){
     			if(tempSpans[x].getAttribute(this._includeAttribute) && tempSpans[x].lastChild.nodeName=="#text"){     				
     				/* When the asset with empty space(s) gets saved without any text and re-edited the dummy tempNode has already been removed.
     				*  Content will still be lost due to the extra space. Instead of the dummy node however, a textnode is created.
     				*  Prepend the contents of the text to the next element.
     				*/
     				ele[x].lastChild.data = tempSpans[x].lastChild.data+ele[x].lastChild.data;     				
     			}
     			if(tempSpans[x].getAttribute('ID') == '#tempNode') {
     				if(tempSpans[x].innerHTML!="&nbsp;"){
						var tempText = tempSpans[x].ownerDocument.createTextNode(tempSpans[x].innerText);
     					tempSpans[x].parentNode.appendChild(tempText);
     				}
					tempSpans[x].parentNode.removeChild(tempSpans[x]);
     				break;
     			}
     			this.ieNoContent = false;
     		}
     	} 
        for(var p=0 ; p < tags.length; p++){
			var tag = tags[p];
			var includes = rootNode.getElementsByTagName(tag);
			//Assume root node has the include node
			var _nodeSearch = true;
			var _foundNode = false;
			var idAttr;
			while(_nodeSearch && includes.length > 0){
				for(var i=0;i < includes.length;i++){
					if(includes[i].getAttribute(this._includeAttribute) && includes[i].getAttribute(this._includeAttribute).indexOf('_CSEMBEDTYPE_=inclusion') != -1){
						_foundNode=true;
						idAttr = includes[i].getAttribute(this._includeAttribute);
						var assetId='';
						var assetType='';
						var idAttrArray = idAttr.split('&');
						for(var j=0;j < idAttrArray.length;j++){
							if(idAttrArray[j].indexOf('_c_') != -1){
								assetType=idAttrArray[j].substring(4,idAttrArray[j].length)  
							}
							if(idAttrArray[j].indexOf('_cid_') != -1){
								assetId=idAttrArray[j].substring(6,idAttrArray[j].length)  
							}
						}
						//Get the owner document from the include element
						var doc = includes[i].ownerDocument;
						var temp = doc.createElement("b");
						var tempText = doc.createTextNode("#temp");
						temp.appendChild(tempText);
						var parentEle = includes[i].parentNode;
						parentEle.insertBefore(temp,includes[i]);
						parentEle.removeChild(includes[i]);
						var spanEle = doc.createElement("span");
						var innerEle = doc.createElement("i"); 
						var innerText = doc.createTextNode("[" + 'Asset Included' + "(" + 'Id' + ":" + assetId + ";" + 'Type' + ":" + assetType + ")]");
						//Add the inner element to the include element
						spanEle.appendChild(innerEle);
						spanEle.setAttribute('id',idAttr);
						//Add the text node to the inner element
						innerEle.appendChild(innerText);
						//Now replace the temp with the span
						parentEle.replaceChild(spanEle,temp);
						
					}
				if(_foundNode){
					//Recalculate the new tags after replacing the child
					includes = rootNode.getElementsByTagName(tag);
					_foundNode = false;
					_nodeSearch = true;
					break;
				} else {
					_nodeSearch = false;
				}
				}
			}
		}
		
		var links = rootNode.getElementsByTagName(this._linkElement);
		_nodeSearch = true;
		_foundNode = false;
		var hrefAttr;
		while(_nodeSearch && links.length > 0){
			for(var i=0;i < links.length;i++){
				if(links[i].getAttribute(this._linkAttribute) && ((links[i].getAttribute(this._linkAttribute).indexOf('_CSEMBEDTYPE_=internal') != -1) || (links[i].getAttribute(this._linkAttribute).indexOf('_CSEMBEDTYPE_=external') != -1) )){
					_foundNode=true;
					//Lets remove the href and _fcksavedurl(fck created)
					var n = links[i].removeAttribute("href");
					var n = links[i].removeAttribute("_fcksavedurl");
					//Get the internal href value from the _fwhref attribute
					hrefAttr = links[i].getAttribute(this._linkAttribute);
					//Remove _fwhref attribute since we dont need it now.
					links[i].removeAttribute(this._linkAttribute);
					//Remove our custom style.(This style shows the border around the link.We dont want this to be stored in the database.)
					links[i].removeAttribute("style");
					//Now create the new href attribute and set the _fwhref attribute value.
					links[i].setAttribute("href",hrefAttr)
				}
			if(_foundNode){
				//Recalculate the new links after modifying the link
				links = rootNode.getElementsByTagName(this._linkElement);
				_foundNode = false;
				_nodeSearch = true;
				break;
			} else {
				_nodeSearch = false;
			}
			}
		}
		var data = rootNode.innerHTML;
		//Fix for PR#23147 
		var rx = /(<span id="_CSEMBEDTYPE[^>]+><i>[^<]+<\/i><\/span>\s*<\/p>)\s*<p>(?:&nbsp;)?<\/p>/ig;
		data = data.replace(rx, '$1');		
		//PR#18941.Single line break may get inserted if there is no data in the fckeditor so if there is a single line break ignore it.This will only ignore the single line break.
		var rexp = /^<(br)(?=[ >])[^>]*>(?:\s*|&nbsp;)(<\/\1>)?$/;
		if(rexp.test(data)){
			return '' ;
		}
		return data ;        
	},
	
		toDataFormat : function( html, fixForBody )
		{
			var _html = this.FWConvertToDataFormat(this.editor, html)  ;	
			var writer = this.writer,
				fragment = CKEDITOR.htmlParser.fragment.fromHtml( _html, fixForBody );
			writer.reset();

			fragment.writeHtml( writer, this.htmlFilter );

			var data = writer.getHtml( true );

			// Restore those non-HTML protected source. (#4475,#4880)
			data = unprotectRealComments( data );
			data = unprotectSource( data, this.editor );
			// Need To Do Encode All FW Asset Include and Link in this Editor		
			return data;
		}
	};
})();



// Timeout callback for Request to Rendere
function killSynchronousRequest (req)
{
    req.abort();
}

/*************************************************************************************
                                  CKUtilities
 Common utilities method for CKEditor
*************************************************************************************/

var CKUtilities =
{
    _includeElement:'DIV',
	_includeElementForLink:'SPAN',
	_includeAttribute:'_fwid',
	_linkElement:'A',
	_linkAttribute:'_fwhref',
	
	MoveToFWCustomNode:function(editor,type)
	{								
		var nodeTagNames;
		var nodeAttributeName;
		var ancestorNode;
		if (type == 'include') {						
			nodeTagNames = new Array(this._includeElement,this._includeElementForLink);
			nodeAttributeName = this._includeAttribute;
		} else if (type == 'link') {
			nodeTagNames = new Array(this._linkElement);
			nodeAttributeName = this._linkAttribute;
		}
		if(!editor.document.getSelection())
			return null;
		for(var i=0; i < nodeTagNames.length ; i ++ ){
			var nodeTagName = nodeTagNames[i];						
			if(editor.document.getSelection().getType() === CKEDITOR.SELECTION_ELEMENT && editor.document.getSelection().getSelectedElement()) {
				ancestorNode = editor.document.getSelection().getSelectedElement();
			} else	{
				ancestorNode = editor.document.getSelection().getStartElement();						
			}					
			//PR#19599 FCKSelection.GetSelectedElement() doesn't work for IE(when selection is not control type like for example image) and if the user right clicks twice on the included asset in IE we need to make sure that the included asset custom element still gets selected and shows the context menu.Here is the workaround.
			var regexp = /<(?:span|div) [^>]*_fwid=(["'])(.+?)\1.*?>/gi;
			
			while ( ancestorNode!=null ) {														
				if ((ancestorNode.getName().toUpperCase()== nodeTagName ) && ancestorNode.getAttribute(nodeAttributeName)&& ancestorNode.getAttribute(nodeAttributeName).indexOf('_CSEMBEDTYPE_') != -1){								
					return ancestorNode ;
				}
				ancestorNode = ancestorNode.getParent();							
			}						
		}
		return null ;									
	},
	getCustomElement:function(editor){
		if ( editor.mode != "wysiwyg" ) {
			return false;
		}
		if ( editor.window )
		{					
			var selectedNode = CKUtilities.MoveToFWCustomNode(editor,'link');
			if(!selectedNode) {
				selectedNode = CKUtilities.MoveToFWCustomNode(editor,'include');
			}
			if(selectedNode){									
				return selectedNode;
			} 
		}				
		return null;
	},	
	// Get the Selection
	getCKEditorSelection : function ( editor )
	{
	   var mySelection = editor.getSelection();
	   //Reset any previous selection
	   mySelection.reset();
	   var selectedText="" ;
		if(mySelection.getType() == CKEDITOR.SELECTION_TEXT){
			selectedText = mySelection.getSelectedText();
		} else if(mySelection.getType() == CKEDITOR.SELECTION_ELEMENT){
			//Check if image was selected from CKEditor. 
		    var _selElement = mySelection.getSelectedElement();
			if(_selElement.getName().toLowerCase() === 'img')
				selectedText = "_#CSEMBED_IMAGE#_";
		}
	   return selectedText ;
	},
	
	//
	insertHTML : function ( oCKEditor , html , modeType )
	{			
		var linkNode,isWebkit,nodeToRemove,linkParent,doc,
			cs_environment =  oCKEditor.config.cs_environment,
			selectedNode = CKUtilities.MoveToFWCustomNode(oCKEditor,'include'),
			isChanged = function(){
				return (linkParent.getChildCount() > 1 && isWebkit);
			};
		
		if(modeType == 'edit'){						
			// Get the Asset Include Node
			linkNode = CKUtilities.MoveToFWCustomNode(oCKEditor,'link');							
		}
		isWebkit = CKEDITOR.env.webkit;		
		nodeToRemove = linkNode || selectedNode;
		if(nodeToRemove){
			linkParent = nodeToRemove.getParent();
			//Fix for Safari when the linked asset is the only child element
			if (isWebkit && linkParent.getLast().nodeName == 'BR' && linkParent.getLast().getAttribute('type') == '_moz') {					
				linkParent.getLast().remove();
			}							
			if (isChanged) {
				for (var i = linkParent.getChildCount(); i--;) {				
					if (linkParent.getChild(i).type == 3 && linkParent.getChild(i).getText().replace(/^\s+|\s+$/g, '') == '') {
						linkParent.getChild(i).remove();							
					}
				}
			}																					
			if (isWebkit) {				
				var tempNode = oCKEditor.document.createText('#_CSEMBED_#');					
				tempNode.insertAfter(nodeToRemove);
				oCKEditor.getSelection().selectElement( tempNode );					
			} 
			// Just Remove Asset Include And Replace / Edit WITH Selected Asset
			nodeToRemove.remove() ;
		  
			// Change and Insert the Replacement Asset
			
			oCKEditor.insertHtml(html);
			if (isChanged) {
				for (var i = 0; i < linkParent.getChildCount(); i++) {					
					if (linkParent.getChild(i).type == 3 && linkParent.getChild(i).getText() == '#_CSEMBED_#') {
						linkParent.getChild(i).remove();
					}
				}
			}							
		} else {	
			if(!oCKEditor.getData() && CKEDITOR.env.ie)	{
				// Bug 14602171: Create a dummy span node after html content while ckeditor has no content.
				// Read above for details about this issue and the fix.
				html = html+"<span id='#tempNode'>&nbsp;</span>";
			}
			oCKEditor.insertHtml(html);
		}
	},
	GetXHTML : function( node, includeNode, format )
	{
		if ( includeNode )
			return node.getOuterHtml()    ;
		else
			return node.getHtml() ;
	},
	setDirtyTab : function(CKeditor){		
		var cs_environment =  (CKeditor) ? CKeditor.config.cs_environment : CKEDITOR.config.cs_environment,
			isMultiValued = (CKeditor && CKeditor.config.isMultiValued) ? true : false;
		// Fix for PR# 14525539. 
		// The document tab has to be set to dirty after adding content.					
		if(cs_environment=='ucform'||cs_environment=='insite'){
			var doc = (cs_environment=='ucform'||isMultiValued) ? SitesApp.getActiveDocument(): parent.SitesApp.getActiveDocument();
			if(CKeditor.mode==="source" || doc && CKeditor.checkDirty() && !doc.dirty){
				doc.set('dirty', true);	
			}
		}
	}
} ;

//This requires to be overridden so that the included images can be linked
CKEDITOR.plugins.link =
{
	/**
	 *  Get the surrounding link element of current selection.
	 * @param editor
	 * @example CKEDITOR.plugins.link.getSelectedLink( editor );
	 * @since 3.2.1
	 * The following selection will all return the link element.
	 *	 <pre>
	 *  <a href="#">li^nk</a>
	 *  <a href="#">[link]</a>
	 *  text[<a href="#">link]</a>
	 *  <a href="#">li[nk</a>]
	 *  [<b><a href="#">li]nk</a></b>]
	 *  [<a href="#"><b>li]nk</b></a>
	 * </pre>
	 */
	getSelectedLink : function( editor )
	{
		try
		{
			var selection = editor.getSelection();
			if ( selection.getType() == CKEDITOR.SELECTION_ELEMENT )
			{
				var selectedElement = selection.getSelectedElement();
				if ( selectedElement.is( 'a' ) )
					return selectedElement;
			}

			var range = selection.getRanges( true )[ 0 ];
			range.shrink( CKEDITOR.SHRINK_TEXT );
			var root = range.getCommonAncestor();
			//Root has been calculated now we can select the custom element if the link is created around it
			var selectedNode = CKUtilities.MoveToFWCustomNode(editor,'include');
			if(selectedNode){																	
				editor.getSelection().selectElement(selectedNode );
			}
			return root.getAscendant( 'a', true );
		}
		catch( e ) { return null; }
	},

	// Opera and WebKit don't make it possible to select empty anchors. Fake
	// elements must be used for them.
	fakeAnchor : CKEDITOR.env.opera || CKEDITOR.env.webkit,

	// For browsers that don't support CSS3 a[name]:empty(), note IE9 is included because of #7783.
	synAnchorSelector : CKEDITOR.env.ie,

	// For browsers that have editing issue with empty anchor.
	emptyAnchorFix : CKEDITOR.env.ie && CKEDITOR.env.version < 8,

	tryRestoreFakeAnchor : function( editor, element )
	{
		if ( element && element.data( 'cke-real-element-type' ) && element.data( 'cke-real-element-type' ) == 'anchor' )
		{
			var link  = editor.restoreRealElement( element );
			if ( link.data( 'cke-saved-name' ) )
				return link;
		}
	}
};