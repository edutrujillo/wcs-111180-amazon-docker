/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'ko',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'포함된 자산을 제거하겠습니까?',
	 RemoveLinkedAssetQuestion:'링크된 자산을 제거하겠습니까?'
	}
});
