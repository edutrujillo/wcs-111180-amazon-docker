/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'it',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'Rimuovere l\'asset incluso?',
	 RemoveLinkedAssetQuestion:'Rimuovere l\'asset collegato?'
	}
});
