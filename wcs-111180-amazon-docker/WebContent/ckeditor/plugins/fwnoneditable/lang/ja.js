/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'ja',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'含まれたアセットを除去しますか。',
	 RemoveLinkedAssetQuestion:'リンク・アセットを除去しますか。'
	}
});
