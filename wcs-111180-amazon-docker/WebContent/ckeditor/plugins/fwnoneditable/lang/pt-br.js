/*
 * fwnoneditable English language file.
 */
CKEDITOR.plugins.setLang("fwnoneditable",'pt-br',{
  fwnoneditable:
    {
	 RemoveIncludedAssetQuestion:'Deseja remover o ativo incluído?',
	 RemoveLinkedAssetQuestion:'Deseja remover o ativo vinculado?'
	}
});
