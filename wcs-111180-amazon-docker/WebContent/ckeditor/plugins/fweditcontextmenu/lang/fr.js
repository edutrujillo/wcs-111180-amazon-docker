/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'fr',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  'Modifier la ressource incluse',
         EditProperties :       'Modifier les propriétés',
         RemoveIncludedAsset :  'Retirer la ressource incluse',
         OpenLink :             'Ouvrir le lien',
         ChangeLinkedAsset :    'Modifier la ressource liée',
         RemoveLinkedAsset :    'Retirer la ressource liée',
         PleaseSelectAssetFromSearchResults : 'Sélectionner une ressource à partir des résultats de recherche',
         CannotLinkOrIncludeThisAsset :       'La liaison ou l\'inclusion de cette ressource n\'est pas autorisée.\nSélectionnez une ressource des types suivants :\n',

         CutIncludedAsset :            'Couper la ressource incluse',
         CopyIncludedAsset :           'Copier la ressource incluse',
         CutLinkedAsset :              'Couper la ressource liée',
         CopyLinkedAsset :             'Copier la ressource liée',
         PasteIncludedAsset :          'Coller la ressource incluse',
         PasteLinkedAsset :            'Coller la ressource liée' 		

	}
});




