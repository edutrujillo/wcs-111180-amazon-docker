/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'pt-br',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  'Alterar Ativo Incluído',
         EditProperties :       'Editar Propriedades',
         RemoveIncludedAsset :  'Remover Ativo Incluído',
         OpenLink :             'Abrir Link',
         ChangeLinkedAsset :    'Alterar Ativo Vinculado',
         RemoveLinkedAsset :    'Remover Ativo Vinculado',
         PleaseSelectAssetFromSearchResults : 'Selecione um ativo nos resultados da pesquisa',
         CannotLinkOrIncludeThisAsset :       'Não é permitido link ou inclusão neste ativo.\nSelecione um ativo dos seguintes tipos:\n',

         CutIncludedAsset :            'Recortar Ativo Incluído',
         CopyIncludedAsset :           'Copiar Ativo Incluído',
         CutLinkedAsset :              'Recortar Ativo Vinculado',
         CopyLinkedAsset :             'Copiar Ativo Vinculado',
         PasteIncludedAsset :          'Colar Ativo Incluído',
         PasteLinkedAsset :            'Colar Ativo Vinculado' 		

	}
});




