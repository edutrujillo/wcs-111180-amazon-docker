/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'zh',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  '變更包括的資產',
         EditProperties :       '編輯特性',
         RemoveIncludedAsset :  '移除包括的資產',
         OpenLink :             '開啟連結',
         ChangeLinkedAsset :    '變更連結的資產',
         RemoveLinkedAsset :    '移除連結的資產',
         PleaseSelectAssetFromSearchResults : '從搜尋結果中選取資產',
         CannotLinkOrIncludeThisAsset :       '不允許連結或包括此資產.\n請選取下列類型的資產:\n',

         CutIncludedAsset :            '剪下包括的資產',
         CopyIncludedAsset :           '複製包括的資產',
         CutLinkedAsset :              '剪下連結的資產',
         CopyLinkedAsset :             '複製連結的資產',
         PasteIncludedAsset :          '貼上包括的資產',
         PasteLinkedAsset :            '貼上連結的資產' 		

	}
});




