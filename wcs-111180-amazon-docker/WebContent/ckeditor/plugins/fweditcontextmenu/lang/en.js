﻿/*
 * Context Menu plugin English language file.
 */

 


CKEDITOR.plugins.setLang( 'fweditcontextmenu', 'en',
{
	fweditcontextmenu :
	{
	
	     ChangeIncludedAsset :  'Change Included Asset',
         EditProperties :       'Edit Properties',
         RemoveIncludedAsset :  'Remove Included Asset',
         OpenLink :             'Open Link',
         ChangeLinkedAsset :    'Change Linked Asset',
         RemoveLinkedAsset :    'Remove Linked Asset',
         PleaseSelectAssetFromSearchResults : 'Select an asset from search results',
         CannotLinkOrIncludeThisAsset :       'This asset is not allowed to link or include.\nPlease select asset of the following types:\n',

         CutIncludedAsset :            'Cut Included Asset',
         CopyIncludedAsset :           'Copy Included Asset',
         CutLinkedAsset :              'Cut Linked Asset',
         CopyLinkedAsset :             'Copy Linked Asset',
         PasteIncludedAsset :          'Paste Included Asset',
         PasteLinkedAsset :            'Paste Linked Asset' 		

	}
});




