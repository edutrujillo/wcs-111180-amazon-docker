﻿/***************************************************************************************
                             fweditcontextmenu
  Created: March 25, 2010
  By:      JAG
  Reason:  The Right Click Context Menu that allows the user
           to perform the cut, copy , paste remove on
           Included and Linked Assets


  Modified:  It's possible to customize every single aspect of the editor,
  from the toolbar, to the skin, to dialogs,
  and here we customize the context menu to detect all
  FW  included and linked Assets in an instance of running ckeditor. 

  The context menu is an important and powerful tool in the editor.
  Added custom features to it by extending cut include copy change
  to include linked Assets

  The editor structure is totally plugin based,
  including many of its core features.
  Plugins live in separate files, making them easy to organize and maintain.
  CKEditor  JavaScript API makes developing plugins flexible.

  CKEditor automatically detects the user language,
  automatically configuring its interface to be localized to it.
  This editor speaks the language your users speak.

  Modified         Reason
 ----------        ------

  May  24, 2010     PR#22861    CKEditor: Change link asset ends up including an asset

  May  26, 2010     PR#22862  - CKEditor: It is not currently possible to change an included asset.
                                Also this should work via the Pick From Asset Tree / Table layer.
                                It is also not possible to remove an included asset.

**********************************************************************************/



//Define Global element for cut,copy and paste
var _fwClipBoardElement=null;
CKEDITOR.plugins.add( 'fweditcontextmenu',
{
    requires: ['fwnoneditable'],

    lang : ['en','fr','es','de','it','ja','ko','zh-cn','zh','pt-br'],
	/*
	 * Initialize and register Asset Include and/or Linked  Context Menu
	 */
	init:function(editor)
	{
        // Define and create Asset Context Menu Commamd Objects
        editor.addCommand( 'EditInclude', new CKEDITOR.ChangeIncludeCommand() );
        editor.addCommand( 'EditIncludeProperties', new CKEDITOR.EditIncludePropertiesCommand() );
        editor.addCommand( 'RemoveInclude', new CKEDITOR.RemoveIncludeCommand() );

		editor.addCommand( 'CutCustomElement', new CKEDITOR.CutCustomElementCommand() );
	    editor.addCommand( 'CopyCustomElement', new CKEDITOR.CopyCustomElementCommand() );
	    editor.addCommand( 'PasteCustomElement', new CKEDITOR.PasteCustomElementCommand() );

        // Custom Asset Link Menu Commands
        editor.addCommand( 'CutCustomElementLink', new CKEDITOR.CutCustomElementCommand() );
        editor.addCommand( 'CopyCustomElementLink', new CKEDITOR.CopyCustomElementCommand() );


        // Separator
        editor.addCommand( 'EditLink', new CKEDITOR.ChangeLinkCommand() );
        editor.addCommand( 'EditLinkProperties', new CKEDITOR.EditLinkPropertiesCommand() );
        editor.addCommand( 'RemoveAssetLink', new CKEDITOR.RemoveAssetLinkCommand() );

		// If the "menu" plugin is loaded, register the menu 'FWEDITCONTEXTMENU Plugin ' items.
		if ( editor.addMenuItems )
		{			
			editor.addMenuItems(
				{
                    //  Cut the Selected FW Custom [Included] Asset
					CutCustomElement :
					{
						label :     editor.lang.fweditcontextmenu.CutIncludedAsset,
                        icon:       this.path+"images/cut.gif",
						command :   'CutCustomElement',
						group :     'clipboard',
						order :     6
					},					
					//  Make a copy of Included Asset 
					CopyCustomElement :
					{
						label : editor.lang.fweditcontextmenu.CopyIncludedAsset,
						icon:   this.path+"images/copy.gif",
						command : 'CopyCustomElement',						
						group : 'clipboard',
	                    order : 7
					},	
					//  Edit / Change  Included Asset 
					EditInclude :
					{
						label : editor.lang.fweditcontextmenu.ChangeIncludedAsset,
						icon:   this.path+"images/EditAsset.gif",
						command : 'EditInclude',
						group : 'link',
						order : 8
					},
					//  Edit Properties Included Asset ? 
					EditIncludeProperties :
					{
						label : editor.lang.fweditcontextmenu.EditProperties,
						icon:   this.path+"images/editincludeproperties.gif",
						command : 'EditIncludeProperties',
						group : 'link',
						order : 9
					},
					//  Paste the Cut / Copy  Included Asset
					PasteCustomElement :
					{
						label :    editor.lang.fweditcontextmenu.PasteIncludedAsset,
						icon:      this.path+"images/paste.gif",
						command : 'PasteCustomElement',						
						group :   'link',
	                    order :   10						
					},
				    // Add the remove included Asset to the context menu 	
					RemoveInclude :
					{
					    label :   editor.lang.fweditcontextmenu.RemoveIncludedAsset,
						icon:     this.path+"images/removeAsset.gif",
						command : 'RemoveInclude',
						group :   'link',
						order :   10
					},


                //   Cut Linked Asset
                CutCustomElementLink :
                    {
                        label :   editor.lang.fweditcontextmenu.CutLinkedAsset,
                        icon:     this.path+"images/cut.gif",
                        command : 'CutCustomElementLink',
                        group :   'clipboard',
                        order :   6
                    },
                //  Copy Linked Asset
				CopyCustomElementLink :
					{
						label :   editor.lang.fweditcontextmenu.CopyLinkedAsset,
						icon:     this.path+"images/copy.gif",
						command : 'CopyCustomElementLink',
						group :   'clipboard',
	                    order :   7
					},

                 	//  Edit / Change  Linked Asset
					EditLink :
					{
						label :   editor.lang.fweditcontextmenu.ChangeLinkedAsset,
						icon:     this.path+"images/EditAsset.gif",
						command : 'EditLink',
						group :   'link',
						order :   8
					},
					//  Edit Properties Linked Asset ?
					EditLinkProperties :
					{
						label :   editor.lang.fweditcontextmenu.EditProperties,
						icon:     this.path+"images/editincludeproperties.gif",
						command : 'EditLinkProperties',
						group :   'link',
						order :    9
					},
                    //  Edit Properties Linked Asset ?
					RemoveAssetLink :
					{
						label :   editor.lang.fweditcontextmenu.RemoveLinkedAsset,
						icon:     this.path+"images/editincludeproperties.gif",
						command : 'RemoveAssetLink',
						group :   'link',
						order :   10
					},
                    //  Paste the Cut / Copy  Linked Asset
					PasteCustomLinkedElement :
					{
						label :    editor.lang.fweditcontextmenu.PasteLinkedAsset,
						icon:      this.path+"images/paste.gif",
						command : 'PasteCustomElement',
						group :   'link',
	                    order :   11
					}

                });
		}

		// If the "contextmenu" plugin is loaded, register the listeners. for Customized Menu 
		// Include and Asset Links..
        // Added if Pasting Included or Linked Asset
        if ( editor.contextMenu )
		{
            // Get the context Menu Handle
            var menu = editor.contextMenu  ;
			
		    // Register Asset Context Menu Listener before its displayed
			editor.contextMenu.addListener( function( element, selection )
			      {
				editor.contextMenu._isDisplayed=true;
				var length = menu.items.length, selectedNode, 
					anchorTag = '<A';
					if ( selection )
					{
						// Get the reference to the custom element node
					    selectedNode = CKUtilities.getCustomElement(editor);
						if(!selectedNode)
						{
                             // Any Included or Linked Asset on the Clipboard
                             // That we can paste into selected editor Area...
                             if(_fwClipBoardElement!=null)
							 {								
                                // If Clipboard HTML is a Span Tag then Paste the  Included Asset
                                // AND/OR  ITS a RLinked Reference
                                // The <a> tag defines an anchor. An anchor can be used in two ways:
                                // To create a link to another document, by using the href attribute                                 
                                if(_fwClipBoardElement.toUpperCase().indexOf(anchorTag)==0)
                                {									
							    	return { PasteCustomLinkedElement : CKEDITOR.TRISTATE_ON } ;
                                }
                                else
                                {
                                	return { PasteCustomElement  : CKEDITOR.TRISTATE_ON } ;							      
                                }
                             }
		                     return ;
	                     }

                         var enableEmbeddedLinks =  editor.config.enableEmbeddedLinks ;    // Yes Enabled

                         // Provide the limited functionality when the embedded links are disabled
                         // Attrinute CheckBox Allow
                         if(enableEmbeddedLinks=='0')
	                     {
	                    	if (selectedNode.getName().toUpperCase() == CKUtilities._includeElement || selectedNode.getName().toUpperCase() == CKUtilities._includeElementForLink)
		                    {
                                   // Remove All Menu Items and Enable the normal cut, copy remove
                                   // opeartions
	                    		 	menu.removeAll();
				                   // User selected a Included Asset  Enable Asset Include Menu Itrems
							       return {
								            CutCustomElement       : CKEDITOR.TRISTATE_ON,
											CopyCustomElement      : CKEDITOR.TRISTATE_ON,
								            RemoveInclude          : CKEDITOR.TRISTATE_ON
                                          };
                            } else  if ( selectedNode.getName().toUpperCase() == CKUtilities._linkElement)
	                             	{
                                         menu.removeAll();
                                         return {
								                   CutCustomElementLink       : CKEDITOR.TRISTATE_ON,
                                                   CopyCustomElementLink      : CKEDITOR.TRISTATE_ON,
                                                   RemoveAssetLink            : CKEDITOR.TRISTATE_ON
                                                };
                                    }
	                	    return;
	                     }

                         // Has the User clicked and selected a Custom Asset
                         // Included and Added 
                         if(selectedNode)
						 {		
						        //  Included Asset
		                	    if (selectedNode.getName().toUpperCase() == CKUtilities._includeElement || selectedNode.getName().toUpperCase() == CKUtilities._includeElementForLink)
		                        {									
                                   // Remove All Menu Items
                                   menu.removeAll();
                                   // User selected a Included Asset  Enable Asset Include Menu Itrems
                                   if(editor.config.cs_environment!='standard'||editor.config.showSiteTree!='true') {
                                	   return {
                                		   CutCustomElement       : CKEDITOR.TRISTATE_ON,
                                		   CopyCustomElement      : CKEDITOR.TRISTATE_ON,
                                		   EditIncludeProperties  : CKEDITOR.TRISTATE_ON,
                                		   RemoveInclude          : CKEDITOR.TRISTATE_ON
                                       };
                                   }else{
                                   		return {
                                   			CutCustomElement      : CKEDITOR.TRISTATE_ON,
                                   			CopyCustomElement     : CKEDITOR.TRISTATE_ON,
                                   			EditInclude           : CKEDITOR.TRISTATE_ON,
                                   			EditIncludeProperties : CKEDITOR.TRISTATE_ON,
											RemoveInclude         : CKEDITOR.TRISTATE_ON
                                   		};
                                   	}
							    }
								else
								{
									if ( selectedNode.getName().toUpperCase() == CKUtilities._linkElement)
                                    {
                                        menu.removeAll();

                                        // Turn on Custom Asset Link Menu Operations
                                        //
                                        if(editor.config.cs_environment!='standard'||editor.config.showSiteTree!='true') {
                                        	return {
												CutCustomElementLink       : CKEDITOR.TRISTATE_ON,
												CopyCustomElementLink      : CKEDITOR.TRISTATE_ON,
												EditLinkProperties         : CKEDITOR.TRISTATE_ON,
												RemoveAssetLink            : CKEDITOR.TRISTATE_ON
                                            };
                                        }else{
                                        	return {
                                        		CutCustomElementLink       : CKEDITOR.TRISTATE_ON,
												CopyCustomElementLink      : CKEDITOR.TRISTATE_ON,
												EditLink                   : CKEDITOR.TRISTATE_ON,
												EditLinkProperties         : CKEDITOR.TRISTATE_ON,
												RemoveAssetLink            : CKEDITOR.TRISTATE_ON
                                        	};
                                        }
                                    }
                                }
						}
						return {  RemoveInclude    : CKEDITOR.TRISTATE_DISABLED, 
						          CutCustomElement : CKEDITOR.TRISTATE_DISABLED} ; 
					}					
					return ;					
				});
		}
	},		
    // Post After Context Menu Initilized and Ready for Action 
	afterInit : function( editor )
	{
		// Register a filter to displaying placeholders after mode change.
        // console.log("<Finished> fweditcontextmenu.plugin.init()" ) ;
	}

} );




/***********************************************************************************
*                Asset Customized Context Menu
               Execute the  CUT Asset  CallBack

     Reason:  Contextmenu  CutCustomElement  Asset  Callback
 *    The Command that gets executed when user wants to perform
      a Cut operation on a Included Asset 
 ************************************************************************************/
CKEDITOR.CutCustomElementCommand = function(){};

CKEDITOR.CutCustomElementCommand.prototype =
{
	// The Cut Asset Opeartion Callback ContextMenu Callback Operation
	exec : function( editor )
	{
		// Get the editors contextmenu handle..
	    var contextMenu = editor.contextMenu  ;
	    // Get the editor's selection reference handle 
		var selection = editor.getSelection() ; 
					
		//Get the reference to the Element node
	    var customElement = CKUtilities.getCustomElement(editor);
		if(!customElement)
		   return ;
        
        //Get the parent
	    var parent = customElement.getParent();

        // Clone the Node
        var clonedCustomElement = customElement.clone(true);

        // Is a DOM Node
        _fwClipBoardElement = CKUtilities.GetXHTML( clonedCustomElement, true, false ) ;

        //Remove the child from the parent node.
        customElement.remove() ;
	
	    //Clear the previous data from the window Top Level clipboard
	    if(typeof(window.clipboardData) != 'undefined')
			window.clipboardData.clearData();
	}
};


/**************************************************************************************
*                      Asset  Copy CallBack 
     Contextmenu  CopyCustomElement   Asset  Callback
 *   The Command that gets executed when user wants to perform 
     a  Copy Asset operation on a Included Asset
 **************************************************************************************/ 
CKEDITOR.CopyCustomElementCommand = function(){};

CKEDITOR.CopyCustomElementCommand.prototype =
{
	// Hopefully this is the Remove  ContextMenu Callback Operation 
	exec : function( editor )
	{
		// Get the editors contextmenu handle..
	    var contextMenu = editor.contextMenu  ; 
		
	    // Get the editor's selection reference handle 
		var selection = editor.getSelection() ; 
				
		//Get the reference to the Element node
	    var customElement = CKUtilities.getCustomElement(editor);
		if(!customElement)
		  return ;

	    //Get the parent, not really needed
	    var parent = customElement.getParent();

        // Need to clone the Include Custom Asset
        var clonedCustomElement = customElement.clone(true);

        // Generate the XHTML
        _fwClipBoardElement = CKUtilities.GetXHTML( clonedCustomElement, true, false ) ;

	    //Clear the previous data from the window clipboard
	    if(typeof(window.clipboardData) != 'undefined')
			window.clipboardData.clearData();	

	}
};



/* *************************************************************************************
*                         CTX   Asset   Paste CallBack
 Contextmenu  CopyCustomElement   Asset  Callback 
 *   The Command that gets executed when user wants to perform 
      a paste asset from clipboard to editor textarea 
 **************************************************************************************/ 
CKEDITOR.PasteCustomElementCommand = function(){};

CKEDITOR.PasteCustomElementCommand.prototype =
{
	// Hopefully this is the Remove  ContextMenu Callback Operation 
	exec : function( editor )
	{
		 // Get the editors contextmenu handle.. 
	    var contextMenu = editor.contextMenu  ; 
		
	    // Get the editor's selection reference handle 
		var selection = editor.getSelection() ; 		
	
	    //Get the reference to the Element node
	    //editor.insertHtml('&nbsp; <SPAN _fwid="_CSEMBEDTYPE_=inclusion&amp;_PAGENAME_=FirstSiteII%2FFSIIDetail&amp;_cid_=1114633414970&amp;_c_=Media_C"><IMG class=ImageDetail alt="Content Server Image" src="/cs/BlobServer?blobkey=id&amp;blobwhere=1114900713853&amp;blobcol=urldata&amp;blobtable=MungoBlobs" width=158 height=41>&nbsp;&nbsp;</SPAN>  &nbsp;');	    
	    if(_fwClipBoardElement != null)
		{           
		   // We can not insert  this HTML becouse DATA renderer will be called breaking rendering
		   editor.insertHtml(_fwClipBoardElement);
		   
           //editor.config.doHtmlEncoding = true;
           _fwClipBoardElement = null ;
	    }
	}
};



/***************************************************************************
 *        Contextmenu  Edit / Change Included Asset  Callback
 *        The Command that gets executed when user wants to perform
          Edit Change Included  Included Asset Element
 ***********************************************************************/
CKEDITOR.ChangeIncludeCommand = function(){};

/************************************************************************************************
    Context Menu callback Execute Change ChangeIncludeCommand
                      the * INCLUDED ASSET *

    Modified: May  26, 2010
    Reason:   PR#22862  - CKEditor: It is not currently possible to change an included asset.
                          Also this should work via the Pick From Asset Tree / Table layer.
                          It is also not possible to remove an included asset.

************************************************************************************************/

CKEDITOR.ChangeIncludeCommand.prototype =
{
    // Change Included Asset
    exec :  function( ckeditor )
    {       
        // Is tree applet is Visible
        var showSiteTree = ckeditor.config.showSiteTree ;
		
		embedtype = "include" ;

        if(showSiteTree)
	    {			
		   var linkCommand = new CKEDITOR.ChangeLinkCommand();
		   linkCommand.editEmbeddedLinkT(ckeditor,embedtype);
	    }
    }
};




//
//  Advanced UI User wants to EDIT the Asset Properties
//  the included Asset with different select from tree asset
CKEDITOR.EditIncludePropertiesCommand = function(){};

CKEDITOR.EditIncludePropertiesCommand.prototype =
{
	// Hopefully this is the Remove  ContextMenu Callback Operation 
	exec : function( ckEditor )
	{	
		var cs_environment =  ckEditor.config.cs_environment ;
        if(cs_environment!='ucform'&&cs_environment!='insite')
        {
			// Get the editors contextmenu handle.. 
			var contextMenu = ckEditor.contextMenu  ; 
			
			// Get the editor's selection reference handle 
			var selection = ckEditor.getSelection() ; 
			var ckConfig = ckEditor.config;
			
			//Get config properties
			var allowedassettypes = ckConfig.allowedassettypes;
			var assetName =  ckConfig.assetName;
			var assetId = ckConfig.assetId;
			var assetType =  ckConfig.assetType;
			var fieldname =  ckConfig.fieldName;
			var fielddesc = ckConfig.fieldDesc; 
			var editingstyle = ckConfig.editingstyle; 

			// Get the Root CS Application Context
			// Set the Elements Base CS Root Context
			var csRootContext = ckConfig.csRootContext;

			var pURL = csRootContext+"ContentServer?pagename=OpenMarket/Xcelerate/Actions/";

			var hrefOrIdString;
			var customElement;

			customElement = CKUtilities.MoveToFWCustomNode(ckEditor,'include');
			
			// Does it contain a specific Node Attribute 
			// If yes it returns the value 
			hrefOrIdString = customElement.getAttribute('_fwid');

			pURL = pURL + "AddInclusion";
			pURL = pURL + "&hrefOrIdString=" + encodeURIComponent(hrefOrIdString);
			pURL = pURL + "&name=" + encodeURIComponent(assetName );
			pURL = pURL + "&AssetType=" + assetType;
			pURL = pURL + "&fieldname=" + encodeURIComponent(fieldname);
			pURL = pURL + "&fielddesc=" + encodeURIComponent(fielddesc);
			pURL = pURL + "&formFieldName=" + encodeURIComponent(fieldname);
			pURL = pURL + "&EditingStyle=" + encodeURIComponent(editingstyle);
			pURL = pURL + "&modeType=edit";    
     		pURL = pURL + "&IFCKEditor=true";
			
			return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=700,location=no,menubar=no,resizable=yes,toolbar=no,top=50,width=400,height=700");
        } else {
        	// TO DO: Extract Contributor UI logic for EditLinkProperties and EditIncludeProperties to a separate utility function
        	var selectedNode, c, cid,elements,pname,env,fieldname,additionalParams, CK,
        	legalArgs = [],
			Split = function(str,delim){
				return str.split(delim);
			},
			getParam = function(str,delim){
				var substr='';
				for(var i=0;i<str.length;i++){
					if(str[0]!='='||str[i]==delim){					
						break;
					}else{
						if(str[i]!='=')substr+=str[i];
						else continue;
					}					
				}		
				return substr;
			},
			parseElement = function(ele,delim){
				for(var i=0;i<ele.length;i++){
					var temp = getParam(ele[i],delim);
					if(temp!='') break;
				}
				return temp;
			},
			getLegalArgs = function(additionalParams){
				/*
					Strip away legalArguments from additionalParams and return a list of key-value pairs
					Example :
						Lets assume legal arguments linktext1 and linktext2 with values R&G and Y&T.
						additionalparams will be linktext=R%26G&linktext2=Y%26T
						getLegalArgs should return [{name:'linktext',value:'R&G'},{name:'linktext2',value:'Y&T'}]
				*/
				var obj = {},legalArgs = [], l = decodeURIComponent(additionalParams), arr = l.split('&');
				for(var i=0;i<arr.length;i++){
					var temp = arr[i].split('=');
					obj.name = decodeURIComponent(temp[0]);
					obj.value = decodeURIComponent(temp[1]);
					legalArgs.push(obj);
					obj = {};
				}
				return legalArgs;
			},
			ckconfig=ckEditor.config;
			if(CKEDITOR.env.ie){
				ckEditor.getSelection().lock();
			}
			selectedNode = CKUtilities.MoveToFWCustomNode(ckEditor,'include');
			ckEditor.getSelection().selectElement(selectedNode);
			elements=selectedNode.getAttribute("_fwid");
			cid=parseElement(Split(elements,"_cid_"),'&');
			c=parseElement(Split(elements,"_c_"),'&');
			pname=parseElement(Split(elements,"_PAGENAME_"),'&');
			//Strip off the postfix for fieldname depending on the editingstyle of the content attribute.
			fieldname = ((!ckconfig.fieldName)? ckEditor.name : 
						((ckconfig.editingstyle==="single")? ckconfig.fieldName : ckconfig.fieldName.substr(0,ckconfig.fieldName.lastIndexOf('_'))));
			env=(CKEDITOR.env.webkit)? "webkit" : ((CKEDITOR.env.gecko)? "gecko":"ie");
			assettypes= (!ckconfig.allowedassettypes)? "*": ckconfig.allowedassettypes;
			additionalParams = parseElement(Split(elements,"_ADDITIONALPARAMS_"),'&');
			if(additionalParams) legalArgs = getLegalArgs(additionalParams);
			// CKEditor must invoke the dialogmanager which is a singleton object.
			CK = ckconfig.isMultiValued ? new fw.ui.controller.CKEditorController : new parent.fw.ui.controller.CKEditorController;
			CK.openCKDialog({
				assettypes: assettypes,
				args: {
					action: 'edit',
					item: 'asset',
					fieldName: fieldname,
					DEPTYPE : ckconfig.DEPTYPE,
					env : env,
					isMultiVal : ckconfig.isMultiValued,
					deviceid: ckconfig.deviceid,
					asset: {
						type: c,
						id: cid
					},
					pname: pname,
					templateArgs: legalArgs	
				}
			});
		}
	}
};

CKEDITOR.EditLinkPropertiesCommand = function(){};

CKEDITOR.EditLinkPropertiesCommand.prototype = {
	// Hopefully this is the Remove  ContextMenu Callback Operation
	exec : function( ckEditor )
	{	
		var cs_environment =  ckEditor.config.cs_environment;
        if(cs_environment=='standard')
        {
	    // Get the editors contextmenu handle..
	    var contextMenu = ckEditor.contextMenu  ;

	    // Get the editor's selection reference handle
		var selection = ckEditor.getSelection() ;
		var ckConfig = ckEditor.config;
		
		//Get config properties
		var allowedassettypes = ckConfig.allowedassettypes;
		var assetName =  ckConfig.assetName;
	    var assetId = ckConfig.assetId;
		var assetType =  ckConfig.assetType;
		var fieldname =  ckConfig.fieldName;
		var fielddesc = ckConfig.fieldDesc;
		var editingstyle = ckConfig.editingstyle;

        // Get the Root CS Application Context
        // Set the Elements Base CS Root Context
        var csRootContext = ckConfig.csRootContext  ;

        var pURL = csRootContext+"ContentServer?pagename=OpenMarket/Xcelerate/Actions/";

        var hrefOrIdString;
	    var customElement;

		customElement = CKUtilities.MoveToFWCustomNode(ckEditor,'link');
	    hrefOrIdString = customElement.getAttribute('_fwhref');

        pURL = pURL + "EmbeddedLink";
		pURL = pURL + "&hrefOrIdString=" + encodeURIComponent(hrefOrIdString);
	    pURL = pURL + "&name=" + encodeURIComponent(assetName );
        pURL = pURL + "&AssetType=" + assetType;
	    pURL = pURL + "&fieldname=" + encodeURIComponent(fieldname);
        pURL = pURL + "&fielddesc=" + encodeURIComponent(fielddesc);
        pURL = pURL + "&formFieldName=" + encodeURIComponent(fieldname);
		pURL = pURL + "&EditingStyle=" + encodeURIComponent(editingstyle);
	    pURL = pURL + "&modeType=edit";
  		pURL = pURL + "&IFCKEditor=true";


        return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=700,location=no,menubar=no,resizable=yes,toolbar=no,top=50,width=400,height=700");
        }else{
			// TO DO: Extract all the contributor UI logic for EditLinkProperties and EditIncludeProperties to a new utility function
        	var selectedNode, c, cid,elements,pname,linkText,wrapper,fragment,env,fieldname,assettypes,additionalParams,CK,
        	legalArgs=[],
			Split = function(str,delim){
				return str.split(delim);
			},
			getParam = function(str,delim){
				var substr='';
				for(var i=0;i<str.length;i++){
					if(str[0]!='='||str[i]==delim){
						break;
					}else{
						if(str[i]!='=')substr+=str[i];
						else continue;
					}
				}
				return substr;
			},
			parseElement = function(ele,delim){
				for(var i=0;i<ele.length;i++){
					var temp = getParam(ele[i],delim);
					if(temp!='') break;
				}
				return temp;
			},
			getLegalArgs = function(additionalParams){
				/* 
					Strip away legalArguments from additionalParams and return a list of key-value pairs
					Example : 
						Lets assume legal arguments linktext1 and linktext2 with values R&G and Y&T.
						additionalparams will be linktext=R%26G&linktext2=Y%26T
						getLegalArgs should return [{name:'linktext',value:'R&G'},{name:'linktext2',value:'Y&T'}]
				*/
				var obj = {},legalArgs = [], l = decodeURIComponent(additionalParams), arr = l.split('&');
				for(var i=0;i<arr.length;i++){
					var temp = arr[i].split('=');
					obj.name = decodeURIComponent(temp[0]);
					obj.value = decodeURIComponent(temp[1]);
					legalArgs.push(obj);
					obj = {};
				}
				return legalArgs;
			},
			ckconfig=ckEditor.config;
			if(CKEDITOR.env.ie){
				ckEditor.getSelection().lock();
			}
			selectedNode = CKUtilities.MoveToFWCustomNode(ckEditor,'link');
			ckEditor.getSelection().selectElement(selectedNode);
			elements=selectedNode.getAttribute("_fwhref");
			cid=parseElement(Split(elements,"_cid_"),'&');
			c=parseElement(Split(elements,"_c_"),'&');
			pname=parseElement(Split(elements,"_PAGENAME_"),'&');
			wrapper=parseElement(Split(elements,"_WRAPPER_"),'&');
			fragment=parseElement(Split(elements,"_frag_"),'&');
			linkText = (selectedNode.getText().search(/\S/) ==-1) ? encodeURIComponent('<span id="' + selectedNode.getFirst().getAttribute('_fwid')+ '">_#CSEMBED_IMAGE#_</span>').replace( /'/g, '%27' ) : encodeURIComponent(selectedNode.getText()).replace( /'/g, '%27' );
			fieldname = ((!ckconfig.fieldName)? ckEditor.name : 
						((ckconfig.editingstyle==="single")? ckconfig.fieldName : ckconfig.fieldName.substr(0,ckconfig.fieldName.lastIndexOf('_'))));
			env=(CKEDITOR.env.webkit)? "webkit" : ((CKEDITOR.env.gecko)? "gecko":"ie");
			assettypes= (!ckconfig.allowedassettypes)? "*": ckconfig.allowedassettypes;
			additionalParams = parseElement(Split(elements,"_ADDITIONALPARAMS_"),'&');
			if(additionalParams) legalArgs = getLegalArgs(additionalParams);
			// CKEditor must invoke the dialogmanager which is a singleton object.
			CK = ckconfig.isMultiValued ? new fw.ui.controller.CKEditorController : new parent.fw.ui.controller.CKEditorController;
			CK.openCKDialog({
				assettypes: assettypes,
				args: {
					action: 'edit',
					item: 'link',
					fieldName: fieldname,
					DEPTYPE : ckconfig.DEPTYPE,
					env : env,
					isMultiVal : ckconfig.isMultiValued,
					deviceid: ckconfig.deviceid,
					asset: {
						type: c,
						id: cid
					},
					pname: pname,
					advanced: {
						linkAnchor: fragment,
						wrapper: wrapper
					},
					linkText: linkText,
					templateArgs: legalArgs
				}
			});
        }
	}
};




/*   Contextmenu  Remove Included Asset   Callback
 *   The Command that gets executed when user wants to perform 
     Remove Included Asset
 */ 
CKEDITOR.RemoveIncludeCommand = function(){};

CKEDITOR.RemoveIncludeCommand.prototype =
{
	// Hopefully this is the Remove  ContextMenu Callback Operation 
	exec : function( editor )
	{
	    // Get the editors contextmenu handle.. 
	    var contextMenu = editor.contextMenu  ; 
		
	    // Get the editor's selection reference handle 
		var selection = editor.getSelection() ; 	
		
		//Get the reference to the include node
	    var customElement = CKUtilities.MoveToFWCustomNode(editor,'include');
		if(!customElement)
		  return ;
		
	    //Get the parent
	    var parent = customElement.getParent();

	    // Just Remove yourself 
	    customElement.remove() ;
	}
};

/*******************************************************************************************************

                    L I N K E D   A S S E T    E L E M E N T S

********************************************************************************************************

  Contextmenu  Edit  'LINKED' Asset Properties  Callback
   The Command that gets executed when user wants to perform
  Edit Change on LINKED Asset  Element  Selection
 ****************************************************************************************************/



/*******************************************************************************
                   ChangeLinkCommand
               Edit / Change 'LINKED' Asset
     User wants to perform From Contextmenu Pulldown
     Edit / Change 'LINKED' Asset
     The Command that gets executed when user wants to perform
     Change and Link to different Asset  Asset
*******************************************************************************/
CKEDITOR.ChangeLinkCommand = function(){};

CKEDITOR.ChangeLinkCommand.prototype =
{
	// Change Linked Asset
    exec :  function( ckEditor )
    {
        // Is tree applet is Visible
        var showSiteTree = ckEditor.config.showSiteTree ;

        var config =  ckEditor.config ;
        // Get the editor's selection reference handle
        var selection = ckEditor.getSelection() ;

        embedtype = "link" ;

        if(showSiteTree)
	    {
		   this.editEmbeddedLinkT(ckEditor,embedtype);
	    }	    
    },

   //
   //  Advanced UI User wants to modify in essence change
   //  the included Asset with different select from tree asset
   editEmbeddedLinkT: function(ckEditor, embedtype)
   {
      var ckConfig = ckEditor.config;
	  var assetname =  ckConfig.assetName ;
      var assetId = ckConfig.assetId ;
      var AssetType =  ckConfig.assetType ;
      var fieldname =  ckConfig.fieldName ;
      var fielddesc = ckConfig.fieldDesc ;
	  var editingstyle = ckConfig.editingstyle ;
	  var allowedassettypes = ckConfig.allowedassettypes;		
      
	  // Selected from Asset Tab / Tree
      var EncodedString = parent.parent.frames["XcelWorkFrames"].frames["XcelTree"].document.TreeApplet.exportSelections()+'';
      
	  // Do we have a selected Asset
      if ( EncodedString!=null )
      {
       var idArray = EncodedString.split(',');
       var assetcheck = unescape(idArray[0]);
       if (assetcheck.indexOf('assettype=')!=-1 && assetcheck.indexOf('id=')!=-1)
       {
           var test = new String(EncodedString);
           var nameVal = test.split(",");
           if (nameVal.length!=2) {
                alert(editor.lang.fweditcontextmenu.PleaseSelectOnlyOneAsset);
               return false;
           }

           var treeid = unescape(nameVal[0]);
           var splitid = treeid.split(',');
           if (splitid.length==1)
           {
               alert(editor.lang.fweditcontextmenu.PleaseSelectAssetFromTree);
               return false;
           }
           var splitpair = splitid[0].split("=");
           var tn_id = splitpair[1];
           splitpair = splitid[1].split("=");
           var tn_AssetType = splitpair[1];
		   if ( tn_id == assetId && tn_AssetType == AssetType && embedtype !='link' )
	       {
	           alert(editor.lang.fweditcontextmenu.CannotAddSelfInclude);
	           return false;
	       }
		   if(allowedassettypes && allowedassettypes.toUpperCase().indexOf(tn_AssetType.toUpperCase()) == -1){
				alert(editor.lang.fweditcontextmenu.CannotLinkOrIncludeThisAsset+ allowedassettypes);
				return false;
		   }
			var pURL =ckConfig.csRootContext+"ContentServer?pagename=OpenMarket/Xcelerate/Actions/";
           if (embedtype=="link") {		   
            pURL = pURL + "EmbeddedLink";
           } else {		   
            pURL = pURL + "AddInclusion";
           }

          pURL = pURL + "&tn_id=" + tn_id;
          pURL = pURL + "&tn_AssetType=" + tn_AssetType;
          pURL = pURL + "&name=" + encodeURIComponent(assetname);
          pURL = pURL + "&AssetType=" + AssetType;
		  pURL = pURL + "&fieldname=" + encodeURIComponent(fieldname);
          pURL = pURL + "&fielddesc=" + encodeURIComponent(fielddesc);
          pURL = pURL + "&formFieldName=" + encodeURIComponent(fieldname);
		  pURL = pURL + "&EditingStyle=" + encodeURIComponent(editingstyle);
		  pURL = pURL + "&modeType=edit";
		  pURL = pURL + "&IFCKEditor=true";

          return window.open(pURL, "EmbeddedLinkControl", "directories=no,left=700,location=no,menubar=no,resizable=yes,toolbar=no,top=50,width=400,height=700");
       }
       else
       {
          alert(editor.lang.fweditcontextmenu.PleaseSelectAssetFromTree);
       }
   }
   else
   {
      alert(editor.lang.fweditcontextmenu.PleaseSelectAssetFromTree);
   }
}
};



/************************************************************************
   Removes a Linked Asset and will be restored to the Selected Text

   Contextmenu  Remove Included Asset   Callback
 *   The Command that gets executed when user wants to perform
      Remove Included Asset
 **************************************************************************/
CKEDITOR.RemoveAssetLinkCommand = function(){};


CKEDITOR.RemoveAssetLinkCommand.prototype = 
{
	// Hopefully this is the Remove  ContextMenu Callback Operation
	exec : function( editor )
	{
		var char = {
			"&quot;": '"',
			"&amp;": '&',
			"&lt;": '<',
			"&gt;":'>',
			"&circ;":'ˆ',
			"&tilde;":'˜',
			"&ndash;":'–',
			"&mdash;":'—',
			"&lsquo;":'‘',
			"&rsquo;":'’',
			"&sbquo;":'‚',
			"&ldquo;":'“',
			"&rdquo;":'”',
			"&bdquo;":'„',
			"&dagger;":'†',
			"&Dagger;":'‡',
			"&permil;":'‰',
			"&lsaquo;":'‹',
			"&rsaquo;":'›',
			"&nbsp;":' '
		}
	    // Get the editors contextmenu handle..
	    var contextMenu = editor.contextMenu;
	    // Get the editor's selection reference handle
		var selection = editor.getSelection() ;		
		//Get the reference to the include node
	    var customElement = CKUtilities.MoveToFWCustomNode(editor,'link');
	    var selectedText = customElement.getHtml() ;   
		for(key in char){
			selectedText = selectedText.replace(new RegExp(key,"g"),char[key]);
		}
		if(!customElement)
	  	 return ;
	  	var element = new CKEDITOR.dom.element.createFromHtml(selectedText, this.document);
        element.insertAfter(customElement);
		editor.getSelection().selectElement(element );
		if(CKEDITOR.env.webkit){
			editor.getSelection().scrollIntoView();			
		} else {
			editor.getSelection().reset();
		}  
	    customElement.remove() ;		
		
        
	}
};

