﻿
/*********************************************************************************************************
 *                                 fwlinknewasset

  Created: March 25, 2010
  By:      JAG
  Reason:  The FW Tray ToolBar
           to perform the This is a fatwire implementation for creating asset on
           the fly and linking  LINK it in ckeditor.
           Create new and Include asset command and
           functionality

  Modified:  It's possible to customize every single aspect of the editor,
  from the toolbar, to the skin, to dialogs,
  and here we customize the context menu to detect all
  FW  included and linked Assets in an instance of running ckeditor.


  The editor structure is totally plugin based,
  including many of its core features.
  Plugins live in separate files, making them easy to organize and maintain.
  CKEditor  JavaScript API makes developing plugins flexible.

  Modified: Localization

  CKEditor automatically detects the user language,
  automatically configuring its interface to be localized to it.
  This editor speaks the language your users speak.

 *
 * By:  JAG :
 *
 *
 * Reason: Create New and include as Link into ckeditor
           This is a fatwire implementation for creating
           asset on the fly and linking or including
           it in new ckeditor.
/*********************************************************************************************************/

CKEDITOR.plugins.add('fwlinknewasset',   
  {    
    requires: ['fwlinknewasset'],
	lang : ['en','fr','es','de','it','ja','ko','zh-cn','zh','pt-br'],
	
	// When Instace of the Editor is Created this is the Initilization  Callback 
	// for the plug-in button with the editor handle will be passed in to init plugin 
    init:function(editor)
	{
	   var b="fwlinknewasset";
	   // The Execute Command for this Plug-in Button .... when user Clicks on the
	   // Icon 
	   var c=editor.addCommand( b, {
		   exec : function( ckeditor )
				  {
					  var  fieldName = ckeditor.config.fieldName ;
					  var  enableEmbeddedLinks =  ckeditor.config.enableEmbeddedLinks ;    // Yes Enabled
					  var  embedtype = 'link'   ;

					  // Limited Features if attribute property is not true
					  if(enableEmbeddedLinks=='0')
					  {
						return false;
					  }
					  // Do we have a valid editor instance
					  if(fieldName)
					  {
						  // Get the editors contextmenu handle..
						  var contextMenu = ckeditor.contextMenu  ;
						  // Get the editor's selection reference handle
						  var selection = ckeditor.getSelection() ;

						  // This is a link So Get Selected Text
						  var selectedHTML = CKEDITOR.tools.trim(CKUtilities.getCKEditorSelection(ckeditor));
						  if (selectedHTML == "")  {
							  alert(editor.lang.fwlinknewasset.PleaseSelectLinkTextFrom);
							  return false;
						  }

						  var allowedassettypes = ckeditor.config.allowedassettypes ;

						  var assetName =  ckeditor.config.assetName ;
						  var assetId =    ckeditor.config.assetId ;
						  var assetType =  ckeditor.config.assetType ;
						  var fieldName =  ckeditor.config.fieldName ;
						  var fieldDesc =  ckeditor.config.fieldDesc ;
						  var editingstyle = ckeditor.config.editingstyle ;

						  // Get the Root CS Application Context
						  // Set the Elements Base CS Root Context
						  var csRootContext = ckeditor.config.csRootContext  ;

						  // Craft the URL and Element                                                                                              
						  var pURL = csRootContext+"ContentServer?pagename=OpenMarket/Xcelerate/Actions/AddRefFront&cs_environment=addref";

						  // TODO  localize title dude
						  var title = "New";
						  if(allowedassettypes)pURL = pURL + "&childtypes=" + encodeURI(allowedassettypes);
						  pURL = pURL + "&title=" + encodeURI(title);
						  pURL = pURL + "&embedtype=" + encodeURI(embedtype);
						  pURL = pURL + "&FCKName=" + encodeURIComponent(assetName);
						  pURL = pURL + "&FCKAssetId=" + assetId;
						  pURL = pURL + "&FCKAssetType=" + assetType;
						  pURL = pURL + "&fielddesc=" + encodeURIComponent(fieldDesc);
						  pURL = pURL + "&formFieldName=" + encodeURIComponent(fieldName);
						  pURL = pURL + "&EditingStyle=" + encodeURIComponent(editingstyle);
						  pURL = pURL + "&IFCKEditor=true";
						  var windowProperties = 'directories=no,scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no,top=20,width=600,height=600,left=300';

						  return window.open(pURL, "SiteBuilderAddRef", windowProperties);

					  }
				  },
		   canUndo: false    // No support for undo/redo
		  });
 	   editor.ui.addButton("fwlinknewasset",{
					label   :editor.lang.fwlinknewasset.assetCreatorLinkTitleDesc,
					command :b,
					icon:   this.path+"fwlinknewasset.gif"
		});			
		
	}
});



