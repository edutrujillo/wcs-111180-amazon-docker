/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwlinknewasset', 'zh',
{
	fwlinknewasset :
	{
	  assetCreatorIncludeDlgTitle : '資產建立者',
	  assetCreatorIncludeTitleDesc : '建立和包括新資產',
	  assetCreatorLinkTitleDesc : '建立和連結新資產',
	  PleaseSelectLinkTextFrom	: '必須先選取資產連結的文字' 
	
	
	  	    
	}
});

