/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwlinknewasset', 'pt-br',
{
	fwlinknewasset :
	{
	  assetCreatorIncludeDlgTitle : 'Criador do Ativo',
	  assetCreatorIncludeTitleDesc : 'Criar e incluir um novo ativo',
	  assetCreatorLinkTitleDesc : 'Criar e vincular um novo ativo',
	  PleaseSelectLinkTextFrom	: 'O texto a ser vinculado ao ativo deve ser selecionado primeiro' 
	
	
	  	    
	}
});

