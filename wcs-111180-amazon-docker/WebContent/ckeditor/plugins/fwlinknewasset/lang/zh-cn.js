/*
 * Asset Creator English language file.
 */

 
 
CKEDITOR.plugins.setLang( 'fwlinknewasset', 'zh-cn',
{
	fwlinknewasset :
	{
	  assetCreatorIncludeDlgTitle : '资产创建者',
	  assetCreatorIncludeTitleDesc : '创建和包括新资产',
	  assetCreatorLinkTitleDesc : '创建和链接新资产',
	  PleaseSelectLinkTextFrom	: '必须先选择要链接到资产的文本' 
	
	
	  	    
	}
});

